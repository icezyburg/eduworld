'use strict';

/**
 * @ngdoc directive
 * @name minovateApp.directive:formSubmit
 * @description
 * # formSubmit
 */
angular.module('minovateApp')

.directive("serializer", function () {
	return {
		restrict: "A"
		, scope: {
			onSubmit: "&serializer"
		}
		, link: function (scope, element) {
			// assuming for brevity that directive is defined on <form>

			var form = element;

			form.submit(function (event) {
				event.preventDefault();
				var serializedData = form.serializeArray();

				scope.onSubmit({
					data: serializedData
				});
			});

		}
	};
});
