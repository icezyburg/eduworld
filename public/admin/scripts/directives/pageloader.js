'use strict';

/**
 * @ngdoc directive
 * @name minovateApp.directive:pageLoader
 * @description
 * # pageLoader
 */
angular.module('minovateApp')
    .directive('pageLoader', [
        '$timeout',
        function($timeout) {
            return {
                restrict: 'AE',
                template: '<div class="dot1"></div><div class="dot2"></div>',
                link: function(scope, element) {
                    element.addClass('hide');
                    // console.log(scope.loading + "kskskskssksdsk");
                    scope.$watch("loading", function() {
                        // console.log("loading" + scope.loading);
                        //element.addClass('hide');
                        if (scope.loading) {
                            $timeout(function() {
                                element.removeClass('hide');
                                element.addClass('animate');
                            }, 600);
                            // console.log("test " + scope.loading);
                        } else {
                            $timeout(function() {
                                element.addClass('hide');
                            }, 600);
                        }
                    })
                    scope.$on('$stateChangeStart', function() {
                        element.toggleClass('hide animate');
                    });
                    scope.$on('$stateChangeSuccess', function(event) {
                        event.targetScope.$watch('$viewContentLoaded', function() {
                            $timeout(function() {
                                element.toggleClass('hide animate');
                            }, 600);
                        });
                    });
                }
            };
        }
    ]);
