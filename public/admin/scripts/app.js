'use strict';

/**
 * @ngdoc overview
 * @name minovateApp
 * @description
 * # minovateApp
 *
 * Main module of the application.
 */
angular
	.module('minovateApp', [
        'ngAnimate'
        , 'ngCookies'
        , 'ngResource'
        , 'ngSanitize'
        , 'froala'
        , 'ngTouch'
        , 'picardy.fontawesome'
        , 'ui.bootstrap'
        , 'ui.router'
        , 'ui.utils'
        , 'angular-loading-bar'
        , 'angular-momentjs'
        , 'FBAngular'
        , 'lazyModel'
        , 'toastr'
        , 'angularBootstrapNavTree'
        , 'oc.lazyLoad'
        , 'ui.select'
        , 'ui.tree'
        , 'textAngular'
        , 'colorpicker.module'
        , 'angularFileUpload'
        , 'ngImgCrop'
        , 'datatables'
        , 'datatables.bootstrap'
        , 'datatables.colreorder'
        , 'datatables.colvis'
        , 'datatables.tabletools'
        , 'datatables.scroller'
        , 'datatables.columnfilter'
        , 'ui.grid'
        , 'ui.grid.resizeColumns'
        , 'ui.grid.edit'
        , 'ui.grid.moveColumns'
        , 'ngTable'
        , 'smart-table'
        , 'angular-flot'
        , 'angular-rickshaw'
        , 'easypiechart'
        , 'uiGmapgoogle-maps'
        , 'ui.calendar'
        , 'ngTagsInput'
        , 'pascalprecht.translate'
        , 'satellizer'
        , 'ui.tree'
    , ])
	.run(['$rootScope', '$state', '$stateParams', '$http', "$auth", '$timeout', function ($rootScope, $state, $stateParams, $http, $auth, $timeout) {
		$rootScope.baseUrl = "";
		$rootScope.$state = $state;
		$rootScope.loading = false;
		$rootScope.$stateParams = $stateParams;
		$rootScope.$on('$stateChangeSuccess', function (event, toState) {
			$rootScope.optionsToast = {
				position: 'toast-bottom-right'
				, type: 'success'
				, iconClass: "success"
				, iconType: {
					name: 'check'
					, value: 'fa-check'
				}
				, timeout: '5000'
				, extendedTimeout: '1000'
				, html: false
				, closeButton: false
				, tapToDismiss: true
				, closeHtml: '<i class="fa fa-times"></i>'
			};
			event.targetScope.$watch('$viewContentLoaded', function () {
				// if ($auth.isAuthenticated()) {
				//     var tokBased = {
				//         token: $auth.getToken()
				//     }
				//     $http.post($rootScope.baseUrl + 'api/authenticate/refresh', JSON.stringify(tokBased))
				//         .success(function(result) {
				//             $auth.setToken(result.newToken, true);
				//         }).catch(function(e) {

				//             localStorage.removeItem('user');

				//             // Flip authenticated to false so that we no longer
				//             // show UI elements dependant on the user being logged in
				//             $rootScope.authenticated = false;

				//             // Remove the current user info from rootscope
				//             $rootScope.currentUser = null;
				//             $auth.logout();
				//             $timeout(function() {
				//                 $rootScope.error_feed = "Login Expired , please login again.";
				//             });
				//         });
				// }

				var user = JSON.parse(localStorage.getItem('user'));

				// if ($auth.isAuthenticated()) {
				//     console.log("asuu");
				//     $http.get($rootScope.baseUrl + "api/authenticate/user")
				//         .success(function(result) {
				//             var user = JSON.stringify(result.user);
				//             if (result.user.user_role == 0) {
				//                 console.log(toState.name);
				//                 if (toState.name == "core.login") {
				//                     $state.go('app.dashboard', {});
				//                 }
				//                 // Set the stringified user data into local storage
				//                 localStorage.setItem('user', user);

				//                 // The user's authenticated state gets flipped to
				//                 // true so we can now show parts of the UI that rely
				//                 // on the user being logged in
				//                 $rootScope.authenticated = true;

				//                 // Putting the user's data on $rootScope allows
				//                 // us to access it anywhere across the app
				//                 $rootScope.currentUser = result.user;
				//                 if (result.user.user_photo == "") {
				//                     $rootScope.avatar = "Profile_03.png";
				//                 } else {
				//                     $rootScope.avatar = result.user.user_photo;
				//                 }
				//                 if (result.user.user_token == 1) {
				//                     $rootScope.superUser = true;
				//                 } else if (result.user.user_token == 2) {
				//                     $rootScope.superUser = "";
				//                 }
				//             } else {
				//                 $rootScope.superUser = "";
				//                 localStorage.removeItem('user');
				//                 // Flip authenticated to false so that we no longer
				//                 // show UI elements dependant on the user being logged in
				//                 $rootScope.authenticated = false;

				//                 // Remove the current user info from rootscope
				//                 $rootScope.currentUser = null;
				//                 // window.location.href = "/";
				//             }

				//         }).catch(function(e) {
				//             localStorage.removeItem('user');
				//             $('#ModalLogin').modal('show');
				//             $timeout(function() {
				//                 $rootScope.error_feed = "Login Expired , please login again.";
				//             });
				//             // Flip authenticated to false so that we no longer
				//             // show UI elements dependant on the user being logged in
				//             $rootScope.authenticated = false;

				//             // Remove the current user info from rootscope
				//             $rootScope.currentUser = null;
				//             $auth.logout();
				//             if (toState.name != "core.login") {
				//                 // window.location.href = "/";
				//             }
				//         });
				// }
				angular.element('html, body, #content').animate({
					scrollTop: 0
				}, 200);

				setTimeout(function () {
					angular.element('#wrap').css('visibility', 'visible');

					if (!angular.element('.dropdown').hasClass('open')) {
						angular.element('.dropdown').find('>ul').slideUp();
					}
				}, 200);
			});
			$rootScope.containerClass = toState.containerClass;
		});
    }])
	.value('froalaConfig', {
		key: 'rfmioD-16e1tB1pH-9x=='
	})
	.config(['uiSelectConfig', function (uiSelectConfig) {
		uiSelectConfig.theme = 'bootstrap';
    }])
	//
	////angular-language
	//.config(['$translateProvider', function($translateProvider) {
	//    $translateProvider.useStaticFilesLoader({
	//        prefix: 'languages/',
	//        suffix: '.json'
	//    });
	//    $translateProvider.useLocalStorage();
	//    $translateProvider.preferredLanguage('en');
	//}])

.config(['$stateProvider', '$urlRouterProvider', '$authProvider', function ($stateProvider, $urlRouterProvider, $authProvider) {


	$authProvider.loginUrl = 'api/authenticate';

	// $urlRouterProvider.otherwise('/app/dashboard');
	$urlRouterProvider.otherwise('/core/login');

	$stateProvider

		.state('app', {
			abstract: true
			, url: '/app'
			, controller: 'HeaderCtrl as head'
			, templateUrl: 'admin/views/tmpl/app.html'
		})
		.state('download', {
			abstract: true
			, url: '/download'
			, controller: 'HeaderCtrl as head'
			, templateUrl: 'admin/views/tmpl/app.html'
		})
		.state('setting', {
			abstract: true
			, url: '/setting'
			, controller: 'HeaderCtrl as head'
			, templateUrl: 'admin/views/tmpl/app.html'
		})

	/* State for About */
	.state('app.about.edit', {
			url: '/about/edit/:blogID'
			, controller: 'EditaboutCtrl as blog'
			, templateUrl: 'admin/views/admin/about_edit.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.about.create', {
			url: '/about/create'
			, controller: 'aboutCtrl as blog'
			, templateUrl: 'admin/views/admin/about_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.about', {
			url: '/about'
			, controller: 'aboutListCtrl as blog'
			, templateUrl: 'admin/views/admin/about.html'
			, parent: "app"
		})

	/* State for Data */
	.state('data.detail', {
			url: '/data/detail/:id'
			, controller: 'DetaildataCtrl as blog'
			, templateUrl: 'admin/views/admin/data_detail.html'
			, parent: "app"
		})
		.state('data', {
			url: '/data/:type'
			, controller: 'dataListCtrl as blog'
			, templateUrl: 'admin/views/admin/data.html'
			, parent: "app"
		})

	/* State for services */
	.state('app.services.edit', {
			url: '/services/edit/:blogID'
			, controller: 'EditservicesCtrl as blog'
			, templateUrl: 'admin/views/admin/services_edit.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.services.create', {
			url: '/services/create'
			, controller: 'servicesCtrl as blog'
			, templateUrl: 'admin/views/admin/services_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.services', {
			url: '/services'
			, controller: 'servicesListCtrl as blog'
			, templateUrl: 'admin/views/admin/services.html'
			, parent: "app"
		})
		.state('app.servicescategory', {
			url: '/services/:id'
			, controller: 'servicesCategoryListCtrl as servicesCat'
			, templateUrl: 'admin/views/admin/services.html'
			, parent: "app"
		})

	/* State for Testimonial */
	.state('app.testimonial.edit', {
			url: '/testimonial/edit/:blogID'
			, controller: 'EdittestimonialCtrl as blog'
			, templateUrl: 'admin/views/admin/testimonial_edit.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.testimonial.create', {
			url: '/testimonial/create'
			, controller: 'testimonialCtrl as blog'
			, templateUrl: 'admin/views/admin/testimonial_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.testimonial', {
			url: '/testimonial'
			, controller: 'testimonialListCtrl as blog'
			, templateUrl: 'admin/views/admin/testimonial.html'
			, parent: "app"
		})


	/* State for Page */
	.state('app.page.edit', {
			url: '/page/edit/:pageID'
			, controller: 'EditpageCtrl as blog'
			, templateUrl: 'admin/views/admin/page_edit.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.page.create', {
			url: '/page/create'
			, controller: 'pageCtrl as blog'
			, templateUrl: 'admin/views/admin/page_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.page', {
			url: '/page'
			, controller: 'pageListCtrl as blog'
			, templateUrl: 'admin/views/admin/page.html'
			, parent: "app"
		})


	/* State for Post */
	.state('app.post.edit', {
		url: '/post/edit/:blogID'
		, controller: 'EditblogCtrl as blog'
		, templateUrl: 'admin/views/admin/post_edit.html'
		, parent: "app"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
		}
	})

	.state('app.post.create', {
		url: '/post/create'
		, controller: 'blogCtrl as blog'
		, templateUrl: 'admin/views/admin/post_create.html'
		, parent: "app"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                        'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                    ]);
                }]
		}
	})
	.state('app.post', {
		url: '/post'
		, controller: 'postListCtrl as blog'
		, templateUrl: 'admin/views/admin/post.html'
		, parent: "app"
	})
	.state('app.postCategory', {
		url: '/post/category/:id'
		, controller: 'postCategoryListCtrl as blog'
		, templateUrl: 'admin/views/admin/post.html'
		, parent: "app"
	})


	.state('app.video', {
		url: '/video'
		, controller: 'videoListCtrl as video'
		, templateUrl: 'admin/views/admin/video.html'
		, parent: "app"
	})

	.state('app.video.create', {
		url: '/video/create'
		, controller: 'videoCtrl as blog'
		, templateUrl: 'admin/views/admin/video_create.html'
		, parent: "app"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                        'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                    ]);
                }]
		}
	})
	.state('app.video.edit', {
		url: '/post/video/:blogID'
		, controller: 'EditvideoCtrl as blog'
		, templateUrl: 'admin/views/admin/video_edit.html'
		, parent: "app"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
		}
	})
		

	/* State for Gallery */

	.state('app.gallery', {
			url: '/gallery/:albumID'
			, controller: 'galleryListCtrl as blog'
			, templateUrl: 'admin/views/admin/gallery.html'
			, parent: "app"
		})

	.state('app.gallery.edit', {
		url: '/gallery/edit/:galleryID'
		, controller: 'EditgalleryCtrl as blog'
		, templateUrl: 'admin/views/admin/gallery_edit.html'
		, parent: "app"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
		}
	})

	.state('app.gallery.create', {
			url: '/gallery/:albumID/create'
			, controller: 'galleryCtrl as blog'
			, templateUrl: 'admin/views/admin/gallery_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
	.state('app.gallery.bulk_create', {
			url: '/gallery/bulk-create/:albumID'
			, controller: 'galleryBulkCtrl as blog'
			, templateUrl: 'admin/views/admin/gallery_bulk_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		
		.state('app.album', {
			url: '/album'
			, controller: 'albumListCtrl as blog'
			, templateUrl: 'admin/views/admin/album.html'
			, parent: "app"
		})
		.state('app.album.create', {
			url: '/album/create'
			, controller: 'albumCreateCtrl as category'
			, templateUrl: 'admin/views/admin/album_add.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
								'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
							]);
						}]
			}
		})
		.state('app.album.edit', {
			url: '/album/edit/:albumID'
			, controller: 'EditalbumCtrl as category'
			, templateUrl: 'admin/views/admin/album_edit.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
								'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
							]);
						}]
			}
		})


	/* State for Post Category */
	.state('app.category.edit', {
		url: '/category/edit/:categoryID'
		, controller: 'EditcategoryCtrl as category'
		, templateUrl: 'admin/views/admin/category_edit.html'
		, parent: "app"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
		}
	})

	.state('app.category.create', {
			url: '/category/create'
			, controller: 'categoryCtrl as category'
			, templateUrl: 'admin/views/admin/category_create.html'
			, parent: "app"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('app.category', {
			url: '/category'
			, controller: 'categoryListCtrl as category'
			, templateUrl: 'admin/views/admin/category.html'
			, parent: "app"
		})

	/* State for Download */
	.state('downloadEdit', {
		url: '/edit/:blogID'
		, controller: 'EditdownloadCtrl as blog'
		, templateUrl: 'admin/views/admin/download_edit.html'
		, parent: "download"
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
		}
	})

	.state('download.create', {
			url: '/create'
			, controller: 'downloadCtrl as blog'
			, templateUrl: 'admin/views/admin/download_add.html'
			, parent: "download"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		.state('download.show', {
			url: '/show'
			, controller: 'downloadListTbl as blog'
			, templateUrl: 'admin/views/admin/download.html'
			, parent: "download"
		})

	/* State for Contact */
	.state('setting.contact', {
			url: '/contact'
			, controller: 'contactCtrl as blog'
			, templateUrl: 'admin/views/admin/contact.html'
			, parent: "setting"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		/* State for Social Media */
		.state('setting.social', {
			url: '/social'
			, controller: 'socialCtrl as blog'
			, templateUrl: 'admin/views/admin/social.html'
			, parent: "setting"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})
		/* State for Website Setting */
		.state('setting.website', {
			url: '/website'
			, controller: 'websiteCtrl as blog'
			, templateUrl: 'admin/views/admin/website.html'
			, parent: "setting"
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                            'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                        ]);
                    }]
			}
		})

	//dashboard
	.state('app.dashboard', {
		url: '/dashboard'
		, controller: 'DashboardCtrl'
		, templateUrl: 'admin/views/tmpl/dashboard.html'
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                        'admin/scripts/vendor/datatables/datatables.bootstrap.min.css'
                        , 'admin/scripts/vendor/datatables/datatables.bootstrap.min.css'
                    ]);
                }]
		}
	})



	/* State for Gallery */
	.state('app.blog.gallery', {
		url: '/gallery'
		, controller: 'galleryListTbl as gallery'
		, templateUrl: 'admin/views/admin/list_gallery.html'
		, parent: "app.blog"
	})

	/* State for Album */
	.state('app.blog.album', {
		url: '/album'
		, controller: 'blogAlbumListTbl as blogList'
		, templateUrl: 'admin/views/admin/list_album.html'
		, parent: "app.blog"
	})

	.state('app.blog.album.create', {
		url: '/album/create'
		, controller: 'albumCtrl as blog'
		, templateUrl: 'admin/views/admin/album.html'
		, parent: "app.blog"
	})

	.state('app.blog.album.edit', {
		url: '/album/edit/:blogID'
		, controller: 'EditalbumCtrl as blog'
		, templateUrl: 'admin/views/admin/album_edit.html'
		, parent: "app.blog"
	})


	.state('app.contact', {
		url: '/contact-us'
		, controller: 'contactCtrl as blog'
		, templateUrl: 'admin/views/admin/contact.html'
		, parent: "app"
	})

	.state('app.blog.edit', {
		url: '/edit/:blogID/:type'
		, controller: 'EditblogCtrl as blog'
		, templateUrl: 'admin/views/admin/blog_edit.html'
		, parent: "app.blog"
	})

	.state('app.thread', {
		url: '/thread'
		, template: '<div ui-view></div>'
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                    'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                ]);
            }]
		}
	})

	.state('app.thread.show', {
		url: '/show/:type/'
		, controller: 'threadListTbl as blog'
		, templateUrl: 'admin/views/admin/list_thread.html'
		, parent: "app.thread"
	})

	.state('app.thread.detail', {
		url: '/detail/:id/:type'
		, controller: 'threadDetail as td'
		, templateUrl: 'admin/views/admin/detail_thread.html'
		, parent: "app.thread"
	})

	.state('app.thread.comment', {
		url: '/comment/:id/:type'
		, controller: 'threadComment as tc'
		, templateUrl: 'admin/views/admin/comment_thread.html'
		, parent: "app.thread"
	})


	//forms/common
	.state('app.forms.common', {
			url: '/common'
			, controller: 'FormsCommonCtrl'
			, templateUrl: 'admin/views/tmpl/forms/common.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'admin/scripts/vendor/slider/bootstrap-slider.js'
                        , 'admin/scripts/vendor/touchspin/jquery.bootstrap-touchspin.js'
                        , 'admin/scripts/vendor/touchspin/jquery.bootstrap-touchspin.css'
                        , 'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                    ]);
                }]
			}
		})
		//forms/validate
		.state('app.forms.validate', {
			url: '/validate'
			, controller: 'FormsValidateCtrl'
			, templateUrl: 'admin/views/tmpl/forms/validate.html'
		})
		// forms/wizard
		.state('app.forms.wizard', {
			url: '/wizard'
			, controller: 'FormWizardCtrl'
			, templateUrl: 'admin/views/tmpl/forms/wizard.html'
		})

	//forms/upload
	.state('app.forms.upload', {
		url: '/upload/:album'
		, controller: 'FormUploadCtrl'
		, templateUrl: 'admin/views/admin/gallery_upload.html'
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                    'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                ]);
            }]
		}
	})

	//forms/upload
	.state('app.forms.tools', {
		url: '/tools/resize'
		, controller: 'toolsCtrl'
		, templateUrl: 'admin/views/admin/MadImageTools.html'
		, resolve: {
			plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load([
                    'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                ]);
            }]
		}
	})

	//forms/imgcrop
	.state('app.forms.imgcrop', {
			url: '/imagecrop'
			, controller: 'FormImgCropCtrl'
			, templateUrl: 'admin/views/tmpl/forms/imgcrop.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                    ]);
                }]
			}
		})
		//tables
		.state('app.tables', {
			url: '/tables'
			, template: '<div ui-view></div>'
		})
		//tables/bootstrap
		.state('app.tables.bootstrap', {
			url: '/bootstrap'
			, controller: 'TablesBootstrapCtrl'
			, templateUrl: 'admin/views/tmpl/tables/bootstrap.html'
		})
		//tables/datatables
		.state('app.tables.datatables', {
			url: '/datatables'
			, controller: 'TablesDatatablesCtrl'
			, templateUrl: 'admin/views/tmpl/tables/datatables.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'scripts/vendor/datatables/ColReorder/css/dataTables.colReorder.min.css'
                        , 'scripts/vendor/datatables/ColReorder/js/dataTables.colReorder.min.js'
                        , 'scripts/vendor/datatables/Responsive/dataTables.responsive.css'
                        , 'scripts/vendor/datatables/Responsive/dataTables.responsive.js'
                        , 'scripts/vendor/datatables/ColVis/css/dataTables.colVis.min.css'
                        , 'scripts/vendor/datatables/ColVis/js/dataTables.colVis.min.js'
                        , 'scripts/vendor/datatables/TableTools/css/dataTables.tableTools.css'
                        , 'scripts/vendor/datatables/TableTools/js/dataTables.tableTools.js'
                        , 'scripts/vendor/datatables/datatables.bootstrap.min.css'
                    ]);
                }]
			}
		})
		//tables/uiGrid
		.state('app.tables.ui-grid', {
			url: '/ui-grid'
			, controller: 'TablesUiGridCtrl'
			, templateUrl: 'admin/views/tmpl/tables/ui-grid.html'
		})
		//tables/ngTable
		.state('app.tables.ng-table', {
			url: '/ng-table'
			, controller: 'TablesNgTableCtrl'
			, templateUrl: 'admin/views/tmpl/tables/ng-table.html'
		})
		//tables/smartTable
		.state('app.tables.smart-table', {
			url: '/smart-table'
			, controller: 'TablesSmartTableCtrl'
			, templateUrl: 'admin/views/tmpl/tables/smart-table.html'
		})
		//tables/fooTable
		.state('app.tables.footable', {
			url: '/footable'
			, controller: 'TablesFootableCtrl'
			, templateUrl: 'admin/views/tmpl/tables/footable.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'scripts/vendor/footable/dist/footable.all.min.js'
                        , 'scripts/vendor/footable/css/footable.core.min.css'
                    ]);
                }]
			}
		})
		//charts
		.state('app.charts', {
			url: '/charts'
			, controller: 'ChartsCtrl'
			, templateUrl: 'admin/views/tmpl/charts.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'scripts/vendor/flot/jquery.flot.resize.js'
                        , 'scripts/vendor/flot/jquery.flot.orderBars.js'
                        , 'scripts/vendor/flot/jquery.flot.stack.js'
                        , 'scripts/vendor/flot/jquery.flot.pie.js'
                        , 'scripts/vendor/gaugejs/gauge.min.js'
                    ]);
                }]
			}
		})
		//layouts
		.state('app.layouts', {
			url: '/layouts'
			, template: '<div ui-view></div>'
		})
		//layouts/boxed
		.state('app.layouts.boxed', {
			url: '/boxed'
			, controller: 'BoxedlayoutCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/boxed.html'
			, containerClass: 'boxed-layout'
		})
		//layouts/fullwidth
		.state('app.layouts.fullwidth', {
			url: '/fullwidth'
			, controller: 'FullwidthlayoutCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/fullwidth.html'
		})
		//layouts/sidebar-sm
		.state('app.layouts.sidebar-sm', {
			url: '/sidebar-sm'
			, controller: 'SidebarsmlayoutCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/sidebar-sm.html'
			, containerClass: 'sidebar-sm-forced sidebar-sm'
		})
		//layouts/sidebar-xs
		.state('app.layouts.sidebar-xs', {
			url: '/sidebar-xs'
			, controller: 'SidebarxslayoutCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/sidebar-xs.html'
			, containerClass: 'sidebar-xs-forced sidebar-xs'
		})
		//layouts/offcanvas
		.state('app.layouts.offcanvas', {
			url: '/offcanvas'
			, controller: 'OffcanvaslayoutCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/offcanvas.html'
			, containerClass: 'sidebar-offcanvas'
		})
		//layouts/hz-menu
		.state('app.layouts.hz-menu', {
			url: '/hz-menu'
			, controller: 'HzmenuCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/hz-menu.html'
			, containerClass: 'hz-menu'
		})
		//layouts/rtl-layout
		.state('app.layouts.rtl', {
			url: '/rtl'
			, controller: 'RtlCtrl'
			, templateUrl: 'admin/views/tmpl/layouts/rtl.html'
			, containerClass: 'rtl'
		})
		//maps
		.state('app.maps', {
			url: '/maps'
			, template: '<div ui-view></div>'
		})
		//maps/vector
		.state('app.maps.vector', {
			url: '/vector'
			, controller: 'VectorMapCtrl'
			, templateUrl: 'admin/views/tmpl/maps/vector.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'scripts/vendor/jqvmap/jqvmap/jquery.vmap.min.js'
                        , 'scripts/vendor/jqvmap/jqvmap/maps/jquery.vmap.world.js'
                        , 'scripts/vendor/jqvmap/jqvmap/maps/jquery.vmap.usa.js'
                        , 'scripts/vendor/jqvmap/jqvmap/maps/jquery.vmap.europe.js'
                        , 'scripts/vendor/jqvmap/jqvmap/maps/jquery.vmap.germany.js'
                    ]);
                }]
			}
		})
		//maps/google
		.state('app.maps.google', {
			url: '/google'
			, controller: 'GoogleMapCtrl'
			, templateUrl: 'admin/views/tmpl/maps/google.html'
		, })
		//maps/leaflet
		.state('app.maps.leaflet', {
			url: '/leaflet'
			, controller: 'LeafletMapCtrl'
			, templateUrl: 'admin/views/tmpl/maps/leaflet.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load({
						name: 'leaflet-directive'
					});
                }]
			}
		})
		//calendar
		.state('app.calendar', {
			url: '/calendar'
			, controller: 'CalendarCtrl'
			, templateUrl: 'admin/views/tmpl/calendar.html'
		})
		//app core pages (errors, login,signup)
		.state('core', {
			abstract: true
			, url: '/core'
			, template: '<div ui-view></div>'
		})
		//login
		.state('core.login', {
			url: '/login'
			, controller: 'LoginCtrl as auth'
			, templateUrl: 'admin/views/tmpl/pages/login.html'
		})
		//signup
		.state('core.signup', {
			url: '/signup'
			, controller: 'SignupCtrl'
			, templateUrl: 'admin/views/tmpl/pages/signup.html'
		})
		//forgot password
		.state('core.forgotpass', {
			url: '/forgotpass'
			, controller: 'ForgotPasswordCtrl'
			, templateUrl: 'admin/views/tmpl/pages/forgotpass.html'
		})
		//page 404
		.state('core.page404', {
			url: '/page404'
			, templateUrl: 'admin/views/tmpl/pages/page404.html'
		})
		//page 500
		.state('core.page500', {
			url: '/page500'
			, templateUrl: 'admin/views/tmpl/pages/page500.html'
		})
		//page offline
		.state('core.page-offline', {
			url: '/page-offline'
			, templateUrl: 'admin/views/tmpl/pages/page-offline.html'
		})
		//locked screen
		.state('core.locked', {
			url: '/locked'
			, templateUrl: 'admin/views/tmpl/pages/locked.html'
		})
		//example pages
		.state('app.pages', {
			url: '/pages'
			, template: '<div ui-view></div>'
		})
		//gallery page
		.state('app.pages.gallery', {
			url: '/gallery/:album'
			, controller: 'GalleryCtrl'
			, templateUrl: 'admin/views/tmpl/pages/gallery.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'scripts/vendor/mixitup/jquery.mixitup.js'
                        , 'scripts/vendor/magnific/magnific-popup.css'
                        , 'scripts/vendor/magnific/jquery.magnific-popup.min.js'
                    ]);
                }]
			}
		})
		//timeline page
		.state('app.pages.timeline', {
			url: '/timeline'
			, controller: 'TimelineCtrl'
			, templateUrl: 'admin/views/tmpl/pages/timeline.html'
		})
		//chat page
		.state('app.pages.chat', {
			url: '/chat'
			, controller: 'ChatCtrl'
			, templateUrl: 'admin/views/tmpl/pages/chat.html'
		})
		//search results
		.state('app.pages.search-results', {
			url: '/search-results'
			, controller: 'SearchResultsCtrl'
			, templateUrl: 'admin/views/tmpl/pages/search-results.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'scripts/vendor/slider/bootstrap-slider.js'
                    ]);
                }]
			}
		})
		//profile page
		.state('app.pages.profile', {
			url: '/profile'
			, controller: 'ProfileCtrl'
			, templateUrl: 'admin/views/tmpl/pages/profile.html'
			, resolve: {
				plugins: ['$ocLazyLoad', function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                        'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                    ]);
                }]
			}
		})
		//documentation
		.state('app.help', {
			url: '/help'
			, controller: 'HelpCtrl'
			, templateUrl: 'admin/views/tmpl/help.html'
		})
		
	.state('app.admin', {
        url: '/admin',
        template: '<div ui-view></div>',
        resolve: {
            plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'admin/scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                ]);
            }]
        }
    })
    .state('app.admin.show', {
        url: '/show/',
        controller: 'adminListTbl as blog',
        templateUrl: 'admin/views/admin/list_admin.html',
        parent: "app.admin"
    })

    .state('app.admin.change', {
        url: '/change/:id/:user',
        controller: 'changePass as td',
        templateUrl: 'admin/views/admin/change_password.html',
        parent: "app.admin"
    })


    .state('app.admin.create', {
        url: '/create/',
        controller: 'createAdmin as td',
        templateUrl: 'admin/views/admin/admin.html',
        parent: "app.admin"
    })

	.state('app.admin.comment', {
		url: 'admin//comment/:id/:type'
		, controller: 'threadComment as tc'
		, templateUrl: 'admin/views/admin/comment_thread.html'
		, parent: "app.thread"
	});


}]);
