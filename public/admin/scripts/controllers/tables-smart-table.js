'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:TablesSmartTableCtrl
 * @description
 * # TablesSmartTableCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
    .controller('TablesSmartTableCtrl', function($scope) {
        $scope.page = {
            title: 'Smart Table',
            subtitle: 'Place subtitle here...'
        };
    })

.controller('BasicTableCtrl', ['$scope', '$filter', function($scope) {
    var firstnames = ['Laurent', 'Blandine', 'Olivier', 'Max'];
    var lastnames = ['Renard', 'Faivre', 'Frere', 'Eponge'];
    var dates = ['1987-05-21', '1987-04-25', '1955-08-27', '1966-06-06'];
    var id = 1;

    function generateRandomItem(id) {

        var firstname = firstnames[Math.floor(Math.random() * 3)];
        var lastname = lastnames[Math.floor(Math.random() * 3)];
        var birthdate = dates[Math.floor(Math.random() * 3)];
        var balance = Math.floor(Math.random() * 2000);

        return {
            id: id,
            firstName: firstname,
            lastName: lastname,
            birthDate: new Date(birthdate),
            balance: balance
        };
    }

    $scope.rowCollection = [];

    for (id; id < 5; id++) {
        $scope.rowCollection.push(generateRandomItem(id));
    }

    //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
    $scope.displayedCollection = [].concat($scope.rowCollection);

    //add to the real data holder
    $scope.addRandomItem = function addRandomItem() {
        $scope.rowCollection.push(generateRandomItem(id));
        id++;
    };

    //remove to the real data holder
    $scope.removeItem = function removeItem(row) {
        var index = $scope.rowCollection.indexOf(row);
        if (index !== -1) {
            $scope.rowCollection.splice(index, 1);
        }
    };

    $scope.predicates = ['firstName', 'lastName', 'birthDate', 'balance', 'email'];
    $scope.selectedPredicate = $scope.predicates[0];
}])

.controller('RowTableCtrl', ['$scope', '$filter', function(scope) {
    var
        nameList = ['Pierre', 'Pol', 'Jacques', 'Robert', 'Elisa'],
        familyName = ['Dupont', 'Germain', 'Delcourt', 'bjip', 'Menez'];

    function createRandomItem() {
        var
            firstName = nameList[Math.floor(Math.random() * 4)],
            lastName = familyName[Math.floor(Math.random() * 4)],
            age = Math.floor(Math.random() * 100),
            email = firstName + lastName + '@whatever.com',
            balance = Math.random() * 3000;

        return {
            firstName: firstName,
            lastName: lastName,
            age: age,
            email: email,
            balance: balance
        };
    }

    scope.itemsByPage = 10;

    scope.rowCollection = [];
    for (var j = 0; j < 200; j++) {
        scope.rowCollection.push(createRandomItem());
    }
}])

.controller('PipeTableCtrl', ['$scope', '$timeout', function($scope, $timeout) {
        var nameList = ['Pierre', 'Pol', 'Jacques', 'Robert', 'Elisa'];
        var familyName = ['Dupont', 'Germain', 'Delcourt', 'bjip', 'Menez'];
        var promise = null;


        $scope.isLoading = false;
        $scope.rowCollection = [];


        function createRandomItem() {
            var
                firstName = nameList[Math.floor(Math.random() * 4)],
                lastName = familyName[Math.floor(Math.random() * 4)],
                age = Math.floor(Math.random() * 100),
                email = firstName + lastName + '@whatever.com',
                balance = Math.random() * 3000;

            return {
                firstName: firstName,
                lastName: lastName,
                age: age,
                email: email,
                balance: balance
            };
        }

        function getAPage() {
            $scope.rowCollection = [];
            for (var j = 0; j < 20; j++) {
                $scope.rowCollection.push(createRandomItem());
            }
        }

        $scope.callServer = function getData(tableState, tableController) {

            //here you could create a query string from tableState
            //fake ajax call
            $scope.isLoading = true;

            $timeout(function() {
                getAPage();
                $scope.isLoading = false;
            }, 2000);

        };

        getAPage();

    }])
    .controller('articlesListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Articles',
            subtitle: 'Articles Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.articles.create", { 'type': "articles" })
        };
        $scope.typer = "articles";
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/articles")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })

    .controller('blogListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'News',
            subtitle: 'News Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.news.create", { 'type': "news" })
        };
        $scope.typer = "news";
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/show/11")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })


    .controller('visimisiListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Vision and Mission',
            subtitle: 'Vision and Mission Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.visimisi.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/show/22")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogBrands = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('teamListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Team',
            subtitle: 'Team Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.team.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/team")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogBrands = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('showcaseListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'TV Showcase',
            subtitle: 'TV Showcase Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.showcase.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/showcase")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogBrands = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('brandsListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Brands',
            subtitle: 'Brands Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.brands.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/show/10")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogBrands = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('blogDnaListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'List Our DNA',
            subtitle: 'Our DNA'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.blog.dna.create", { 'type': "our-dna" })
        };
        $scope.typer = "our-dna";
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/show/2")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('blogAlbumListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'List Our DNA',
            subtitle: 'Our DNA'
        };
        $rootScope.loading = true;
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.blog.album.create", { 'type': "album" })
        };
        $scope.typer = "our-dna";
        $http.get($scope.baseUrl + "blog/show/8")
            .success(function(result) {
                $scope.rowBlogNews = result;
            })
            .then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                })
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })

.controller('galleryListTbl', function($rootScope, $timeout, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
    $scope.page = {
        title: 'List Our DNA',
        subtitle: 'Our DNA'
    };
    $rootScope.loading = true;
    var vm = this;
    vm.message = '';
    $scope.typer = "our-dna";
    $http.get($scope.baseUrl + "admin/gallery")
        .success(function(result) {
            $scope.rowBlogNews = result;
        })
        .then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            })
        });
    $scope.del = function(id, index, filename) {
        if (confirm("Relly Delete This Item ?") === true) {
            $http.get('admin/gallery/delete/' + filename + '/' + id)
                .success(function(result) {
                    $scope.flash = "Delete Success";
                    // $scope.rowBlogNews.splice(index,1);
                    $scope.$state.go($state.current, {}, { reload: true });
                });
        }
    }
})

.controller('blogJoinListTbl', function($rootScope, $timeout, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'List Join Us',
            subtitle: 'Join Us'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.blog.join.create", { 'type': "join" })
        };
        $scope.typer = "our-dna";

        $rootScope.loading = true;
        $http.get($scope.baseUrl + "blog/show/5")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                })
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('blogListInfoTbl', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'List Info',
            subtitle: 'Info'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {

            $state.go("app.blog.news.create", { 'type': "news" })
        };
        $scope.typer = "info";
        $http.get($scope.baseUrl + "blog/show/1")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;
            });
    })
    .controller('blogListWorldTbl', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'List Blog Lexus World',
            subtitle: 'Lexus World'
        };

        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {

            $state.go("app.blog.news.create", { 'type': "world" })
        };
        $scope.typer = "world";
        $http.get($scope.baseUrl + "blog/show/3")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;

            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('blogListLifeTbl', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'List Blog Lifestyle',
            subtitle: 'Lifestyle'
        };

        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {

            $state.go("app.blog.news.create", { 'type': "life" })
        };
        $scope.typer = "life";
        $http.get($scope.baseUrl + "blog/show/2")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogNews = result;
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/blog/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, { reload: true });
                        $scope.$state.go('app.blog.lifestyle');
                    });
            }
        }
    })
    .controller('memberListTbl', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
        $scope.page = {
            title: 'Member List',
            subtitle: 'Member list'
        };

        $http.get($scope.baseUrl + "member/show")
            .success(function(result) {
                console.log(result);
                $scope.rowBlogNews = result;
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Member ?") === true) {
                $http.get('admin/account/delete' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";

                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('adminListTbl', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams, toastr, toastrConfig) {
        $scope.page = {
            title: 'Admin List',
            subtitle: 'Admin list'
        };

        $http.get($scope.baseUrl + "admin/show")
            .success(function(result) {
                console.log(result);
                $scope.rowBlogNews = result;
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Member ?") === true) {
                $http.get('admin/admin/delete/' + id)
                    .success(function(data) {
                        $scope.flash = "Delete Success";

                        $scope.$state.go($state.current, {}, { reload: true });
                        if (data.t == 1) {
                            var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                                iconClass: 'bg-success',
                                iconType: "fa-check"
                            });
                        } else {
                            var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                                iconClass: 'bg-warning',
                                iconType: "fa-warning"
                            });
                        }
                    });
            }
        }
    })
    .controller('threadListTbl', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
        $scope.list = $stateParams.type;
        $scope.page = {
            title: 'Thread List',
            subtitle: $scope.list
        };

        if ($scope.list == "marketplace") {
            var typera = "1";
        } else if ($scope.list == "webboard") {
            var typera = "2";
        }
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("app.blog.news.create", { 'type': "news" })
        };

        $scope.typer = "news";
        $http.get($scope.baseUrl + "thread/show/" + typera)
            .success(function(result) {
                console.log(result);
                $scope.rowBlogNews = result;
            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Thread ?") === true) {
                $http.get('admin/account/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";

                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    })
    .controller('memberDetail', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {

        $scope.list = $stateParams.id;
        $scope.page = {
            title: 'Member Detail',
            subtitle: "Member"
        };
        var vm = this;
        vm.message = '';

        $scope.back = function() {
            $scope.$state.go("app.member.show");
        }

        $scope.typer = "news";
        $http.get($scope.baseUrl + "api/authenticate/get_user/" + $scope.list + "/0")
            .success(function(result) {
                console.log(result);
                $scope.rowThreadDetail = result;
            });
    })
    .controller('threadDetail', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
        $scope.list = $stateParams.id;
        $scope.list_a = $stateParams.type;
        $scope.page = {
            title: 'Detail Thread',
            subtitle: "Thread"
        };
        var vm = this;
        vm.message = '';

        $scope.back = function() {
            if ($scope.list_a == 1) {
                $scope.$state.go("app.thread.show", { 'type': "marketplace" });
            } else if ($scope.list_a == 2) {
                $scope.$state.go("app.thread.show", { 'type': 'webboard' });

            }
        }

        $scope.typer = "news";
        $http.get($scope.baseUrl + "thread/detail/" + $scope.list)
            .success(function(result) {
                console.log(result);
                $scope.rowThreadDetail = result;
            });
    })
    .controller('threadComment', function($scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
        $scope.list = $stateParams.id;
        $scope.list_a = $stateParams.type;
        var vm = this;
        vm.message = '';

        $scope.back = function() {
            if ($scope.list_a == 1) {
                $scope.$state.go("app.thread.show", { 'type': "marketplace" });
            } else if ($scope.list_a == 2) {
                $scope.$state.go("app.thread.show", { 'type': 'webboard' });
            } else if ($scope.list_a == 3) {
                $scope.$state.go("app.blog.news");
            }
        }

        $scope.typer = "news";
        if ($scope.list_a == 3) {
            $http.get($scope.baseUrl + "blog/edit/" + $scope.list)
                .success(function(result) {
                    console.log(result);
                    $scope.titleThread = result[0].pd_title;
                });
            $scope.listType = "3";
        } else {
            $scope.listType = "1";
            $http.get($scope.baseUrl + "thread/detail/" + $scope.list)
                .success(function(result) {
                    console.log(result);
                    $scope.titleThread = result[0].thread_title;
                });

        }

        $http.get($scope.baseUrl + "thread/getComment2/" + $scope.list + "/" + $scope.list_a)
            .success(function(result) {
                console.log(result);
                $scope.rowBlogNews = result;

            });
        $scope.del = function(id, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/comment/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);

                        $scope.$state.go($state.current, {}, { reload: true });
                    });
            }
        }
    });
