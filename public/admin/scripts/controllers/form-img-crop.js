'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormImgCropCtrl
 * @description
 * # FormImgCropCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('FormImgCropCtrl', function ($scope) {
    $scope.myImage='';
    $scope.myCroppedImage='';
    $scope.cropType='circle';

    var handleFileSelect=function(evt) {
      console.log(evt);
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          item.image=evt.target.result;
      console.log($scope.myImage);
        });
      };
      reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
    console.log(angular.element(document.querySelector('#fileInput')));
  });
