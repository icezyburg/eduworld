'use strict';

angular.module('minovateApp')
    .controller('FormWizardCtrl', function($scope) {
        $scope.page = {
            title: 'Form Wizard',
            subtitle: 'Place subtitle here...'
        };
    })


.controller('aboutListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
    $scope.page = {
        title: 'About',
        subtitle: 'About Content'
    };
    var vm = this;
    vm.message = '';
    $scope.addLink = function() {
        $state.go("app.about.create")
    };
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "post/about")
        .success(function(result) {
            console.log(result);
            // $scope.rowBlogNews=result;
            $scope.rowBlogAbout = result;
        }).then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            });
        });
    $scope.del = function(id, index) {
        if (confirm("Delete This Item ?") === true) {
            $http.get('admin/post/delete/' + id)
                .success(function(result) {
                    $scope.flash = "Delete Success";
                    // $scope.rowBlogNews.splice(index,1);
                    $scope.$state.go($state.current, {}, {
                        reload: true
                    });
                });
        }
    }
})

.controller('dataListCtrl', function($timeout, $stateParams, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource,$auth) {
    $scope.page = {
        title: 'Data',
        subtitle: 'Data Content'
    };
    var vm = this;
    var typeNa = $stateParams.type;
    vm.message = '';
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "data/list/" + typeNa)
        .success(function(result) {
            console.log(result);
            // $scope.rowBlogNews=result;
            $scope.rowData = result;
        }).then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            });
        });
    $scope.export = function(){
        window.open('admin/export/' + typeNa + '?token=' + $auth.getToken());
    }
    $scope.del = function(id, index) {
        if (confirm("Delete This Item ?") === true) {
            $http.get('admin/data/delete/' + id)
                .success(function(result) {
                    $scope.flash = "Delete Success";
                    // $scope.rowBlogNews.splice(index,1);
                    $scope.$state.go($state.current, {}, {
                        reload: true
                    });
                });
        }
    }
})

.controller('DetaildataCtrl', function($timeout, $stateParams, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Detail Data',
            subtitle: 'Detail Data Content'
        };
        var vm = this;
        var id = $stateParams.id;
        vm.message = '';
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "data/detail/" + id)
            .success(function(result) {
                console.log(result);
                $scope.rowData = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/data/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
    .controller('servicesListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Services',
            subtitle: 'Services Content'
        };
        var vm = this;
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.services.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/services")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogAbout = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
    .controller('servicesCategoryListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
        $scope.page = {
            title: 'Services',
            subtitle: 'Services Content'
        };
        var id = $stateParams.id;
        var vm = this;
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.services.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/services/category/" + id)
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogAbout = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
    .controller('testimonialListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Testimonial',
            subtitle: 'Testimonial Content'
        };
        var vm = this;
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.testimonial.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/testimonial")
            .success(function(result) {
                console.log(result);
                $scope.rowBlogTestimonial = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
    .controller('createAdmin', ['$rootScope','$scope','$http','$timeout','$filter','$stateParams','$state', 'toastr','toastrConfig',function ($rootScope,$scope,$http,$timeout,$filter,$stateParams,$state,toastr,toastrConfig) {
        $scope.register_process = function()
        {
            console.log("start logging");
            $rootScope.loading = true;

                var dataRegister = {
                  username : $scope.td.username,
                  fname : $scope.td.fname,
                  lname : $scope.td.lname,
                  password : $scope.td.password,
                  email : $scope.td.email,
                };
            
                $http.post($scope.baseUrl+'account/admin', JSON.stringify(dataRegister)).success(function (data) {
                    $rootScope.loading = false;
                    $scope.msg = data.status;
                    
                    if(data.t == 1)
                    {
                      var toast = toastr[$scope.optionsToast.type]( "Saved","Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                      });
                      scope.$state.go("app.admin.show", {}, {reload: true});           
                    }
                    else
                    {
                      var toastFail = toastr[$scope.optionsToast.type]( "Internal Server Error , Please Try Again","Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                      });
                    }
                }).error(function (data) {
                    var toastFail = toastr[$scope.optionsToast.type]( "Internal Server Error , Please Try Again","Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                      });
                });
              };
          
        
    }])
    .controller('changePass', ['$rootScope','$scope','$http','$timeout','$filter','$stateParams','$state', 'toastr','toastrConfig',function ($rootScope,$scope,$http,$timeout,$filter,$stateParams,$state,toastr,toastrConfig) {
  $scope.username = $stateParams.user;
    $scope.register_process = function()
    {
        $rootScope.loading = true;

            var dataRegister = {
              user_id : $stateParams.id,
              password : $scope.td.currentpassword,
              newpassword : $scope.td.confirm_password,
            }

            $http.post($scope.baseUrl+'account/changepasswordadm', JSON.stringify(dataRegister)).success(function (data) {
                $rootScope.loading = false;
                $scope.msg = data.status;
                console.log(data);
                if(data.t == 1)
                {
                  var toast = toastr[$scope.optionsToast.type]( "Saved","Notification", {
                    iconClass: 'bg-success',
                    iconType: "fa-check"
                  });
                  $scope.$state.go("app.admin.show", {}, {reload: true});               
                }
                else
                {
                  var toastFail = toastr[$scope.optionsToast.type]( data.status,"Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                  });
                }
            }).error(function (data) {
                var toastFail = toastr[$scope.optionsToast.type]( "Internal Server Error , Please Try Again","Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                  });
            });
          }
      
    
}])
    .controller('downloadListTbl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Download',
            subtitle: 'Download Content'
        };
        var vm = this;
        vm.message = '';
        $scope.creteLink = function() {
            $state.go("download.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "download/show")
            .success(function(result) {
                console.log(result);
                // $scope.rowBlogNews=result;
                $scope.rowBlogDownload = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, file, index) {
            if (confirm("Relly Delete This Item ?") === true) {
                $http.get('admin/download/delete/' + file + '/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
    .controller('pageListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Page',
            subtitle: 'Page Content'
        };
        var vm = this;
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.page.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/page")
            .success(function(result) {
                console.log(result);
                $scope.rowBlogAbout = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })

.controller('videoListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Video',
            subtitle: 'Video Content'
        };
        var vm = this;
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.video.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/blog/category/44")
            .success(function(result) {
                console.log(result);
                $scope.rowBlog = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
.controller('postListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
        $scope.page = {
            title: 'Post',
            subtitle: 'Post Content'
        };
        var vm = this;
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.post.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/blog")
            .success(function(result) {
                console.log(result);
                $scope.rowBlog = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })
    .controller('postCategoryListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
        $scope.page = {
            title: 'Post',
            subtitle: 'Post Content'
        };
        var vm = this;
        var id = $stateParams.id
        vm.message = '';
        $scope.addLink = function() {
            $state.go("app.post.create")
        };
        $rootScope.loading = true;
        $http.get($scope.baseUrl + "post/blog/category/" + id)
            .success(function(result) {
                console.log(result);
                $scope.rowBlog = result;
            }).then(function() {
                $timeout(function() {
                    $rootScope.loading = false;
                });
            });
        $scope.del = function(id, index) {
            if (confirm("Delete This Item ?") === true) {
                $http.get('admin/post/delete/' + id)
                    .success(function(result) {
                        $scope.flash = "Delete Success";
                        // $scope.rowBlogNews.splice(index,1);
                        $scope.$state.go($state.current, {}, {
                            reload: true
                        });
                    });
            }
        }
    })


.controller('albumListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
    $scope.page = {
        title: 'Album',
        subtitle: 'Album Gallery'
    };
    var vm = this;
    vm.message = '';
    $scope.addLink = function() {
        $state.go("app.album.create")
    };
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "post/album")
        .success(function(result) {
            console.log(result);
            $scope.rowAlbum = result;
        }).then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            });
        });
    $scope.del = function(id, index) {
        if (confirm("Delete This Album ?") === true) {
            $http.get('admin/category/delete/' + id)
                .success(function(result) {
                    $scope.flash = "Delete Success";
                    // $scope.rowBlogNews.splice(index,1);
                    $scope.$state.go($state.current, {}, {
                        reload: true
                    });
                });
        }
    }
})

.controller('albumCreateCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Album',
        subtitle: 'Add New Album'
    };

    $http.get($scope.baseUrl + "post/parent_album")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });
    
    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = evt.target.result;
                // $scope.myImage2=evt.target.result;
                // $scope.myImage3=evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                name: $scope.category.name,
                parent_id: $scope.category.parent_category,
                thumbnail: $(".thumbnail").attr("value"),
                type: 4,
                visibility: $scope.category.visibility,
                metakeyword: $scope.category.metakey,
                metadecription: $scope.category.metades

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/category/create', JSON.stringify(putdatablog)).success(function(data) {
                uploader.uploadAll();
                $rootScope.loading = false;
                $scope.persons = data;
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $scope.$state.go('app.album');

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('EditalbumCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Album',
        subtitle: 'Album Content'
    };


    $http.get($scope.baseUrl + "post/parent_album")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });
    $rootScope.loading = true;
    $scope.blog = [];
    $scope.statusId = $stateParams.albumID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "post/category/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            $scope.category.name = result[0].cd_name;
            $scope.parent_category = result[0].parent_id;
            $scope.category.type = result[0].type_id;
            $scope.category.metakey = result[0].cd_meta_keyword;
            $scope.category.metades = result[0].cd_meta_description;
            $scope.category.visibility = result[0].status;
            $scope.thumbnail = result[0].image;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {

                $scope.thumbnail = '';
                angular.element('.prevSblm').hide();
                $scope.thumbnail_value = evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];



    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });


    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                name: $scope.category.name,
                parent_id: $scope.category.parent_category,
                type: 4,
                visibility: $scope.category.visibility,
                metakeyword: $scope.category.metakey,
                thumbnail: $(".thumbnail").attr("value"),
                metadecription: $scope.category.metades,
                category_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/category/update', JSON.stringify(putdatablog)).success(function(data) {
                uploader.uploadAll();
                $scope.persons = data;
                console.log(data);
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });


                    $scope.$state.go('app.album');


                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])





.controller('downloadCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Download',
        subtitle: 'Download Content'
    };

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });
    var uploader2 = $scope.uploader2 = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });



    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll Uploader 1', addedFileItems);
    };

    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.myLogo = evt.target.result;
                item.formData.push({
                    imag2: $scope.myLogo
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.myLogo);
        reader.readAsDataURL(item._file);
    };
    uploader2.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll Uploader 2', addedFileItems);
    };

    uploader2.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.mycover = evt.target.result;
                $scope.mycoverPreview = evt.target.result;
                item.formData.push({
                    imag3: $scope.mycover
                });
            });
        };
        console.log("uploader2" + item._file);
        console.log("uploader2" + $scope.mycover);
        reader.readAsDataURL(item._file);
    };

    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }


    /**
        * Converts data uri to Blob. Necessary for uploading.
        * @see
        *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
        * @param  {String} dataURI
        * @return {Blob}
        */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    // FILTERS


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader2.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    // uploader.onAfterAddingAll = function(addedFileItems) {
    //   console.info('onAfterAddingAll', addedFileItems);
    // };


    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }


    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $http.get($scope.baseUrl + "post/category/5")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });
    $http.get($scope.baseUrl + "setting/download_category")
        .success(function(result) {
            if (result == '1') {
                angular.element('.categoryForm').show();
                console.log(result);
            } else {
                angular.element('.categoryForm').hide();
                console.log(result);
            }
        });
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";

    $timeout(function() {
        $scope.blog.author = "Admin"
    });

    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {

            var putdatablog = {
                title: $scope.blog.title_blog,
                author: 'Admin',
                content: 'Download File',
                category: $scope.blog.category,
                excerpt: $scope.blog.pd_excerpt,
                type: 5,
                visibility: $scope.blog.visibility_blog,
                imag2: $(".logo").attr("value"),
                pagetitle: $scope.blog.pagetitle,
                imag3: $(".cover").attr("value")

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/download/create', JSON.stringify(putdatablog)).success(function(data) {
                $rootScope.loading = false;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });
                    $scope.$state.go('download.show');

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('EditdownloadCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', '$timeout', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig, $timeout) {
    $scope.page = {
        title: 'Edit Download Content',
        subtitle: 'Download Content'
    };
    $rootScope.loading = true;
    $scope.blog = [];
    $scope.list = $stateParams.type;

    $scope.statusId = $stateParams.blogID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "download/show/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            // $scope.rowBlogNews=result;
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;

            console.log(arrayvar.a);
            $scope.blog.title_blog = result[0].pd_title;
            $scope.blog.pd_excerpt = parseInt(result[0].pd_excerpt);
            $scope.blog.category = result[0].post_categories_id;
            $scope.blog.visibility_blog = result[0].post_status;
            $scope.blog.pagetitle = result[0].pd_pagetitle;
            $scope.blog.post_thumbnail3 = result[0].post_thumbnail3;

            //$scope.myImage2='/api/image/blog/'+result[0].post_thumbnail2;
            // $scope.myLogo='/api/image/blog/'+result[0].post_thumbnail2;
            $scope.mycoverPreview = '/api/image/blog/' + result[0].post_thumbnail3;
            $scope.rowBlogDownload = result;
            $rootScope.loading = false;
        });
    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });
    var uploader2 = $scope.uploader2 = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });



    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll Uploader 1', addedFileItems);
    };

    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.myLogo = evt.target.result;
                item.formData.push({
                    imag2: $scope.myLogo
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.myLogo);
        reader.readAsDataURL(item._file);
    };
    uploader2.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll Uploader 2', addedFileItems);
    };

    uploader2.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.mycover = evt.target.result;
                $scope.mycoverPreview = evt.target.result;
                item.formData.push({
                    imag3: $scope.mycover
                });
            });
        };
        console.log("uploader 2" + item._file);
        console.log("uploader 2" + $scope.mycover);
        reader.readAsDataURL(item._file);
    };

    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }


    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    // FILTERS


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    uploader2.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    // uploader.onAfterAddingAll = function(addedFileItems) {
    //   console.info('onAfterAddingAll', addedFileItems);
    // };


    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        headers: {
            'X-XSRF-TOKEN': token
        },
        height: '300',
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }


    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $http.get($scope.baseUrl + "post/category/5")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });

    $http.get($scope.baseUrl + "setting/download_category")
        .success(function(result) {
            if (result == '1') {
                angular.element('.categoryForm').show();
                console.log(result);
            } else {
                angular.element('.categoryForm').hide();
                console.log(result);
            }
        });

    $scope.tabIndex = 0;
    $scope.buttonLabel = "";

    $timeout(function() {
        $scope.blog.author = "Admin"
    });

    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {

            var putdatablog = {
                title: $scope.blog.title_blog,
                author: 'Admin',
                content: 'Download File',
                category: $scope.blog.category,
                type: 5,
                excerpt: $scope.blog.pd_excerpt,
                visibility: $scope.blog.visibility_blog,
                imag2: $(".logo").attr("value"),
                imag3: $(".cover").attr("value"),
                pagetitle: $scope.blog.pagetitle,
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/download/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });



                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }

}])



.controller('categoryListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource) {
    $scope.page = {
        title: 'Category',
        subtitle: 'Category Content'
    };
    var vm = this;
    vm.message = '';
    
    $scope.addLink = function() {
        $state.go("app.category.create")
    };
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "post/category")
        .success(function(result) {
            console.log(result);
            $scope.rowBlogCategory = result;
        }).then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            });
        });
    $scope.del = function(id, index) {
        if (confirm("Delete This Item ?") === true) {
            $http.get('admin/category/delete/' + id)
                .success(function(result) {
                    $scope.flash = "Delete Success";
                    // $scope.rowBlogNews.splice(index,1);
                    $scope.$state.go($state.current, {}, {
                        reload: true
                    });
                });
        }
    }
})
.controller('categoryCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Category',
        subtitle: 'Add New Category'
    };

    $http.get($scope.baseUrl + "post/parent_category")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });
    $http.get($scope.baseUrl + "post/type")
        .success(function(result) {
            console.log(result);
            $scope.type_list = result;
        });
    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = evt.target.result;
                // $scope.myImage2=evt.target.result;
                // $scope.myImage3=evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                name: $scope.category.name,
                parent_id: $scope.category.parent_category,
                thumbnail: $(".thumbnail").attr("value"),
                type: $scope.category.type,
                visibility: $scope.category.visibility,
                metakeyword: $scope.category.metakey,
                metadecription: $scope.category.metades

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/category/create', JSON.stringify(putdatablog)).success(function(data) {
                uploader.uploadAll();
                $rootScope.loading = false;
                $scope.persons = data;
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $scope.$state.go('app.category');

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])
.controller('EditcategoryCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Category',
        subtitle: 'Category Content'
    };


    $http.get($scope.baseUrl + "post/parent_category")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });
    $http.get($scope.baseUrl + "post/type")
        .success(function(result) {
            console.log(result);
            $scope.type_list = result;
        });
    $rootScope.loading = true;
    $scope.blog = [];
    $scope.statusId = $stateParams.categoryID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "post/category/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            $scope.category.name = result[0].cd_name;
            $scope.parent_category = result[0].parent_id;
            $scope.category.type = result[0].type_id;
            $scope.category.metakey = result[0].cd_meta_keyword;
            $scope.category.metades = result[0].cd_meta_description;
            $scope.category.visibility = result[0].status;
            $scope.thumbnail = result[0].image;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {

                $scope.thumbnail = '';
                angular.element('.prevSblm').hide();
                $scope.thumbnail_value = evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];



    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });


    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                name: $scope.category.name,
                parent_id: $scope.category.parent_category,
                type: $scope.category.type,
                visibility: $scope.category.visibility,
                metakeyword: $scope.category.metakey,
                thumbnail: $(".thumbnail").attr("value"),
                metadecription: $scope.category.metades,
                category_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/category/update', JSON.stringify(putdatablog)).success(function(data) {
                uploader.uploadAll();
                $scope.persons = data;
                console.log(data);
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });


                    $scope.$state.go('app.category');


                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])

.controller('galleryListCtrl', function($timeout, $rootScope, $scope, $http, DTOptionsBuilder, DTColumnBuilder, $state, $resource, $stateParams) {
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(id) {
    var idx = $scope.selection.indexOf(id);

    // Is currently selected
    if (idx > -1) {
      $scope.selection.splice(idx, 1);
    }

    // Is newly selected
    else {
      $scope.selection.push(id);
    }

    console.log($scope.selection);
  };
    $scope.selectAll = false;
    $scope.selectAllN = function selectAllN() {
        if($scope.selectAll == false){
            $scope.selection = [];
            angular.element('input[type=checkbox]').click();
        }else{
            $scope.selection = [];
        }
        
        $scope.selectAll = !$scope.selectAll;
    };
    $scope.page = {
        title: 'Gallery',
        subtitle: 'Gallery Content'
    };
    var vm = this;
    vm.message = '';
        
    $scope.albumId = $stateParams.albumID;
    console.log( $stateParams );
    $scope.addLink = function() {
        $state.go("app.gallery.create", {'albumID':$scope.albumId});
    };
    $scope.bulkLink = function() {
        $state.go("app.gallery.bulk_create", {'albumID':$scope.albumId});
    };
    $scope.bulkDelete = function() {
        var arryNaSelection = {id: $scope.selection};
        var config = 1;
        console.log(arryNaSelection);
        $http.post($scope.baseUrl + 'admin/gallery/bulk_delete/?id='+$scope.selection)
            .success(function(result) {
                $scope.flash = "Delete Success";
                // $scope.rowBlogNews.splice(index,1);
                $scope.$state.go($state.current, {}, {
                    reload: true
                });
            });
    };
    
    
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "post/gallery/category/"+$scope.albumId)
        .success(function(result) {
            console.log(result);
            $scope.rowBlogGallery = result;
        }).then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            });
        });
    $scope.del = function(id, filename, index) {
        if (confirm("Delete This Item ?") === true) {
            $http.get('admin/gallery/delete/' + filename + '/' + id)
                .success(function(result) {
                    $scope.flash = "Delete Success";
                    // $scope.rowBlogNews.splice(index,1);
                    $scope.$state.go($state.current, {}, {
                        reload: true
                    });
                });
        }
    }
})
.controller('galleryCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {

    $scope.albumId = $stateParams.albumID;
    
    $scope.page = {
        title: 'Gallery',
        subtitle: 'Gallery Content'
    };

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

    $scope.myCroppedImage1 = "";
    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = evt.target.result;
                item.formData.push({
                    name: $scope.thumbnail
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.thumbnail);
        reader.readAsDataURL(item._file);
    };



    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        headers: {
            'X-XSRF-TOKEN': token
        },
        height: '300',
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }
    $scope.lang_admin = [];


    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    var vm = this;
    $scope.lang_admin = [];
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {

        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                author: "Admin",
                category: $scope.albumId,
                type: 4,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadescription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: "",
                imag3: "",

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/gallery/create', JSON.stringify(putdatablog)).success(function(data) {
                uploader.uploadAll();
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $scope.$state.go('app.gallery', { 'albumID': $scope.albumId });

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('EditgalleryCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Gallery Content',
        subtitle: 'Gallery Content'
    };


    $http.get($scope.baseUrl + "post/category/type/4")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });

    $rootScope.loading = true;
    $scope.blog = [];

    $scope.statusId = $stateParams.galleryID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "post/gallery/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            // $scope.rowBlogNews=result;
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;
            var tags_raw = result[0].pd_meta_keyword.split(",");
            // tags_raw.forEach(function(data)
            // {
            //   a["key"] = data;
            // });

            // arrayvar.a.push(a);

            console.log(arrayvar.a);
            $scope.blog.title_blog = result[0].pd_title;
            $scope.blog.content_blog = result[0].pd_content;
            $scope.blog.author = result[0].post_author;
            $scope.blog.category = result[0].post_categories_id;
            $scope.blog.tags_blog = tags_raw;
            $scope.blog.pagetitle = result[0].pd_pagetitle;
            $scope.blog.metades = result[0].pd_meta_description;
            $scope.blog.visibility_blog = result[0].post_status;
            $scope.thumbnail = result[0].image;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {

                $scope.thumbnail = '';
                angular.element('.prevSblm').hide();
                $scope.thumbnail_value = evt.target.result;
            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        headers: {
            'X-XSRF-TOKEN': token
        },
        height: '300',
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];



    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });


    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                author: $scope.blog.author,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadescription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: '',
                imag3: '',
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/gallery/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])

.controller('galleryBulkCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function ($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    
    $scope.albumId = $stateParams.albumID;
    
    $scope.page = {
        title: 'Gallery'
        , subtitle: 'Gallery Content'
    };

    $http.get($scope.baseUrl + "post/category/type/4")
        .success(function (result) {
            console.log(result);
            $scope.category_list = result;
        });
    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create'
        , withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });


    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);
    $scope.imagePage = [];
    $scope.myCroppedImage1 = "";
    uploader.onAfterAddingFile = function (item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.image = evt.target.result;
                $scope.imagePage.push({
                    name: $scope.image
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.thumbnail);
        reader.readAsDataURL(item._file);
    };



    var base64ToBinary = function (base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function (item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function (dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };


    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    $scope.froalaOptions1 = {
        inlineMode: false
        , theme: 'custom'
        , headers : { 'X-XSRF-TOKEN' : token }
        , height: '300'
        , fileUploadURL: 'upload_file'
        , fileUploadParam: 'upload_param'
        , imageUploadURL: 'upload_image'
        , imageUploadParam: 'image_param'
        , imageUploadParams: {
            id: 'my_editor'
        }
        , events: {
            align: function (e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }
    $scope.lang_admin = [];


    $http.get($scope.baseUrl + "language/show")
        .success(function (result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    //  $scope.filemanage = function(type = 'image', input_id, preview_id) {
    //
    //        if (type === 'image' || type === 'images') {
    //            type = 'Images';
    //        } else {
    //            type = 'Files';
    //        }
    //
    //
    //      localStorage.setItem('target_input', input_id);
    //      localStorage.setItem('target_preview', preview_id);
    //      window.open('/laravel-filemanager?type=' + type, 'FileManager', 'width=900,height=600');
    //      return false;
    //    };
    //  $scope.SetUrl = function (url) {
    //      //set the value of the desired input to image url
    //      let target_input = $('#' + localStorage.getItem('target_input'));
    //      target_input.val(url);
    //
    //      //set or change the preview image src
    //      let target_preview = $('#' + localStorage.getItem('target_preview'));
    //      target_preview.attr('src', url);
    //  };
    var vm = this;
    $scope.lang_admin = [];
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function (isValid, data) {

        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                title: $scope.blog.title_blog
                , content: $scope.blog.title_blog
                , author: "Admin"
                , category: $scope.albumId
                , type: 4
                , tags: $scope.blog.title_blog
                , pagetitle: $scope.blog.title_blog
                , metadescription: $scope.blog.title_blog
                , visibility: $scope.blog.visibility_blog
                , imag1: $scope.imagePage
                , imag2: ""
                , imag3: "",

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/gallery/bulk_create', JSON.stringify(putdatablog)).success(function (data) {
                uploader.uploadAll();
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success'
                        , iconType: "fa-check"
                    });

                    $scope.$state.go('app.gallery', {'albumID': $scope.albumId});

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning'
                        , iconType: "fa-warning"
                    });
                }
            }).error(function (data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning'
                    , iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function ($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function ($index) {
        if ($index == 0) {
            return true;
        }
    }
}])


.controller('testimonialCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Testimonial',
        subtitle: 'Testimonial Content'
    };

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

    $scope.myCroppedImage1 = "";
    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = evt.target.result;
                item.formData.push({
                    name: $scope.thumbnail
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.thumbnail);
        reader.readAsDataURL(item._file);
    };



    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };


    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $scope.cropType = 'circle';

    $scope.image = {
        originalInfo: {},
        cropInfo: {
            "width": 800,
            "height": 500,
            "x": 0,
            "y": 0,
        }
    };

    var vm = this;
    $scope.lang_admin = [];
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                title: 'Testimonial ' + $scope.blog.author,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: $scope.blog.author,
                category: $scope.blog.category,
                type: 6,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: "",
                imag3: "",

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/create', JSON.stringify(putdatablog)).success(function(data) {
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $scope.$state.go('app.testimonial');

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('aboutCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'About',
        subtitle: 'About Content'
    };

    $http.get($scope.baseUrl + "post/category/type/2")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });

    $http.get($scope.baseUrl + "setting/about_category")
        .success(function(result) {
            if (result == '1') {
                angular.element('.categoryForm').show();
                console.log(result);
            } else {
                angular.element('.categoryForm').hide();
                console.log(result);
            }
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

    $scope.myCroppedImage1 = "";
    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = evt.target.result;
                item.formData.push({
                    name: $scope.thumbnail
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.thumbnail);
        reader.readAsDataURL(item._file);
    };



    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $scope.cropType = 'circle';

    $scope.image = {
        originalInfo: {},
        cropInfo: {
            "width": 800,
            "height": 500,
            "x": 0,
            "y": 0,
        }
    };

    var vm = this;
    $scope.lang_admin = [];
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: "Admin",
                category: $scope.blog.category,
                type: 2,
                color: $scope.blog.color,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: "",
                imag3: "",

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/create', JSON.stringify(putdatablog)).success(function(data) {
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $scope.$state.go('app.about.edit', {blogID:data.id});

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('servicesCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Services',
        subtitle: 'Services Content'
    };
    $scope.toggle = function(scope) {
        scope.toggle();
    };
    $http.get($scope.baseUrl + "post/category/type/7")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;

        });

    $scope.tgleColor = function(id) {
        if (id == 19) {
            angular.element('.clorPick').show();
        } else {
            angular.element('.clorPick').hide();
        }
    };
    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

    $scope.myCroppedImage1 = "";
    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = evt.target.result;
                item.formData.push({
                    name: $scope.thumbnail
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.thumbnail);
        reader.readAsDataURL(item._file);
    };



    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $scope.cropType = 'circle';

    $scope.image = {
        originalInfo: {},
        cropInfo: {
            "width": 800,
            "height": 500,
            "x": 0,
            "y": 0,
        }
    };

    var vm = this;
    $scope.lang_admin = [];
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: "Admin",
                category: $scope.blog.category,
                color: $scope.blog.color,
                type: 7,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: "",
                imag3: "",

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/create', JSON.stringify(putdatablog)).success(function(data) {
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });
                    var idNaih = data.id;
                    $scope.$state.go('app.services.edit', {blogID:idNaih});

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('pageCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Page',
        subtitle: 'Page Content'
    };

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false, // autoUpload: true,
        method: 'POST', // removeAfterUpload: true,
        // headers: {'Content-Type': undefined },
        // withCredentials:true,
        // removeAfterUpload:true,

    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);
    $scope.imagePage = [];
    $scope.myCroppedImage1 = "";
    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.image = evt.target.result;
                $scope.imagePage.push({
                    name: $scope.image
                });
            });
        };
        console.log("uploader" + item._file);
        console.log("uploader" + $scope.thumbnail);
        reader.readAsDataURL(item._file);
    };



    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };


    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor'
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });
    $scope.cropType = 'circle';

    $scope.image = {
        originalInfo: {},
        cropInfo: {
            "width": 800,
            "height": 500,
            "x": 0,
            "y": 0,
        }
    };

    var vm = this;
    $scope.lang_admin = [];
    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: "Admin",
                category: 0,
                type: 3,
                pagetype: $scope.blog.type,
                tags: $scope.blog.tags_blog,
                pagetitle: '',
                metadescription: '',
                visibility: 'hidden',
                imag1: $scope.imagePage,
                imag2: "",
                imag3: "",

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/page/create', JSON.stringify(putdatablog)).success(function(data) {
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $scope.$state.go('app.page');

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('EdittestimonialCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Testimonial Content',
        subtitle: 'Testimonial Content'
    };


    $rootScope.loading = true;
    $scope.blog = [];

    $scope.statusId = $stateParams.blogID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "post/testimonial/" + $scope.statusId)
        .success(function(result) {
            console.log(result);

            $scope.blog.content_blog = result[0].pd_content;
            $scope.blog.excerpt = result[0].pd_excerpt;
            $scope.blog.author = result[0].post_author;
            $scope.blog.category = result[0].post_categories_id;
            $scope.blog.pagetitle = result[0].pd_pagetitle;
            $scope.blog.metades = result[0].pd_meta_description;
            $scope.blog.visibility_blog = result[0].post_status;
            $scope.thumbnail = result[0].image;
            $scope.rowBlogNews = result;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {

                $scope.thumbnail = '';
                angular.element('.prevSblm').hide();
                $scope.thumbnail_value = evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];



    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });


    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                title: 'Testimonial ' + $scope.blog.author,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: $scope.blog.author,
                category: $scope.blog.category,
                tags: [{
                    'text': "Testimonial"
                }, {
                    'text': "Content"
                }],
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: '',
                imag3: '',
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });


                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])

.controller('EditaboutCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit About Content',
        subtitle: 'About Content'
    };


    $http.get($scope.baseUrl + "post/category/type/2")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });

    $http.get($scope.baseUrl + "setting/about_category")
        .success(function(result) {
            if (result == '1') {
                angular.element('.categoryForm').show();
                console.log(result);
            } else {
                angular.element('.categoryForm').hide();
                console.log(result);
            }
        });
    $rootScope.loading = true;
    $scope.blog = [];

    $scope.statusId = $stateParams.blogID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "post/about/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            // $scope.rowBlogNews=result;
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;
            var tags_raw = result[0].pd_meta_keyword.split(",");
            // tags_raw.forEach(function(data)
            // {
            //   a["key"] = data;
            // });

            // arrayvar.a.push(a);

            console.log(arrayvar.a);
            $scope.blog.title_blog = result[0].pd_title;
            $scope.blog.content_blog = result[0].pd_content;
            $scope.blog.excerpt = result[0].pd_excerpt;
            $scope.blog.author = result[0].post_author;
            $scope.blog.category = result[0].post_categories_id;
            $scope.blog.tags_blog = tags_raw;
            $scope.blog.pagetitle = result[0].pd_pagetitle;
            $scope.blog.metades = result[0].pd_meta_description;
            $scope.blog.visibility_blog = result[0].post_status;
            $scope.thumbnail = result[0].image;
            $scope.blog.color = result[0].additional;
            $scope.rowBlogNews = result;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {

                $scope.thumbnail = '';
                angular.element('.prevSblm').hide();
                $scope.thumbnail_value = evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];



    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });


    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: $scope.blog.author,
                category: $scope.blog.category,
                tags: $scope.blog.tags_blog,
                color: $scope.blog.color,
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: '',
                imag3: '',
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });


                    // $scope.$state.go('app.about');


                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])







.controller('EditservicesCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Services Content',
        subtitle: 'Services Content'
    };


    $http.get($scope.baseUrl + "post/category/type/7")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });

    $rootScope.loading = true;
    $scope.blog = [];

    $scope.tgleColor = function(id) {
        if (id == 19) {
            angular.element('.clorPick').show();
        } else {
            angular.element('.clorPick').hide();
        }
    };
    $scope.statusId = $stateParams.blogID;
    console.log($scope.statusId);
    $http.get($scope.baseUrl + "post/services/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            // $scope.rowBlogNews=result;
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;
            var tags_raw = result[0].pd_meta_keyword.split(",");
            // tags_raw.forEach(function(data)
            // {
            //   a["key"] = data;
            // });

            // arrayvar.a.push(a);

            console.log(arrayvar.a);
            $scope.blog.title_blog = result[0].pd_title;
            $scope.blog.content_blog = result[0].pd_content;
            $scope.blog.excerpt = result[0].pd_excerpt;
            $scope.blog.author = result[0].post_author;
            $scope.blog.category = result[0].post_categories_id;
            $scope.blog.tags_blog = tags_raw;
            $scope.blog.pagetitle = result[0].pd_pagetitle;
            $scope.blog.metades = result[0].pd_meta_description;
            $scope.blog.visibility_blog = result[0].post_status;
            $scope.thumbnail = result[0].image;
            $scope.blog.color = result[0].additional;
            $scope.rowBlogNews = result;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });


    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.thumbnail = '';
                angular.element('.prevSblm').hide();
                $scope.thumbnail_value = evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };


    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }
    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];



    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });


    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: $scope.blog.author,
                category: $scope.blog.category,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                color: $scope.blog.color,
                imag1: $(".thumbnail").attr("value"),
                imag2: '',
                imag3: '',
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });


                    // $scope.$state.go('app.services');


                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])

.controller('blogCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
        $scope.page = {
            title: 'Post',
            subtitle: 'Create New Post'
        };
        $scope.list = $stateParams.type;

        var uploader = $scope.uploader = new FileUploader({
            url: 'admin/blog/create',
            withCredentials: false, // autoUpload: true,
            method: 'POST', // removeAfterUpload: true,
            // headers: {'Content-Type': undefined },
            // withCredentials:true,
            // removeAfterUpload:true,

        });

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(html) {
            // console.info('onErrorItem', fileItem, response, status, headers);
            console.log(html);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        $scope.thumbnailCropped = "";
        uploader.onAfterAddingFile = function(item) {
            // var file=item.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.myImage = evt.target.result;
                    item.formData.push({
                        name: $scope.myImage
                    });
                });
            };
            console.log("uploader" + item._file);
            console.log("uploader" + $scope.myImage);
            reader.readAsDataURL(item._file);
        };

        var base64ToBinary = function(base64EncodedFile) {
            var BASE64_MARKER = ';base64,';
            var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
            var base64 = base64EncodedFile.substring(base64Index);
            var raw = atob(base64);
            var rawLength = raw.length;
            var array = new Uint8Array(rawLength);

            for (i = 0; i < rawLength; i++) {
                array[i] = raw.charCodeAt(i);
            }
            return array.buffer;
        }

        /**
         * Upload Blob (cropped image) instead of file.
         * @see
         *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
         *   https://github.com/nervgh/angular-file-upload/issues/208
         */
        uploader.onBeforeUploadItem = function(item) {
            var blob = dataURItoBlob($scope.myCroppedImage1);
            console.log(blob);
            item._file = blob;
            console.log(item._file);
        };

        /**
         * Converts data uri to Blob. Necessary for uploading.
         * @see
         *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
         * @param  {String} dataURI
         * @return {Blob}
         */

        var dataURItoBlob = function(dataURI) {
            console.log(dataURI);
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {
                type: mimeString
            });
        };


        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };


        $scope.froalaOptions1 = {
            inlineMode: false,
            charCounterMax: 20,
            theme: 'custom',
            height: '300',
            headers: {
                'X-XSRF-TOKEN': token
            },
            fileUploadURL: 'upload_file',
            fileUploadParam: 'upload_param',
            imageUploadURL: 'upload_image',
            imageUploadParam: 'image_param',
            imageUploadParams: {
                id: 'my_editor'
            },
            events: {
                align: function(e, editor, alignment) {
                    console.log(alignment + ' aligned');
                }
            }
        }
        var vm = this;
        $scope.lang_admin = [];

        $http.get($scope.baseUrl + "language/show")
            .success(function(result) {
                console.log(result);
                $scope.lang_admin = result;
            });


        $http.get($scope.baseUrl + "post/category/type/1")
            .success(function(result) {
                console.log(result);
                $scope.category_list = result;
            });

        $scope.tabIndex = 0;
        $scope.buttonLabel = "";

        $scope.proceed = function(isValid, data) {
            if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
                $scope.lang_admin[$scope.tabIndex].active = false;
                $scope.tabIndex++
                    $scope.lang_admin[$scope.tabIndex].active = true;
            }
            console.log($scope.lang_admin.length);


            if ($scope.tabIndex === $scope.lang_admin.length - 1) {
                var putdatablog = {
                    title: $scope.blog.title_blog,
                    content: $scope.blog.content_blog,
                    excerpt: $scope.blog.excerpt,
                    author: $scope.blog.author,
                    category: $scope.blog.category,
                    type: '1',
                    tags: $scope.blog.tags_blog,
                    pagetitle: $scope.blog.pagetitle,
                    metadecription: $scope.blog.metades,
                    visibility: $scope.blog.visibility_blog,
                    imag1: $(".thumbnail").attr("value"),
                    imag2: '',
                    imag3: '',

                };
                console.log(putdatablog);
                $rootScope.loading = true;
                $http.post($scope.baseUrl + 'admin/post/create', JSON.stringify(putdatablog)).success(function(data) {
                    $rootScope.loading = false;
                    $scope.persons = data;
                    console.log(data);
                    uploader.uploadAll();
                    if (data.t == 1) {
                        var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                            iconClass: 'bg-success',
                            iconType: "fa-check"
                        });
                        $scope.$state.go('app.post');

                    } else {
                        var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                            iconClass: 'bg-warning',
                            iconType: "fa-warning"
                        });
                    }
                }).error(function(data) {
                    $scope.status = status;
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                });
            }

        };

        if ($scope.tabIndex == $scope.lang_admin.length) {
            $scope.buttonLabel = "Submit";
        } else {

            $scope.buttonLabel = "Next";
        }

        $scope.setIndex = function($index) {
            $scope.tabIndex = $index;
        }

        $scope.getIndex = function($index) {
            if ($index == 0) {
                return true;
            }
        }
    }])
.controller('videoCtrl', ["$rootScope", '$scope', '$http', 'FileUploader', '$timeout', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $timeout, $stateParams, toastr, toastrConfig) {
        $scope.page = {
            title: 'Video',
            subtitle: 'Create New Video'
        };
        $scope.list = $stateParams.type;

        var uploader = $scope.uploader = new FileUploader({
            url: 'admin/blog/create',
            withCredentials: false, // autoUpload: true,
            method: 'POST', // removeAfterUpload: true,
            // headers: {'Content-Type': undefined },
            // withCredentials:true,
            // removeAfterUpload:true,

        });

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(html) {
            // console.info('onErrorItem', fileItem, response, status, headers);
            console.log(html);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        $scope.thumbnailCropped = "";
        $scope.myImage = "";
        uploader.onAfterAddingFile = function(item) {
            // var file=item.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.myImage = evt.target.result;
                    item.formData.push({
                        name: $scope.myImage
                    });
                });
            };
            console.log("uploader" + item._file);
            console.log("uploader" + $scope.myImage);
            reader.readAsDataURL(item._file);
        };

        var base64ToBinary = function(base64EncodedFile) {
            var BASE64_MARKER = ';base64,';
            var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
            var base64 = base64EncodedFile.substring(base64Index);
            var raw = atob(base64);
            var rawLength = raw.length;
            var array = new Uint8Array(rawLength);

            for (i = 0; i < rawLength; i++) {
                array[i] = raw.charCodeAt(i);
            }
            return array.buffer;
        }

        /**
         * Upload Blob (cropped image) instead of file.
         * @see
         *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
         *   https://github.com/nervgh/angular-file-upload/issues/208
         */
        uploader.onBeforeUploadItem = function(item) {
            var blob = dataURItoBlob($scope.myCroppedImage1);
            console.log(blob);
            item._file = blob;
            console.log(item._file);
        };

        /**
         * Converts data uri to Blob. Necessary for uploading.
         * @see
         *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
         * @param  {String} dataURI
         * @return {Blob}
         */

        var dataURItoBlob = function(dataURI) {
            console.log(dataURI);
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {
                type: mimeString
            });
        };


        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };


        $scope.froalaOptions1 = {
            inlineMode: false,
            charCounterMax: 20,
            theme: 'custom',
            height: '300',
            headers: {
                'X-XSRF-TOKEN': token
            },
            fileUploadURL: 'upload_file',
            fileUploadParam: 'upload_param',
            imageUploadURL: 'upload_image',
            imageUploadParam: 'image_param',
            imageUploadParams: {
                id: 'my_editor'
            },
            events: {
                align: function(e, editor, alignment) {
                    console.log(alignment + ' aligned');
                }
            }
        }
        var vm = this;
        $scope.lang_admin = [];

        $http.get($scope.baseUrl + "language/show")
            .success(function(result) {
                console.log(result);
                $scope.lang_admin = result;
            });

        $scope.tabIndex = 0;
        $scope.buttonLabel = "";

        $scope.proceed = function(isValid, data) {
            if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
                $scope.lang_admin[$scope.tabIndex].active = false;
                $scope.tabIndex++
                    $scope.lang_admin[$scope.tabIndex].active = true;
            }
            console.log($scope.lang_admin.length);


            if ($scope.tabIndex === $scope.lang_admin.length - 1) {
                var putdatablog = {
                    title: $scope.blog.title_blog,
                    content: $scope.blog.content_blog,
                    excerpt: $scope.blog.excerpt,
                    author: 'admin',
                    category: 44,
                    type: '1',
                    tags: [{'text': "Video"}, {'text': "Content"}],
                    pagetitle: '',
                    metadecription: '',
                    visibility: '',
                    imag1: '',
                    imag2: $(".thumbnail").attr("value"),
                    imag3: '',

                };
                console.log(putdatablog);
                $rootScope.loading = true;
                $http.post($scope.baseUrl + 'admin/post/create', JSON.stringify(putdatablog)).success(function(data) {
                    $rootScope.loading = false;
                    $scope.persons = data;
                    console.log(data);
                    uploader.uploadAll();
                    if (data.t == 1) {
                        var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                            iconClass: 'bg-success',
                            iconType: "fa-check"
                        });
                        $scope.$state.go('app.video');

                    } else {
                        var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                            iconClass: 'bg-warning',
                            iconType: "fa-warning"
                        });
                    }
                }).error(function(data) {
                    $scope.status = status;
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                });
            }

        };

        if ($scope.tabIndex == $scope.lang_admin.length) {
            $scope.buttonLabel = "Submit";
        } else {

            $scope.buttonLabel = "Next";
        }

        $scope.setIndex = function($index) {
            $scope.tabIndex = $index;
        }

        $scope.getIndex = function($index) {
            if ($index == 0) {
                return true;
            }
        }
    }])
    .controller('contentCtrl', ['$scope', '$state', '$rootScope', '$http', '$modalInstance', 'toastr', 'items', function($scope, $state, $rootScope, $http, $modalInstance, toastr, items) {
        $scope.title = items.title;
        $scope.description = items.description;
        $scope.link = items.link;
        $scope.id = items.id;

        $scope.froalaOptions1 = {
            inlineMode: false,
            theme: 'custom',
            height: '300',
            headers: {
                'X-XSRF-TOKEN': token
            },
            buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
            fileUploadURL: 'upload_file',
            fileUploadParam: 'upload_param',
            imageUploadURL: 'upload_image',
            imageUploadParam: 'image_param',
            imageUploadParams: {
                id: 'my_editor',
                class: "img-responsive"
            },
            events: {
                align: function(e, editor, alignment) {
                    console.log(alignment + ' aligned');
                }
            }
        }

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.proceed = function(isValid, data) {

            var putdatablog = {
                title: $scope.title,
                description: $scope.description,
                link: $scope.link,
                id: $scope.id

            };
            console.log(putdatablog);
            $http.post($scope.baseUrl + 'admin/page/update_image_content', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                    $modalInstance.close();
                    $state.reload();

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });


        };


    }])
    .controller('EditpageCtrl', ['$rootScope', '$modal', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $modal, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {

        $scope.editcontentgambar = function(img) {
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'contentCtrl',
                resolve: {
                    items: function() {
                        return img;
                    }
                }
            });

        };
        $scope.page = {
            title: 'Edit Page',
            subtitle: 'Page Content'
        };
        $rootScope.loading = true;
        $scope.blog = [];
        $scope.statusId = $stateParams.pageID;
        console.log($scope.statusId);


        $scope.deletegambar = function(img) {
            $http.get($scope.baseUrl + "page/deleteimage/" + img.id + '/' + img.image)
                .success(function(result) {
                    img.hide = true;
                    console.log(result);
                });
        };


        $http.get($scope.baseUrl + "post/category/type/3")
            .success(function(result) {
                console.log(result);
                $scope.category_list = result;
            });

        $http.get($scope.baseUrl + "post/page/" + $scope.statusId)
            .success(function(result) {
                console.log(result);
                var a = new Array();
                var arrayvar = {};
                arrayvar.a = a;
                var tags_raw = result[0].pd_meta_keyword.split(",");

                console.log(arrayvar.a);
                $scope.blog.title_blog = result[0].pd_title;
                $scope.blog.content_blog = result[0].pd_content;
                $scope.blog.excerpt = result[0].pd_excerpt;
                $scope.blog.author = result[0].post_author;
                $scope.blog.category = result[0].post_categories_id;
                $scope.blog.tags_blog = tags_raw;
                $scope.blog.pagetitle = result[0].pd_pagetitle;
                $scope.blog.metades = result[0].pd_meta_description;
                $scope.blog.visibility_blog = result[0].post_status;
                $scope.myImage = result[0].image_list;
                $scope.rowBlogNews = result;
                $rootScope.loading = false;
            });

        var uploader = $scope.uploader = new FileUploader({
            url: 'admin/blog/create',
            withCredentials: false,
            method: 'POST',
        });

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(html) {
            // console.info('onErrorItem', fileItem, response, status, headers);
            console.log(html);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        $scope.imagePage = [];
        uploader.onAfterAddingFile = function(item) {
            // var file=item.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.image = evt.target.result;
                    $scope.imagePage.push({
                        name: $scope.image
                    });

                });
            };
            console.log(item._file);
            reader.readAsDataURL(item._file);
        };

        var base64ToBinary = function(base64EncodedFile) {
            var BASE64_MARKER = ';base64,';
            var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
            var base64 = base64EncodedFile.substring(base64Index);
            var raw = atob(base64);
            var rawLength = raw.length;
            var array = new Uint8Array(rawLength);

            for (i = 0; i < rawLength; i++) {
                array[i] = raw.charCodeAt(i);
            }
            return array.buffer;
        }

        $scope.froalaOptions1 = {
            inlineMode: false,
            theme: 'custom',
            height: '300',
            headers: {
                'X-XSRF-TOKEN': token
            },
            buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
            fileUploadURL: 'upload_file',
            fileUploadParam: 'upload_param',
            imageUploadURL: 'upload_image',
            imageUploadParam: 'image_param',
            imageUploadParams: {
                id: 'my_editor',
                class: "img-responsive"
            },
            events: {
                align: function(e, editor, alignment) {
                    console.log(alignment + ' aligned');
                }
            }
        }

        /**
         * Upload Blob (cropped image) instead of file.
         * @see
         *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
         *   https://github.com/nervgh/angular-file-upload/issues/208
         */
        uploader.onBeforeUploadItem = function(item) {
            var blob = dataURItoBlob($scope.myCroppedImage1);
            console.log(blob);
            item._file = blob;
            console.log(item._file);
        };

        /**
         * Converts data uri to Blob. Necessary for uploading.
         * @see
         *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
         * @param  {String} dataURI
         * @return {Blob}
         */

        var dataURItoBlob = function(dataURI) {
            console.log(dataURI);
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {
                type: mimeString
            });
        };

        //FILTERS

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/ , options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };

        var vm = this;
        $scope.lang_admin = [];

        $http.get($scope.baseUrl + "language/show")
            .success(function(result) {
                console.log(result);
                $scope.lang_admin = result;
            });

        $scope.tabIndex = 0;
        $scope.buttonLabel = "";


        $scope.proceed = function(isValid, data) {
            if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
                $scope.lang_admin[$scope.tabIndex].active = false;
                $scope.tabIndex++
                    $scope.lang_admin[$scope.tabIndex].active = true;
            }
            console.log($scope.lang_admin.length);
            if ($scope.tabIndex === $scope.lang_admin.length - 1) {
                // if (isValid) {
                var putdatablog = {
                    title: $scope.blog.title_blog,
                    content: $scope.blog.content_blog,
                    excerpt: $scope.blog.excerpt,
                    author: $scope.blog.author,
                    category: $scope.blog.category,
                    tags: $scope.blog.tags_blog,
                    pagetitle: $scope.blog.pagetitle,
                    metadecription: $scope.blog.metades,
                    visibility: $scope.blog.visibility_blog,
                    imag1: $scope.imagePage,
                    imag2: '',
                    imag3: '',
                    post_id: $scope.statusId

                };
                console.log(putdatablog);
                $rootScope.loading = true;
                $http.post($scope.baseUrl + 'admin/page/update', JSON.stringify(putdatablog)).success(function(data) {
                    $scope.persons = data;
                    console.log(data);
                    uploader.uploadAll();
                    if (data.t == 1) {
                        var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                            iconClass: 'bg-success',
                            iconType: "fa-check"
                        });

                    } else {
                        var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                            iconClass: 'bg-warning',
                            iconType: "fa-warning"
                        });
                    }

                    $rootScope.loading = false;

                }).error(function(data) {
                    $scope.status = status;
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                });

                // }
            }

        };

        if ($scope.tabIndex == $scope.lang_admin.length) {
            $scope.buttonLabel = "Submit";
        } else {

            $scope.buttonLabel = "Next";
        }

        $scope.setIndex = function($index) {
            $scope.tabIndex = $index;
        }

        $scope.getIndex = function($index) {
            if ($index == 0) {
                return true;
            }
        }


    }])

.controller('EditblogCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Post',
        subtitle: 'Post Content'
    };
    $rootScope.loading = true;
    $scope.blog = [];
    $scope.thumbnailCropped = "";
    $scope.statusId = $stateParams.blogID;
    console.log($scope.statusId);

    $http.get($scope.baseUrl + "post/category/type/1")
        .success(function(result) {
            console.log(result);
            $scope.category_list = result;
        });

    $http.get($scope.baseUrl + "post/blog/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;
            var tags_raw = result[0].pd_meta_keyword.split(",");

            console.log(arrayvar.a);
            $scope.blog.title_blog = result[0].pd_title;
            $scope.blog.content_blog = result[0].pd_content;
            $scope.blog.excerpt = result[0].pd_excerpt;
            $scope.blog.author = result[0].post_author;
            $scope.blog.category = result[0].post_categories_id;
            $scope.blog.tags_blog = tags_raw;
            $scope.blog.pagetitle = result[0].pd_pagetitle;
            $scope.blog.metades = result[0].pd_meta_description;
            $scope.blog.visibility_blog = result[0].post_status;
            $scope.myImage = result[0].image;
            $scope.rowBlogNews = result;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
                // $scope.myImage2=evt.target.result;
                // $scope.myImage3=evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };

    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    $scope.froalaOptions1 = {
        inlineMode: false,
        charCounterMax: 20,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: $scope.blog.author,
                category: $scope.blog.category,
                tags: $scope.blog.tags_blog,
                pagetitle: $scope.blog.pagetitle,
                metadecription: $scope.blog.metades,
                visibility: $scope.blog.visibility_blog,
                imag1: $(".thumbnail").attr("value"),
                imag2: '',
                imag3: '',
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                 

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])
.controller('EditvideoCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig) {
    $scope.page = {
        title: 'Edit Video',
        subtitle: 'Video Content'
    };
    $rootScope.loading = true;
    $scope.blog = [];
    $scope.thumbnailCropped = "";
    $scope.statusId = $stateParams.blogID;

    $http.get($scope.baseUrl + "post/blog/" + $scope.statusId)
        .success(function(result) {
            console.log(result);
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;
            var tags_raw = result[0].pd_meta_keyword.split(",");

            console.log(arrayvar.a);
            $scope.blog.title_blog = result[0].pd_title;
            $scope.blog.content_blog = result[0].pd_content;
            $scope.blog.excerpt = result[0].pd_excerpt;
            $scope.myImage = '/api/image/blog/'+result[0].post_thumbnail2;
            $rootScope.loading = false;
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.myImage = evt.target.result;
                // $scope.myImage2=evt.target.result;
                // $scope.myImage3=evt.target.result;

            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };

    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    $scope.froalaOptions1 = {
        inlineMode: false,
        charCounterMax: 20,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    /**
     * Upload Blob (cropped image) instead of file.
     * @see
     *   https://developer.mozilla.org/en-US/docs/Web/API/FormData
     *   https://github.com/nervgh/angular-file-upload/issues/208
     */
    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };

    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                title: $scope.blog.title_blog,
                content: $scope.blog.content_blog,
                excerpt: $scope.blog.excerpt,
                author: 'admin',
                category: 44,
                type: '1',
                tags: [{'text': "Video"}, {'text': "Content"}],
                pagetitle: '',
                metadecription: '',
                visibility: '',
                imag1: '',
                imag2: $(".thumbnail").attr("value"),
                imag3: '',
                post_id: $scope.statusId

            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/post/update', JSON.stringify(putdatablog)).success(function(data) {
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }

                $rootScope.loading = false;

            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }


}])

.controller('socialCtrl', ['$rootScope', '$scope', '$http', '$element', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', '$timeout', function($rootScope, $scope, $http, $element, FileUploader, $stateParams, toastr, toastrConfig, $timeout) {
    $scope.page = {
        title: 'Social Media',
        subtitle: 'Social Media'
    };
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "get/social")
        .success(function(result) {
            console.log(result);
            $scope.formContact = result;
        })
        .then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            })
        });

    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(dataNa) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdata = {
                data: dataNa
            };
            console.log(dataNa);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/social/update', JSON.stringify(putdata)).success(function(data) {
                $rootScope.loading = false;
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('contactCtrl', ['$rootScope', '$scope', '$http', '$element', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', '$timeout', function($rootScope, $scope, $http, $element, FileUploader, $stateParams, toastr, toastrConfig, $timeout) {
    $scope.page = {
        title: 'Contact Us',
        subtitle: 'Contact Content'
    };
    $rootScope.loading = true;
    $http.get($scope.baseUrl + "get/contact")
        .success(function(result) {
            console.log(result);
            $scope.formContact = result;
        })
        .then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            })
        });

    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    $scope.tabIndex = 0;
    $scope.buttonLabel = "";


    $scope.proceed = function(dataNa) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdata = {
                data: dataNa
            };
            console.log(dataNa);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/contact/update', JSON.stringify(putdata)).success(function(data) {
                $rootScope.loading = false;
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])

.controller('websiteCtrl', ['$rootScope', '$scope', '$http', 'FileUploader', '$stateParams', 'toastr', 'toastrConfig', '$timeout', function($rootScope, $scope, $http, FileUploader, $stateParams, toastr, toastrConfig, $timeout) {
    $scope.page = {
        title: 'Website Setting',
        subtitle: 'Website Setting'
    };

    $rootScope.loading = true;
    $http.get($scope.baseUrl + "get/website")
        .success(function(result) {
            console.log(result);
            var a = new Array();
            var arrayvar = {};
            arrayvar.a = a;
            var tags_raw = result.keyword.split(",");
            $scope.blog.metades = result.description;
            $scope.blog.tags_blog = tags_raw;
            $scope.blog.email = result.main_email;
            $scope.blog.pagetitle = result.title;
            $scope.blog.title = result.website_name;
            $scope.logoLight = result.logolight;
            $scope.logoDark = result.logodark;
        })
        .then(function() {
            $timeout(function() {
                $rootScope.loading = false;
            })
        });

    var uploader = $scope.uploader = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });

    var uploader1 = $scope.uploader1 = new FileUploader({
        url: 'admin/blog/create',
        withCredentials: false,
        method: 'POST',
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(html) {
        // console.info('onErrorItem', fileItem, response, status, headers);
        console.log(html);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    uploader.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.logo_light = evt.target.result;
                $scope.logoLight = evt.target.result;
            });
        };
        console.log(item._file);
        reader.readAsDataURL(item._file);
    };

    uploader1.onAfterAddingFile = function(item) {
        // var file=item.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(evt) {
            $scope.$apply(function($scope) {
                $scope.logo_dark = evt.target.result;
                $scope.logoDark = evt.target.result;
            });
        };
        console.log("uploader1" + item._file);
        console.log("uploader1" + $scope.myLogo);
        reader.readAsDataURL(item._file);
    };

    var base64ToBinary = function(base64EncodedFile) {
        var BASE64_MARKER = ';base64,';
        var base64Index = base64EncodedFile.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = base64EncodedFile.substring(base64Index);
        var raw = atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(rawLength);

        for (i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array.buffer;
    }

    $scope.froalaOptions1 = {
        inlineMode: false,
        theme: 'custom',
        height: '300',
        headers: {
            'X-XSRF-TOKEN': token
        },
        buttons: ['bold', 'italic', 'underline', 'removeFormat', 'color', 'sep', 'formatBlock', 'align', 'insertOrderedList', 'insertUnorderedList', 'outdent', 'indent', 'sep', 'createLink', 'insertImage', 'insertVideo', 'uploadFile', 'table', 'undo', 'redo', 'fullscreen', 'sep', 'html'],
        fileUploadURL: 'upload_file',
        fileUploadParam: 'upload_param',
        imageUploadURL: 'upload_image',
        imageUploadParam: 'image_param',
        imageUploadParams: {
            id: 'my_editor',
            class: "img-responsive"
        },
        events: {
            align: function(e, editor, alignment) {
                console.log(alignment + ' aligned');
            }
        }
    }

    uploader.onBeforeUploadItem = function(item) {
        var blob = dataURItoBlob($scope.myCroppedImage1);
        console.log(blob);
        item._file = blob;
        console.log(item._file);
    };

    /**
     * Converts data uri to Blob. Necessary for uploading.
     * @see
     *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
     * @param  {String} dataURI
     * @return {Blob}
     */

    var dataURItoBlob = function(dataURI) {
        console.log(dataURI);
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };

    //FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };


    var vm = this;
    $scope.lang_admin = [];

    $http.get($scope.baseUrl + "language/show")
        .success(function(result) {
            console.log(result);
            $scope.lang_admin = result;
        });

    $scope.tabIndex = 0;
    $scope.buttonLabel = "";
    $scope.proceed = function(isValid, data) {
        if ($scope.tabIndex !== $scope.lang_admin.length - 1) {
            $scope.lang_admin[$scope.tabIndex].active = false;
            $scope.tabIndex++
                $scope.lang_admin[$scope.tabIndex].active = true;
        }
        console.log($scope.lang_admin.length);
        if ($scope.tabIndex === $scope.lang_admin.length - 1) {
            // if (isValid) {
            var putdatablog = {
                website_name: $scope.blog.title,
                email: $scope.blog.email,
                keyword: $scope.blog.tags_blog,
                title: $scope.blog.pagetitle,
                description: $scope.blog.metades,
                logolight: $(".logo_light").attr("value"),
                logodark: $(".logo_dark").attr("value")
            };
            console.log(putdatablog);
            $rootScope.loading = true;
            $http.post($scope.baseUrl + 'admin/website/update', JSON.stringify(putdatablog)).success(function(data) {
                $rootScope.loading = false;
                $scope.persons = data;
                console.log(data);
                uploader.uploadAll();
                if (data.t == 1) {
                    var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                        iconClass: 'bg-success',
                        iconType: "fa-check"
                    });

                } else {
                    var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                        iconClass: 'bg-warning',
                        iconType: "fa-warning"
                    });
                }
            }).error(function(data) {
                $scope.status = status;
                var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            });

            // }
        }

    };

    if ($scope.tabIndex == $scope.lang_admin.length) {
        $scope.buttonLabel = "Submit";
    } else {

        $scope.buttonLabel = "Next";
    }

    $scope.setIndex = function($index) {
        $scope.tabIndex = $index;
    }

    $scope.getIndex = function($index) {
        if ($index == 0) {
            return true;
        }
    }
}])





//untouch
.controller('changePass', ['$rootScope', '$scope', '$http', '$timeout', '$filter', '$stateParams', '$state', 'toastr', 'toastrConfig', function($rootScope, $scope, $http, $timeout, $filter, $stateParams, $state, toastr, toastrConfig) {
    $scope.username = $stateParams.user;
    $scope.register_process = function() {
        $rootScope.loading = true;

        var dataRegister = {
            user_id: $stateParams.id,
            password: $scope.td.currentpassword,
            newpassword: $scope.td.confirm_password,
        }

        $http.post($scope.baseUrl + 'account/changepasswordadm', JSON.stringify(dataRegister)).success(function(data) {
            $rootScope.loading = false;
            $scope.msg = data.status;
            console.log(data);
            if (data.t == 1) {
                var toast = toastr[$scope.optionsToast.type]("Saved", "Notification", {
                    iconClass: 'bg-success',
                    iconType: "fa-check"
                });
                $scope.$state.go("app.admin.show", {}, {
                    reload: true
                });
            } else {
                var toastFail = toastr[$scope.optionsToast.type](data.status, "Notification", {
                    iconClass: 'bg-warning',
                    iconType: "fa-warning"
                });
            }
        }).error(function(data) {
            var toastFail = toastr[$scope.optionsToast.type]("Internal Server Error , Please Try Again", "Notification", {
                iconClass: 'bg-warning',
                iconType: "fa-warning"
            });
        });
    }


}])
