'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('NavCtrl', function ($scope,$http) {
    $scope.oneAtATime = false;
    $scope.menuReady = false;

    $scope.status = {
      isFirstOpen: true,
      isSecondOpen: true,
      isThirdOpen: true
    };

    $http.get("/get/menu")
		.success(function (result) {
			console.log(result);
			$scope.dynamicMenu = result;
			$scope.menuReady = true;
		});
  });
