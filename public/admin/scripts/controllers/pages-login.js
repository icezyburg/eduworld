'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:PagesLoginCtrl
 * @description
 * # PagesLoginCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
    .controller('LoginCtrl', function($rootScope, $auth, $scope, $state, $http) {

        var vm = this;
        $http.get('api/authenticate').success(function(users) {
            $http.get($scope.baseUrl + "api/authenticate/user")
                .success(function(result) {
                    //console.log(result);
                    var user = JSON.stringify(result.user);
                    if (result.user.user_role == 0) {
                        $state.go('app.dashboard', {});
                        // Set the stringified user data into local storage
                        localStorage.setItem('user', user);

                        // The user's authenticated state gets flipped to
                        // true so we can now show parts of the UI that rely
                        // on the user being logged in
                        $rootScope.authenticated = true;

                        // Putting the user's data on $rootScope allows
                        // us to access it anywhere across the app
                        $rootScope.currentUser = result.user;
                    } else {

                        localStorage.removeItem('user');
                        // Flip authenticated to false so that we no longer
                        // show UI elements dependant on the user being logged in
                        $rootScope.authenticated = false;

                        // Remove the current user info from rootscope
                        $rootScope.currentUser = null;
                        window.location.href = "/";
                    }
                }).catch(function(e) {
                    // vm.error_feed = "error";
                });

        }).error(function(error) {

        });

        vm.login = function() {
            var va = this;
            var credentials = {
                    email: vm.email,
                    password: vm.password,
                    user_role: "0"
                }
                // Use Satellizer's $auth service to login
            $auth.login(credentials).then(function(data) {
                console.log(data);
                $http.get($scope.baseUrl + "api/authenticate/user")
                    .success(function(result) {
                        console.log(result);
                        var user = JSON.stringify(result.user);

                        // Set the stringified user data into local storage
                        localStorage.setItem('user', user);

                        // The user's authenticated state gets flipped to
                        // true so we can now show parts of the UI that rely
                        // on the user being logged in
                        $rootScope.authenticated = true;

                        // Putting the user's data on $rootScope allows
                        // us to access it anywhere across the app
                        $rootScope.currentUser = result.user;

                        if (result.user.user_token == 1) {
                            $rootScope.superUser = true;
                        }

                        if (result.user.user_role == 0) {
                            $state.go('app.dashboard', {});
                        } else {
                            localStorage.removeItem('user');
                            // Flip authenticated to false so that we no longer
                            // show UI elements dependant on the user being logged in
                            $rootScope.authenticated = false;

                            // Remove the current user info from rootscope
                            $rootScope.currentUser = null;
                            $window.location.href = "";
                        }
                    }).catch(function(e) {
                        vm.error_feed = "error";
                    });
            }).catch(function(e) {
                vm.error_feed = "error";
                console.log(e);
            });
        }

    });
