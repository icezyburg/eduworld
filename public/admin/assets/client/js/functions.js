function setJoin($rootScope){
	var window_w = window.innerWidth;
	var window_h = window.innerHeight;
	var header_h = angular.element('#header-wrapper').innerHeight();
	var body_h = window_h-header_h;

	angular.element("body").css("padding-top",header_h);

	angular.element(".join-bottom").css({
		"height" : (window_h-angular.element("#join-description").height()-header_h)
	});
}

function setJoinIntro(){
	angular.element("#logo-des").css({
		"padding-top" : ((angular.element("#join-description").height()-angular.element("#logo-des").height())/2)
	});

	console.log("Join Description "+angular.element("#join-description").height());
	console.log("Logo-Des" + angular.element("#logo-des").height());
}

function setDna($rootScope){
	var window_w = window.innerWidth;
	var window_h = window.innerHeight;
	var header_h = angular.element('#header-wrapper').innerHeight();
	var body_h = window_h-header_h;
	var porto_l = $rootScope.portos.length;
	var div = 5;
	var el = '';
	var item_size = 0;
	var header_margin=0;
	var max_description_height = 0;
	var content_summary = 0;

	/* Set DNA Display Company Size */
	item_size = window_w/div;
	el = angular.element(".dna-content");
	el.css({
		width : item_size,
		height: item_size
	});
	angular.element("body").css("padding-top",header_h);

	var elContent = document.getElementById("content-wrapper");
	elContent.style.height = body_h+"px";

	var elPortoDetail = angular.element('.dna-detail');

	var portoItemHeight = 0;
	for(var i=0;i<elPortoDetail.length;i++){
		var target = ".dna-item .dna-content:nth-child("+i+") .dna-detail";
		if(portoItemHeight<angular.element(target).height()){
			portoItemHeight = angular.element(target).height();
		}
	}

	$rootScope.dnaHeight = item_size-portoItemHeight-30;

	/* Set Vertical Center Content Summary */
	content_summary = angular.element("#dna-content").height()-angular.element(".dna-item").height();
	description_margin_top = (content_summary-angular.element("#logo-des").height()-50)/2;
	angular.element("#dna-description").css({
		"margin-top" : description_margin_top
	})

	$rootScope.summaryHeight = angular.element("#dna-content-text").find('p').first().css("height");
	$rootScope.textHeight = angular.element("#dna-content-text").css("height");
	$rootScope.maxHeight = body_h-description_margin_top-30;
	angular.element("#dna-content-text").css({
		"height" : $rootScope.summaryHeight,
		"overflow" : "hidden",
		"max-height" : $rootScope.maxHeight		// 30 adalah tinggi dari separator => OUR COMPANY
	});

}
