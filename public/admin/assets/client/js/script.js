var app = angular.module('kidApp', ['ngScrollbar','ngSanitize','ui.router']);

// configuring routes
// =============================================================================
app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

        // route to show our basic form (/form)
        .state('form', {
            url: '/form',
            templateUrl: 'join-us-form.html',
            controller: 'formCtrl'
        })

        // nested states
        // each of these sections will have their own view
        // url will be nested (/form/vacancies)
        .state('form.vacancies', {
            url: '/vacancies',
            templateUrl: 'vacancies.html'
        })

        // url will be /form/profile
        .state('form.profile', {
            url: '/profile',
            templateUrl: 'profile.html'
        })

        // url will be /form/education
        .state('form.education', {
            url: '/education',
            templateUrl: 'education.html'
        })

        // url will be /form/history
        .state('form.history', {
            url: '/history',
            templateUrl: 'history.html'
        })

        // url will be /form/attachment
        .state('form.attachment', {
            url: '/attachment',
            templateUrl: 'attachment.html'
        })

        .state('contact',{
            url: '/contact-us',
            templateUrl : 'contact.html',
            controller: 'contactCtrl'
        })

        .state('/',{
            url: '/',
            templateUrl : 'home.html',
            controller : 'portoCtrl'
        })

        .state('dna',{
            url: '/our-dna',
            templateUrl : 'our-dna.html',
        })

        .state('join',{
            url: '/join-us',
            templateUrl : 'join-us.html',
        })

    // catch all route
    // send users to the form page
    $urlRouterProvider.otherwise('/');
})

// form Controller
// =============================================================================
app.controller('formCtrl', function($scope,$rootScope,$http) {

    angular.element("#join-content-form").css({
		"padding-top" : angular.element("#header-wrapper").height()
	});

	// we will store all of our form data in this object
    $rootScope.formData = {};

    $rootScope.fake = {};

    $rootScope.fake.photo = "No file choosen";
    $rootScope.fake.resume = "No file choosen";
    $rootScope.fake.portfolio = "No file choosen";
    $rootScope.fake.other = "No file choosen";

    $http.get("json/job.json")
    	.success(function(response){
    		$rootScope.jobs = response.records;
    		$rootScope.formData.position = response.records[0];
    	});

    // function to process the form
    $scope.processForm = function() {
        alert('awesome!');
    };

    $scope.uploadFile = function(){
        var filename = event.target.files[0].name;
        $rootScope.fromData.photo = filename;
        console.log(filename);
    };
});

app.directive('customOnChange', function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeFunc = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeFunc);
        }
    };
});

app.controller('contactCtrl',function($scope,$http,$timeout){
    $scope.contacts = {};

    angular.element("#contact-content").css({
        "padding-top" : angular.element("#header-wrapper").height()
    });
});

app.controller('portoCtrl', function($scope, $http, $timeout ) {
    $http.get("/json/porto.php")
    	.success(function(response){
    		$scope.portos = response.records;
    	})
    	.then(function(){
    		$timeout(function(){
				var window_w = window.innerWidth-17;
				var window_h = window.innerHeight;
				var header_h = angular.element('#header-wrapper').innerHeight();
				var body_h = window_h-header_h;
				var porto_l = $scope.portos.length;
				var div = 4;
				var el = '';

				el = angular.element(".porto-content");
				el.css({
					width : (window_w/div),
					height: (window_w/div)
				});

				var item_size = window_w/div;
				var header_margin = body_h-(item_size*2);

				if(header_margin<0){
					header_margin=0;
				}

				header_margin = header_margin + header_h;

				angular.element("body").css("padding-top",header_h);

				var elContent = document.getElementById("content-wrapper");
				elContent.style.height = body_h+"px";

				var elPortoDetail = angular.element('.porto-detail');

				var portoItemHeight = 0;
				for(var i=0;i<elPortoDetail.length;i++){
					var target = ".porto-item .porto-content:nth-child("+i+") .porto-detail";
					if(portoItemHeight<angular.element(target).height()){
						portoItemHeight = angular.element(target).height();
					}
				}

				angular.element('.porto-logo').height(item_size-portoItemHeight-30);

				console.log("test");
			});
    	});
	// $scope.config = {
	//     autoHideScrollbar: false,
	//     theme: 'light',
	//     advanced:{
	//         updateOnContentResize: true
	//     },
	//         setHeight: 1000,
	//         scrollInertia: 0
 //    }
});

app.controller('joinCtrl',function($rootScope,$http,$scope,$timeout){
	$http.get("/json/job.json")
		.success(function(response){
			$rootScope.jobs = response.records;
			$rootScope.jobChoose = $rootScope.jobs[0];
			$rootScope.jobTitle = $rootScope.jobs[0].pd_title;
			$rootScope.jobContent = $rootScope.jobs[0].pd_content;
		})
		.then(function(){
			$timeout(function(){
				setJoin($rootScope);
			})
		});

	$scope.changeJob = function(item){
		$rootScope.jobTitle = item.pd_title;
		$rootScope.jobContent = item.pd_content;
		$timeout(function(){
			setJoinIntro();
		})
	}
});

app.controller('joinBgCtrl', function($rootScope, $http, $timeout) {
    $http.get("/json/join-bg.json")
    	.success(function(response){
    		$rootScope.joinBg = response.records;
    	});
});

app.controller('joinIntroCtrl', function($rootScope, $http, $timeout) {
    $http.get("/json/join-intro.json")
    	.success(function(response){
    		$rootScope.joinIntro = response.records;
    	})
    	.then(function(){
    		$timeout(function(){
    			setJoinIntro()
    		})
    	});
});

app.controller('dnaCtrl',function($rootScope,$http){
	$http.get("/json/dna-content.php")
		.success(function(response){
			$rootScope.ourdna = response.records;
		});
});

app.controller('dnaCompanyCtrl', function($rootScope, $http, $timeout) {
    $http.get("/json/dna.php")
    	.success(function(response){
    		$rootScope.portos = response.records;
    	}).then(function(){
    		$timeout(function(){
		    	test($rootScope);
    		});
    	});

	$rootScope.seeMore = function(){
		angular.element("#dna-content-text").css({
    		"height" : $rootScope.maxHeight,
    		"overflow" : "hidden"
    	});

    	angular.element(".dna-item").css({
    		"bottom" : -(angular.element(".dna-item").height()-angular.element("#dna-sparator").height()-20)
    	});

    	angular.element("#see-more").css({
    		"opacity" : 0
    	});

    	angular.element("#dna-sparator").css({
    		"cursor" : "pointer"
    	});
	}

	$rootScope.reset = function(){
		angular.element("#dna-content-text").css({
    		"height" : $rootScope.summaryHeight,
    		"overflow" : "hidden"
    	});

    	angular.element(".dna-item").css({
    		"bottom" : "0px"
    	});

    	angular.element("#see-more").css({
    		"opacity" : 1,
    	});

    	angular.element("#dna-sparator").css({
    		"cursor" : "auto"
    	});
	}
});

app.directive('hoverDna', function(){
    return function(scope, element, attrs){
     	element
            .on('mouseenter',function() {
                element.find('.dna-logo').css("height",attrs.hoverDna);
            })
            .on('mouseleave',function() {
                element.find('.dna-logo').css("height","100%");
            });
    };
});

app.directive('resize', function ($window,$rootScope) {
    return function (element,attrs) {
       	var w = angular.element($window);
        w.bind('resize', function () {
			if(attrs.resize=="join"){
				angular.element("#logo-des").css({
    				"padding-top" : ((angular.element("#join-description").height()-angular.element("#logo-des").height())/2)
    			});
			}else{
				test($rootScope);
			}
        });
    }
})

app.directive('backImg', function(){
    return function(scope, element, attrs){
        var url = '/assets/client/images/'+attrs.backImg;
        element.css({
            'background-image': 'url(' + url +')',
            'background-size' : 'cover'
        });

        //alert(attrs.testAttr);
    };
});
