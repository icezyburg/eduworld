'use strict';

var madCoreCtrl = angular.module('madCoreCtrl', [])

.controller("coreCtrl",['$state','$http','$timeout',"$scope","$rootScope",function($state,$http,$timeout,$scope,$rootScope){
	$scope.myInterval = 5000;
	$scope.noWrapSlides = false;
	$scope.active = 0;
	var currIndex = 0;
	var PageNow = $state.current.name;

	if(PageNow == 'servicesdetailcontent' || PageNow == 'servicesdetail'){



	// $timeout(function() {
	// 	$('#main-menu').smartmenus('destroy');
	// }, 1600);


	}
//	if(PageNow == 'home'){
//		angular.element('.breadcrumbs').hide();
//		$http.get("/post/page/content/home")
//		.success(function (result) {
//			console.log(result);
//			$scope.content = result[0];
//		});
//		alert('home');
//	}else if(PageNow == 'about'){
//		angular.element('.breadcrumbs').show();
//		$http.get("/post/page/content/about")
//		.success(function (result) {
//			console.log(result);
//			$scope.content = result[0];
//		});
//		alert('about');
//	}




	$scope.slides = [{ image:"/assets/images/banner.jpg",  text:"Nice image",  id:0}, { image:"/assets/images/banner.jpg",  text:"Awesome photograph",  id:1}, { image:"/assets/images/banner.jpg",  text:"That is so cool",  id:2}, { image:"/assets/images/banner.jpg",  text:"I love that",  id:3}];

}])
.controller("coreSidebarCtrl",["$scope","$http","$rootScope",function($scope,$http,$rootScope){
	$http.get("/post/blog/category/2/limit/0/4")
		.success(function (result) {
			console.log(result);
			$scope.recentblog = result;
		});
}])
.controller("coreContactCtrl",["$scope","$http","$rootScope",function($scope,$http,$rootScope){
	var index;
	var maps_o;
	var maps_t;
	$http.get("/get/contact")
	.success(function (result) {
		$scope.contact = result;

		for (index = 0; index < result.length; ++index) {
			var slug = result[index].slug;
			angular.element('#'+slug).html(result[index].value);

			if(slug == 'maps'){
				maps_o = result[index].value;
				angular.element('#maps').html('<iframe src="'+maps_o+'" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');
			}
			if(slug == 'maps_two'){
				maps_t = result[index].value;
			}
		}
	});


	$scope.maps_one = function(){
		angular.element('#maps').html('<iframe src="'+maps_o+'" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');
	};
	$scope.maps_two = function(){
		angular.element('#maps').html('<iframe src="'+maps_t+'" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');
	};
}])

.controller("HeaderCtrl",["$scope","$rootScope",function($scope,$rootScope){

}])
.controller("homeCtrl",["$scope",'$filter',"$state","$http","$rootScope","$window",function($scope,$filter,$state,$http,$rootScope,$window){

	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');

	$scope.breakpoints = [
	  {
	    breakpoint: 768,
	    settings: {
	      slidesToShow: 2,
	      slidesToScroll: 2
	    }
	  }, {
	    breakpoint: 480,
	    settings: {
	      slidesToShow: 1,
	      slidesToScroll: 1
	    }
	  }
	];

	$scope.eventdetail = function(d){
		var id = d.post_id;
		var slg = $rootScope.slugKeun(d.pd_title);
		$state.go('eventdetail', {slug:slg,id:id});
	};
	$scope.openVideo = function(src){
		angular.element('.iframeVideo').html('<iframe width="100%" height="500" src="https://www.youtube.com/embed/'+src+'" frameborder="0" allowfullscreen></iframe>');
		angular.element('.overlayVideo').fadeIn();
	};

	angular.element('body').on('click','.overlayVideo', function(){
		angular.element('.overlayVideo').fadeOut();
		angular.element('.iframeVideo').html('');
	});


	angular.element('.breadcrumbs').hide();
	$http.get("/post/page/content/home")
		.success(function (result) {
			console.log(result);
			$scope.content = result[0];
			angular.element('#loadingBanner').fadeOut();
		});
	$http.get("/post/testimonial")
		.success(function (result) {
			console.log(result);
			$scope.testimonial = result;
			angular.element('#loadingTestimonial').fadeOut();
		});
	$http.get("/post/blog/category/2/limit/0/4")
		.success(function (result) {
			console.log(result);
			$scope.blog = result;
			angular.element('#loadingBlog').fadeOut();

		});
	$http.get("/post/blog/category/44/limit/0/4")
		.success(function (result) {
			console.log(result);
			$scope.videoblog = result;
			angular.element('#loadingVideo').fadeOut();
		});

	$http.get("/post/blog/category/46/limit/0/7")
		.success(function (result) {
			$scope.studyabroad = result;
			var item = {'pd_title':'', 'post_id':0, 'pd_pagetitle':'', 'post_thumbnail':'', 'image':'' };
			$scope.studyabroad.splice(0, 0, item);
			angular.element('#loadingServices').fadeOut();

		});
	$scope.dateOptions = {
	    dateDisabled: disabled,
	    formatYear: 'yy',
	    maxDate: new Date(),
	    startingDay: 1
	};

	  // Disable weekend selection
	function disabled(data) {
	    var date = data.date,
	    mode = data.mode;
	    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	}

	$scope.open1 = function() {
	    $scope.popup1.opened = true;
	};

	$scope.popup1 = {
		opened: false
	};
	$scope.proceed = function (dataNa) {

			var putdata = {
				data: dataNa
			};
			console.log(dataNa);

			$http.post('/send/enquiry', JSON.stringify(putdata)).success(function (data) {
				if (data.t == 1) {
					angular.element('#notifNa').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Thank you!</strong> '+data.status+'</div>');
					angular.element('.form-control').val('');
				} else {
					angular.element('#notifNa').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry!</strong> '+data.status+'</div>');
				}

			}).error(function (data) {
				angular.element('#notifNa').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry!</strong> '+data.status+'</div>');
			});



	};

}])
.controller("aboutCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
	$scope.breakpoints = [
	  {
	    breakpoint: 768,
	    settings: {
	      slidesToShow: 4,
	      slidesToScroll: 1
	    }
	  }, {
	    breakpoint: 480,
	    settings: {
	      slidesToShow: 2,
	      slidesToScroll: 1
	    }
	  }
	];
	angular.element('.breadcrumbs').show();
	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>About Us</a></li>';
	$http.get("/post/page/content/about")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});
	$http.get("/post/gallery/category/14")
	.success(function (result) {
		console.log(result);
		$scope.network = result;
	});
	$http.get("/post/aboutlist")
	.success(function (result) {
		console.log(result);
		$scope.about_content = result[0];
		$scope.about_list = result;

		angular.element('#loadingAbout').fadeOut();

	});
}])

.controller("servicesDetailCtrl",["$scope","$timeout","$stateParams","$http","$rootScope","$window",function($scope,$timeout,$stateParams,$http,$rootScope,$window){
	angular.element('.breadcrumbs').hide();
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
	var idNa = $stateParams.id;
	var type = $stateParams.type;
	var slugNa = $stateParams.slug;
	$scope.PstId = $stateParams.post_id;

	$rootScope.className = slugNa;

 	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu .mn_'+slugNa).addClass('active');
 	angular.element('#main-menu li').removeClass('active');
 	angular.element('#main-menu .mn_'+slugNa).addClass('active');

 	angular.element('#main-menu li ul li').removeClass('active');
 	angular.element('#main-menu li ul li.sbm_'+idNa).addClass('active');
 	angular.element('#sb-menu li ul li').removeClass('active');
 	angular.element('#sb-menu li ul li.sbm_'+idNa).addClass('active');


 	angular.element('#sb-menu li.active ul').css({'display':'block', 'left': '0'});
 	angular.element('#sb-menu li.active ul li ul').css({'display':'none', 'left': '0'});
 	angular.element('#sb-menu li.active ul li.active ul').css({'display':'block', 'left': '0'});

 	angular.element('#main-menu').mouseover(function(){

 		angular.element('#sb-menu li.active ul').hide();
 	});

 	angular.element('#main-menu').mouseleave(function(){

 		angular.element('#sb-menu li.active ul').show();
 		angular.element('#sb-menu li.active ul li ul').hide();
 		angular.element('#sb-menu li.active ul li.active ul').show();
 	});


	$http.get("/post/services/category/"+idNa)
		.success(function (result) {
			console.log(result);
			$scope.about_content = result;
			angular.element('.titlePageNa strong').html(result[0].cd_name);

			$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li><a>Our Services</a></li><li class="active"><a>'+result[0].cd_name+'</a></li>';

			angular.element('#loadingServices').fadeOut();
			$scope.$watch("PstId",function(sddd){
				if(sddd)
				{
					var xx = parseInt(sddd);
					 $timeout(function() {
					 	$rootScope.setIndex(xx);

					 	angular.element('#main-menu li ul li ul li').removeClass('active');
					 	angular.element('#main-menu li ul li ul li.sbmn_'+xx).addClass('active');

					 	angular.element('#sb-menu li ul li ul li').removeClass('active');
					 	angular.element('#sb-menu li ul li ul li.sbmn_'+xx).addClass('active');

					 }, 1000);

				}
			})
		});

	if(slugNa == 'study-abroad'){
		angular.element('.titlePageNa').html('Study Abroad : <strong></strong>');
	}else if(slugNa == 'high-school'){
		angular.element('.titlePageNa').html('High School : <strong></strong>');
	}else if(slugNa == 'student-exchange-program'){
		angular.element('.titlePageNa').html('Student Exchange Program : <strong></strong>');
	}
	$http.get("/post/page/content/"+slugNa)
	.success(function (result) {
		console.log(result[0]);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});

}])

.controller("summercampCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){

	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Summer Camp</a></li>';
	$http.get("/post/page/content/summercamp")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});
	$http.get("/post/summercamp")
	.success(function (result) {
		console.log(result);
		$scope.summer_list = result;
		angular.element('#loadingServices').fadeOut();
	});

 	angular.element('.sm-blue li').removeClass('active');
 	angular.element('.mn_summer-camp').addClass('active');

}])
.controller("translationCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){

	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Translation</a></li>';
	$http.get("/post/page/content/translation")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});

	$scope.color = ['orange','blue','green'];

 	angular.element('.sm-blue li').removeClass('active');
 	angular.element('.mn_translation').addClass('active');
}])
.controller("videoCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){

	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Video</a></li>';
	$http.get("/post/page/content/video")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});
	$http.get("/post/blog/category/44")
		.success(function (result) {
			console.log(result);
			$scope.videoblog = result;
			angular.element('#loadingVideo').fadeOut();
		});
}])

.controller("pageCtrl",["$scope","$stateParams","$http","$rootScope","$window",function($scope,$stateParams,$http,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
	var id= $stateParams.id;
	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a></a></li>';
	$http.get("/post/page/content/"+id)
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('.breadcrumbs li.active a').html(result[0].pd_title);
		angular.element('#loadingBanner').fadeOut();
	});

}])

.controller("applyCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
	angular.element('.breadcrumbs').show();
	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Apply Online</a></li>';
	$scope.dateOptions = {
		dateDisabled: disabled,
		formatYear: 'yy',
		maxDate: new Date(),
		startingDay: 1
	};

	$scope.optionNat = ['USA','Canada','UK','New Zealand','Singapore','Others'];


	$scope.summercmpNat = function(){
		$scope.optionNat = ['USA','Canada','UK','New Zealand','Singapore','Others'];
	}

	$scope.stndtexNat = function(){
		$scope.optionNat = ['USA','Belgium','Germany','Italy','Netherlands','Others'];
	}
	// Disable weekend selection
	function disabled(data) {
		var date = data.date,
		mode = data.mode;
		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	}

	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};

	$scope.popup1 = {
		opened: false
	};

	$http.get("/post/page/content/apply")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
	});

	$scope.proceed = function (title,dataNa) {
		var putdata = {
			title: title,
			data: dataNa
		};
		console.log(dataNa);

		$http.post('/apply/data', JSON.stringify(putdata)).success(function (data) {
			if (data.t == 1) {
				angular.element('#notifNa').html('<div style="margin:0" class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Thank you!</strong> '+data.status+'</div>');
				angular.element('.form-control').val('');
			} else {
				angular.element('#notifNa').html('<div style="margin:0" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry!</strong> '+data.status+'</div>');
			}

		}).error(function (data) {
			angular.element('#notifNa').html('<div style="margin:0" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry!</strong> '+data.status+'</div>');
		});
	};

	// Check source value
	$scope.source = 'newspaper';
	$scope.model = {
	    otherDisabled: true,
	    websiteDisabled: true
	};
	
	$scope.$watch('source', function(source) {
	    console.log(source);
	    if(source == 'website') {
	    	$scope.model.websiteDisabled = false;
	    	$scope.model.otherDisabled = true;
	    } else if (source == 'other') {
	    	$scope.model.otherDisabled = false;
	    	$scope.model.websiteDisabled = true;
	    } else {
	    	$scope.model.websiteDisabled = true;
	    	$scope.model.otherDisabled = true;
	    }
	});

}])
.controller("testimonialCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Testimonial</a></li>';
	angular.element('.breadcrumbs').show();
	$http.get("/post/testimonial")
		.success(function (result) {
			console.log(result);
			$scope.testimonial = result;
		});
	$http.get("/post/page/content/testimonial")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
	});

}])
.controller("testimonialDetailCtrl",["$scope","$http","$rootScope","$window","$stateParams",function($scope,$http,$rootScope,$window,$stateParams){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Testimonial</a></li>';
	angular.element('.breadcrumbs').show();
	$scope.idNa = $stateParams.id;

	$http.get("/post/testimonial/"+$scope.idNa)
		.success(function (result) {
			console.log(result);
			$scope.testimoniald = result[0];
		});
	$http.get("/post/testimonial")
		.success(function (result) {
			console.log(result);
			$scope.testimonial = result;
		});
	$http.get("/post/page/content/testimonial")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
	});

}])
.controller("blogCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){

	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Blog</a></li>';
	angular.element('.breadcrumbs').show();
	$http.get("/post/blog/category/2")
		.success(function (result) {
			console.log(result);
			$scope.blog = result;
			$scope.blog_first = result[0];
		angular.element('#loadingBlog').fadeOut();
		});
	$http.get("/post/page/content/blog")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});

}])
.controller("eventCtrl",["$scope","$state","$http","$rootScope","$window",function($scope,$state,$http,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Event</a></li>';
	angular.element('.breadcrumbs').show();
	$http.get("/post/blog/category/46/limit/0/50")
		.success(function (result) {
			console.log(result);
			$scope.blog = result;
			$scope.blog_first = result[0];
		angular.element('#loadingBlog').fadeOut();
		});
	$http.get("/post/page/content/event")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});


	$scope.eventdetail = function(d){
		var id = d.post_id;
		var slg = $rootScope.slugKeun(d.pd_title);
		$state.go('eventdetail', {slug:slg,id:id});
	};
}])

.controller("eventdetailCtrl",['$sce',"$scope","$stateParams","$http","$rootScope","$window","$location",function($sce,$scope,$stateParams,$http,$rootScope,$window,$location){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li><a>News &amp; Event</a></li><li class="active"><a></a></li>';
	angular.element('.breadcrumbs').show();
	$rootScope.idBlg = $stateParams.id;
	$http.get("/post/blog/"+$rootScope.idBlg)
		.success(function (result) {
			console.log(result);
			$scope.blog = result[0];
			$scope.blogcontent = $sce.trustAsHtml(result[0].pd_content);
			angular.element('.breadcrumbs li.active a').html(result[0].pd_title);
			angular.element('#loadingBlog').fadeOut();
		});

	$http.get("/post/event/other/"+$rootScope.idBlg)
		.success(function (result) {
			console.log(result);
			$scope.nextblog = result.next[0];
			$scope.prevblog = result.prev[0];
			$scope.nextblogs = result.next;
			$scope.prevblogs = result.prev;
		});


	$scope.absUrl = $location.absUrl();


}])
.controller("blogdetailCtrl",['$sce',"$scope","$stateParams","$http","$rootScope","$window", "$location",function($sce,$scope,$stateParams,$http,$rootScope,$window,$location){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li><a>Blog</a></li><li class="active"><a></a></li>';
	angular.element('.breadcrumbs').show();
	$rootScope.idBlg = $stateParams.id;
	$http.get("/post/blog/"+$rootScope.idBlg)
		.success(function (result) {
			console.log(result);
			$scope.blog = result[0];
			$scope.blogcontent = $sce.trustAsHtml(result[0].pd_content);
			angular.element('.breadcrumbs li.active a').html(result[0].pd_title);
			angular.element('#loadingBlog').fadeOut();
		});

	$http.get("/post/blog/other/"+$rootScope.idBlg)
		.success(function (result) {
			console.log(result);
			$scope.nextblog = result.next[0];
			$scope.prevblog = result.prev[0];
			$scope.nextblogs = result.next;
			$scope.prevblogs = result.prev;
		});

	$scope.absUrl = $location.absUrl();

}])
.controller("contactCtrl",["$scope","$http","$rootScope","$window",function($scope,$http,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Contact</a></li>';
	angular.element('.breadcrumbs').hide();
	$http.get("/post/page/content/contact")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
	});

	$scope.proceed = function (dataNa) {

			var putdata = {
				data: dataNa
			};
			console.log(dataNa);

			$http.post('/send/contact', JSON.stringify(putdata)).success(function (data) {
				if (data.t == 1) {
					angular.element('#notifNa').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Thank you!</strong> '+data.status+'</div>');
					angular.element('.form-control').val('');
				} else {
					angular.element('#notifNa').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry!</strong> '+data.status+'</div>');
				}

			}).error(function (data) {
				angular.element('#notifNa').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Sorry!</strong> '+data.status+'</div>');
			});



	};

}])
.controller("albumCtrl",["$scope","$http","$state","$rootScope","$window",function($scope,$http,$state,$rootScope,$window){
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Gallery</a></li>';
	$http.get("/post/page/content/gallerypage")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});
	$http.get("/post/sub_category/14")
		.success(function (result) {
			console.log(result);
			$scope.categories = result;
			angular.element('#loadingGallery').fadeOut();
		});
	$scope.gotoGallery = function (ct) {
	    $state.go('gallerydetail', {slug:$rootScope.slugKeun(ct.cd_name),id:ct.categories_id});
	};
}])

.controller("galleryCtrl",["$scope","$state",'Lightbox',"$stateParams","$http","$rootScope","$window",function($scope,$state,Lightbox,$stateParams,$http,$rootScope,$window){
	$rootScope.idGn = $stateParams.id;
	angular.element('#sb-menu li').removeClass('active');
 	angular.element('#sb-menu li ul').hide();
 	angular.element('#main-menu li').removeClass('active');
 	$rootScope.breadcrumb = '<li><a ui-sref="home">Home</a></li><li class="active"><a>Gallery</a></li>';
	$http.get("/post/page/content/gallerypage")
	.success(function (result) {
		console.log(result);
		$scope.content = result[0];
		angular.element('#loadingBanner').fadeOut();
	});

	$scope.gotoGallery = function (ct) {
	    $state.go('gallerydetail', {slug:$rootScope.slugKeun(ct.cd_name),id:ct.categories_id});
	};

	$http.get("/post/sub_category/"+$rootScope.idGn)
		.success(function (result) {
			console.log(result);
			$scope.gallery_category = result;
		});

	$http.get("/post/gallery/category/"+$rootScope.idGn)
		.success(function (result) {
			console.log(result);
			if(result.length){
				angular.element('.tlN').html(result[0].cd_name+"'s Gallery");
				$scope.gallery = result;
			}

			angular.element('#loadingGallery').fadeOut();
		});
	$scope.openLightboxModal = function (index) {
	    Lightbox.openModal($scope.gallery, index);
	};
}])

.filter('ellipsis', function () {
    return function (text, length) {
        if (text.length > length) {
            return text.substr(0, length) + "...";
        }
        return text;
    }
})

.filter('trustAsHtml', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    }
})

.filter('iiff', function () {
   return function(input, trueValue, falseValue) {
        return input ? trueValue : falseValue;
   };
});
