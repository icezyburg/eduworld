'use strict';

var madApp = angular.module("madApp", [
        "madCoreCtrl",
        "madDirectives",
        "ngAnimate",
        "ngSanitize",
        "ngCookies",
        "ui.router",
        "satellizer",
        "psResponsive",
        "mgcrea.ngStrap",
        'ngResource',
        'ui.bootstrap',
        'ui.bootstrap.carousel',
        'wu.masonry',
        'slick',
        'slugifier',
        'bootstrapLightbox',
        'angularUtils.directives.dirDisqus'
    ], function($interpolateProvider) {
        $interpolateProvider.startSymbol('%%');
        $interpolateProvider.endSymbol('%%');
    })
    .run(['$rootScope', '$sce', '$state', '$http', '$stateParams', '$timeout', 'psResponsive', '$auth', "$window", "$location", 'Slug', function($rootScope, $sce, $state, $http, $stateParams, $timeout, psResponsive, $auth, $window, $location, Slug) {
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            });

    $rootScope.$on('$viewContentLoaded', function() {
    	window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '695485047275246',
		      xfbml      : true,
		      version    : 'v2.8'
		    });
		  };
	    (function(d, s, id) {
	        var js, fjs = d.getElementsByTagName(s)[0];
	        //if (d.getElementById(id)) return;
	        js = d.createElement(s); js.id = id;
	        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=695485047275246";
	        fjs.parentNode.insertBefore(js, fjs);
	      }(document, 'script', 'facebook-jssdk'));
	});

        $rootScope.linkto = function(l) {
            $state.go(l);
        };
        $rootScope.linktoTestimonial = function(t) {
            $state.go('testimonialdetail', {
                id: t.post_id
            });
        };

		$rootScope.subscribe = function () {
			var putdatablog = {
				title: $rootScope.email_subscribe
			};
			console.log(putdatablog);
			$http.post('/subscribe', JSON.stringify(putdatablog)).success(function (data) {
				if (data.t == 1) {
					angular.element('#email_subscribe').val('');
					angular.element('#mYnotf').fadeIn();
					angular.element('#mYnotf').html('Thank You :)');

					$timeout(function() {

					angular.element('#mYnotf').fadeOut();

					}, 2000);


				} else {
					
					angular.element('#mYnotf').fadeIn();
					angular.element('#mYnotf').html('Failed to subscribe');

					$timeout(function() {

					angular.element('#mYnotf').fadeOut();

					}, 2000);
				}
			}).error(function (data) {
					angular.element('#mYnotf').fadeIn();
					angular.element('#mYnotf').html('Failed to subscribe');


					$timeout(function() {

					angular.element('#mYnotf').fadeOut();

					}, 2000);
			});
		};
	    $rootScope.slugKeun = function(input) {
	        var slgNa = Slug.slugify(input);
			if(slgNa == ''){
				return input;
			}
			return slgNa;
	    };
        $rootScope.linktoparam = function(l, i, t) {
            $state.go(l({
                id: i,
                slug: t
            }));
        };

        $rootScope.openLine = function() {
            angular.element('.overlayLine').fadeIn();
        };

        angular.element('body').on('click', '.overlayLine', function() {
            angular.element('.overlayLine').fadeOut();
        });

        $rootScope.openContact = function() {
            angular.element('.overlayContact').fadeIn();
        };

        angular.element('body').on('click', '.overlayContact', function() {
            angular.element('.overlayContact').fadeOut();
        });

        $rootScope.tanggal = function(string) {
            var array = string.split('-');
            var tglna = array[2].split(' ');
            return tglna[0];
        }
        $rootScope.bulan = function(string) {
            var array = string.split('-');
            return array[1];
        }
        $rootScope.bulan_nama = function(string) {
            var array = string.split('-');
            var bln_angka = parseInt(array[1] - 1);
            var bulan_abjad = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            return bulan_abjad[bln_angka];
        }
        $rootScope.tahun = function(string) {
            var array = string.split('-');
            return array[0];
        }
        $rootScope.tglNa = function(string) {
            var d = $rootScope.tanggal(string);
            var m = $rootScope.bulan_nama(string);
            var y = $rootScope.tahun(string);
            return d + ' ' + m + ' ' + y;
        }

        function findFacebook(array) {
            return array.slug === 'facebook';
        }

        function findTwitter(array) {
            return array.slug === 'twitter';
        }

        function findLinkedin(array) {
            return array.slug === 'linkedin';
        }

        function findFacebookPage(array) {
            return array.slug === 'facebook_page';
        }

        function findYoutube(array) {
            return array.slug === 'youtube';
        }

        function findInstagram(array) {
            return array.slug === 'instagram';
        }

        $http.get("/get/social")
            .success(function(result) {
                console.log(result);
                $rootScope.social_facebook = result.find(findFacebook);
                $rootScope.social_twitter = result.find(findTwitter);
                $rootScope.social_linkedin = result.find(findLinkedin);
                $rootScope.social_facebookpage = result.find(findFacebookPage);
                $rootScope.social_youtube = result.find(findYoutube);
                $rootScope.social_instagram = result.find(findInstagram);
                $rootScope.social_facebookpagevalue = $sce.trustAsHtml($rootScope.social_facebookpage.value);
            });
        $rootScope.setIndex = function(a) {

            $rootScope.inDx = a;
            $timeout(function() {
                angular.element('.btn').triggerHandler('click');
            }, 100);
        };
        angular.element('#main-nav').hide();
        $http.get("/get/menu")
            .success(function(result) {

                console.log(result);
                $rootScope.topMenu = result;
                angular.element('#main-nav').show();

            });


    }])
    .config(['$stateProvider', '$urlRouterProvider', '$authProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $authProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/home');

        $stateProvider

            .state('mad', {
                templateUrl: "website/tmpl/component/full-width.html",
                url: '',
                controller: "coreCtrl"
            })
            .state('madSidebar', {
                templateUrl: "website/tmpl/component/sidebar.html",
                url: '',
                controller: "coreSidebarCtrl"
            })
            .state('madContact', {
                templateUrl: "website/tmpl/component/contact.html",
                url: '',
                controller: "coreContactCtrl"
            })
            .state('home', {
                title: "EduWorld",
                url: "/home",
                templateUrl: 'website/tmpl/home.html',
                parent: "mad",
                controller: "homeCtrl"
            })
            .state('testimonial', {
                title: "EduWorld",
                url: "/testimonial",
                templateUrl: 'website/tmpl/testimonial.html',
                parent: "mad",
                controller: "testimonialCtrl"
            })
            .state('testimonialdetail', {
                title: "EduWorld",
                url: "/testimonial/:id",
                templateUrl: 'website/tmpl/testimonialdetail.html',
                parent: "mad",
                controller: "testimonialDetailCtrl"
            })


        .state('apply', {
                title: "Apply Online",
                url: "/apply-online",
                templateUrl: 'website/tmpl/apply.html',
                parent: "mad",
                controller: "applyCtrl"
            })
            .state('about', {
                title: "About EduWorld",
                url: "/about",
                templateUrl: 'website/tmpl/about.html',
                parent: "mad",
                controller: "aboutCtrl"
            })
            .state('servicesdetail', {
                title: "Our Services",
                url: "/services/:slug/:id/:slugpost",
                templateUrl: 'website/tmpl/aboutdetail.html',
                parent: "mad",
                controller: "servicesDetailCtrl"
            })
            .state('servicesdetailcontent', {
                title: "Our Services",
                url: "/services/:slug/:id/:post_id/:post",
                templateUrl: 'website/tmpl/aboutdetail.html',
                parent: "mad",
                controller: "servicesDetailCtrl"
            })
            .state('page', {
                title: "Static Page",
                url: "/page/:id",
                templateUrl: 'website/tmpl/page.html',
                parent: "mad",
                controller: "pageCtrl"
            })
            .state('exchange', {
                title: "Study Exchange",
                url: "/study-exchange",
                templateUrl: 'website/tmpl/exchange.html',
                parent: "mad",
                controller: "exchangeCtrl"
            })
            .state('summercamp', {
                title: "Summer Camp",
                url: "/summer-camp",
                templateUrl: 'website/tmpl/summercamp.html',
                parent: "mad",
                controller: "summercampCtrl"
            })
            .state('studyabroad', {
                title: "Study Abroad",
                url: "/study-abroad",
                templateUrl: 'website/tmpl/studyabroad.html',
                parent: "mad",
                controller: "studyabroadCtrl"
            })
            .state('highschool', {
                title: "High School",
                url: "/highschool",
                templateUrl: 'website/tmpl/highschool.html',
                parent: "mad",
                controller: "highschoolCtrl"
            })
            .state('translation', {
                title: "Translation",
                url: "/translation",
                templateUrl: 'website/tmpl/translation.html',
                parent: "mad",
                controller: "translationCtrl"
            })
            .state('blog', {
                title: "Blog",
                url: "/blog",
                templateUrl: 'website/tmpl/blog.html',
                parent: "mad",
                controller: "blogCtrl"
            })
            .state('event', {
                title: "Blog",
                url: "/event",
                templateUrl: 'website/tmpl/event.html',
                parent: "mad",
                controller: "eventCtrl"
            })
            .state('blogdetail', {
                title: "Blog",
                url: "/blog/:id/:slug",
                templateUrl: 'website/tmpl/blogdetail.html',
                parent: "madSidebar",
                controller: "blogdetailCtrl"
            })
            .state('eventdetail', {
                title: "Event",
                url: "/event/:id/:slug",
                templateUrl: 'website/tmpl/eventdetail.html',
                parent: "madSidebar",
                controller: "eventdetailCtrl"
            })
            .state('videodetail', {
                title: "Video Detail",
                url: "/video/:id/:slug",
                templateUrl: 'website/tmpl/videodetail.html',
                parent: "madSidebar",
                controller: "blogdetailCtrl"
            })
            .state('video', {
                title: "Video",
                url: "/video",
                templateUrl: 'website/tmpl/video.html',
                parent: "mad",
                controller: "videoCtrl"
            })


            .state('contact', {
                title: "Contact Us",
                url: "/contact",
                templateUrl: 'website/tmpl/contact.html',
                parent: "madContact",
                controller: "contactCtrl"
            })
            .state('gallery', {
                title: "Gallery",
                url: "/gallery",
                templateUrl: 'website/tmpl/album.html',
                parent: "mad",
                controller: "albumCtrl"
            })
            .state('gallerydetail', {
                title: "Gallery Detail",
                url: "/gallery/:id/:slug",
                templateUrl: 'website/tmpl/gallery.html',
                parent: "mad",
                controller: "galleryCtrl"
            })

    }]);
