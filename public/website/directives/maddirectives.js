'use strict';

var madDirectives = angular.module('madDirectives', [])
.directive("serializer", function () {
	return {
		restrict: "A"
		, scope: {
			onSubmit: "&serializer"
		}
		, link: function (scope, element) {
			// assuming for brevity that directive is defined on <form>

			var form = element;

			form.submit(function (event) {
				event.preventDefault();
				var serializedData = form.serializeArray();
				var i, text,clName,valueName,VClassName;

				var kirim = [];
				var obj = {};
				for (i = 0; i < serializedData.length; i++) {
				    clName = serializedData[i]['name'];
				    valueName = serializedData[i]['value'];
				    VClassName = angular.element('#'+serializedData[i]['name']).attr('title');
				    kirim.push({'name':clName,'value':valueName,'title':VClassName});
				    
				}
				var titleNa = serializedData[4]['value']+'( '+serializedData[5]['value']+' )';
				scope.onSubmit({
					title: titleNa,
					data: kirim
				});
			});

		}
	};
})
.directive("serialize", function () {
	return {
		restrict: "A"
		, scope: {
			onSubmit: "&serialize"
		}
		, link: function (scope, element) {
			// assuming for brevity that directive is defined on <form>

			var form = element;

			form.submit(function (event) {
				event.preventDefault();
				var serializedData = form.serializeArray();
				
				scope.onSubmit({
					data: serializedData
				});
			});

		}
	};
})
.directive('dynFbCommentBox', function () {
    function createHTML(href, numposts, colorscheme) {
        return '<div class="fb-comments" ' +
                       'data-href="' + href + '" ' +
                       'data-numposts="' + numposts + '" ' +
                       'data-width="100%"' +
                       'data-colorsheme="' + colorscheme + '">' +
               '</div>';
    }

    return {
        restrict: 'A',
        scope: {},
        link: function postLink(scope, elem, attrs) {
            attrs.$observe('pageHref', function (newValue) {
                var href        = newValue;
                var numposts    = attrs.numposts    || 5;
                var colorscheme = attrs.colorscheme || 'light';

                elem.html(createHTML(href, numposts, colorscheme));
                FB.XFBML.parse(elem[0]);
            });
        }
    };
});