<!doctype html>
<html ng-app="minovateApp" ng-controller="MainCtrl" class="no-js {{containerClass}}">

<head>
	<meta charset="utf-8">
	<title>MADLOGIC</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="icon" type="image/ico" href="favicon.ico" />
	<!-- build:css(.) styles/vendor.css -->
	<!-- bower:css -->
	<script src="admin/bower_components/jquery/jquery.min.js"></script>
	<link rel="stylesheet" href="admin/bower_components/bootstrap/dist/css/bootstrap.css" />
	<link rel="stylesheet" href="admin/bower_components/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="admin/bower_components/animate.css/animate.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-loading-bar/build/loading-bar.css" />
	<link rel="stylesheet" href="admin/bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-ui-tree/dist/angular-ui-tree.min.css" />
	<link rel="stylesheet" href="admin/bower_components/simple-line-icons/css/simple-line-icons.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-toastr/dist/angular-toastr.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-bootstrap-nav-tree/dist/abn_tree.css" />
	<link rel="stylesheet" href="admin/bower_components/chosen/chosen.min.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-ui-select/dist/select.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" href="admin/bower_components/ngImgCrop/compile/minified/ng-img-crop.css" />
	<link rel="stylesheet" href="admin/bower_components/datatables/media/css/jquery.dataTables.css" />
	<link rel="stylesheet" href="admin/bower_components/angular-ui-grid/ui-grid.css" />
	<link rel="stylesheet" href="admin/bower_components/ng-table/ng-table.css" />
	<link rel="stylesheet" href="admin/bower_components/owl-carousel/owl-carousel/owl.carousel.css" />
	<link rel="stylesheet" href="admin/bower_components/owl-carousel/owl-carousel/owl.theme.css" />
	<link rel="stylesheet" href="admin/bower_components/fullcalendar/fullcalendar.css" />
	<link rel="stylesheet" href="admin/bower_components/ng-tags-input/ng-tags-input.min.css" />
	<link rel="stylesheet" href="admin/bower_components/FroalaWysiwygEditor/css/froala_content.min.css" />
	<link rel="stylesheet" href="admin/bower_components/FroalaWysiwygEditor/css/froala_editor.min.css" />
	<link rel="stylesheet" href="admin/bower_components/FroalaWysiwygEditor/css/froala_style.min.css" />
	<link rel="stylesheet" href="admin/bower_components/FroalaWysiwygEditor/css/custom-theme.css" />
	<link rel="stylesheet" href="admin/assets/client/css/angular-ui-tree.min.css" />

	<script src="admin/bower_components/FroalaWysiwygEditor/js/froala_editor.min.js"></script>



	<!-- endbower -->
	<!-- endbuild -->
	<!-- build:css(.tmp) styles/main.css -->
	<link rel="stylesheet" href="admin/styles/main.css">
	<!-- endbuild -->
</head>

<body id="minovate" class="{{main.settings.navbarHeaderColor}} {{main.settings.activeColor}} {{containerClass}}" ng-class="{'header-fixed':main.settings.headerFixed, 'header-static':!main.settings.headerFixed, 'aside-fixed':main.settings.asideFixed, 'aside-static':!main.settings.asideFixed, 'rightbar-show':main.settings.rightbarShow, 'rightbar-hidden':!main.settings.rightbarShow }">
<?php
	$encrypter = app('Illuminate\Encryption\Encrypter');
	$encrypted_token = $encrypter->encrypt(csrf_token());
	echo '<input id="token" type="hidden" value="'.$encrypted_token.'">';
?>
	<!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

	<!-- Application content -->
	<div id="wrap" ui-view autoscroll="false"></div>

	<!-- Page Loader -->
	<div id="pageloader" page-loader style="z-index: 1"></div>
	<script>
		var token = $('#token').val();
	</script>

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o)
				, m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-XXXXX-X');
		ga('send', 'pageview');
	</script>
	<script src='//maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&sensor=false&language=en&v=3.17'></script>

	<!-- build:js(.) scripts/oldieshim.js -->
	<!--[if lt IE 9]>
    <script src="bower_components/es5-shim/es5-shim.js"></script>
    <script src="bower_components/json3/lib/json3.min.js"></script>
    <![endif]-->
	<!-- endbuild -->

	<!-- build:js(.) scripts/vendor.js -->

	<!-- bower:js -->
	<!-- <script src="bower_components/jquery/jquery.js"></script> -->

	<script src="admin/bower_components/angular/angular.js"></script>
	<script src="admin/bower_components/json3/lib/json3.js"></script>
	<script src="admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="admin/bower_components/angular-resource/angular-resource.js"></script>
	<script src="admin/bower_components/angular-cookies/angular-cookies.js"></script>
	<script src="admin/bower_components/angular-sanitize/angular-sanitize.js"></script>
	<script src="admin/bower_components/angular-animate/angular-animate.js"></script>
	<script src="admin/bower_components/angular-touch/angular-touch.js"></script>
	<script src="admin/bower_components/angular-fontawesome/dist/angular-fontawesome.js"></script>
	<script src="admin/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
	<script src="admin/bower_components/jquery.slimscroll/jquery.slimscroll.min.js"></script>
	<script src="admin/bower_components/jquery.sparkline/index.js"></script>
	<script src="admin/bower_components/angular-loading-bar/build/loading-bar.js"></script>
	<script src="admin/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
	<script src="admin/bower_components/angular-ui-utils/ui-utils.js"></script>
	<script src="admin/bower_components/moment/moment.js"></script>
	<script src="admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="admin/bower_components/ng-bs-daterangepicker/src/ng-bs-daterangepicker.js"></script>
	<script src="admin/bower_components/angular-momentjs/angular-momentjs.js"></script>
	<script src="admin/bower_components/angular-fullscreen/src/angular-fullscreen.js"></script>
	<script src="admin/bower_components/angular-ui-tree/dist/angular-ui-tree.js"></script>
	<script src="admin/bower_components/html.sortable/dist/html.sortable.js"></script>
	<script src="admin/bower_components/angular-toastr/dist/angular-toastr.tpls.js"></script>
	<script src="admin/bower_components/angular-bootstrap-nav-tree/dist/abn_tree_directive.js"></script>
	<script src="admin/bower_components/oclazyload/dist/ocLazyLoad.js"></script>
	<script src="admin/bower_components/chosen/chosen.jquery.min.js"></script>
	<script src="admin/bower_components/angular-ui-select/dist/select.js"></script>
	<script src="admin/bower_components/textAngular/dist/textAngular.min.js"></script>
	<script src="admin/bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js"></script>
	<script src="admin/bower_components/angular-file-upload/angular-file-upload.js"></script>
	<script src="admin/bower_components/ngImgCrop/compile/minified/ng-img-crop.js"></script>
	<script src="admin/bower_components/datatables/media/js/jquery.dataTables.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/angular-datatables.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/colreorder/angular-datatables.colreorder.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/columnfilter/angular-datatables.columnfilter.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/colvis/angular-datatables.colvis.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/fixedcolumns/angular-datatables.fixedcolumns.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/fixedheader/angular-datatables.fixedheader.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/scroller/angular-datatables.scroller.min.js"></script>
	<script src="admin/bower_components/angular-datatables/dist/plugins/tabletools/angular-datatables.tabletools.min.js"></script>
	<script src="admin/bower_components/angular-ui-grid/ui-grid.js"></script>
	<script src="admin/bower_components/ng-table/ng-table.js"></script>
	<script src="admin/bower_components/angular-smart-table/dist/smart-table.min.js"></script>
	<script src="admin/bower_components/raphael/raphael.js"></script>
	<script src="admin/bower_components/mocha/mocha.js"></script>
	<script src="admin/bower_components/morrisjs/morris.js"></script>
	<script src="admin/bower_components/flot/jquery.flot.js"></script>
	<script src="admin/bower_components/flot/jquery.flot.resize.js"></script>
	<script src="admin/bower_components/flot.tooltip/js/jquery.flot.tooltip.js"></script>
	<script src="admin/bower_components/angular-flot/angular-flot.js"></script>
	<script src="admin/bower_components/flot-spline/js/jquery.flot.spline.min.js"></script>
	<script src="admin/bower_components/d3/d3.js"></script>
	<script src="admin/bower_components/rickshaw/rickshaw.js"></script>
	<script src="admin/bower_components/angular-rickshaw/rickshaw.js"></script>
	<script src="admin/bower_components/owl-carousel/owl-carousel/owl.carousel.min.js"></script>
	<script src="admin/bower_components/lodash/lodash.min.js"></script>
	<script src="admin/bower_components/angular-google-maps/dist/angular-google-maps.js"></script>
	<script src="admin/bower_components/jquery-ui/ui/jquery-ui.js"></script>
	<script src="admin/bower_components/fullcalendar/fullcalendar.js"></script>
	<script src="admin/bower_components/angular-ui-calendar/src/calendar.js"></script>
	<script src="admin/bower_components/jquery.easy-pie-chart/dist/angular.easypiechart.min.js"></script>
	<script src="admin/bower_components/ng-tags-input/ng-tags-input.min.js"></script>
	<script src="admin/bower_components/angular-translate/angular-translate.js"></script>
	<script src="admin/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js"></script>
	<script src="admin/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js"></script>
	<script src="admin/bower_components/angular-translate-storage-local/angular-translate-storage-local.js"></script>
	<script src="admin/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.min.js"></script>
	<script src="admin/bower_components/leaflet/dist/leaflet.js"></script>
	<script src="admin/bower_components/leaflet/dist/leaflet-src.js"></script>
	<script src="admin/bower_components/angular-leaflet-directive/dist/angular-leaflet-directive.js"></script>
	<script src="admin/bower_components/leaflet-draw/dist/leaflet.draw.js"></script>
	<script src="admin/bower_components/leaflet-plugins/control/Distance.js"></script>
	<script src="admin/bower_components/leaflet-plugins/control/Layers.Load.js"></script>
	<script src="admin/bower_components/leaflet-plugins/control/Permalink.js"></script>
	<script src="admin/bower_components/leaflet-plugins/control/Permalink.Layer.js"></script>
	<script src="admin/bower_components/leaflet-plugins/control/Permalink.Line.js"></script>
	<script src="admin/bower_components/leaflet-plugins/control/Permalink.Marker.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/Icon.Canvas.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/Layer.Deferred.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/Marker.Rotate.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/Marker.Text.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/OpenStreetBugs.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/vector/GPX.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/vector/GPX.Speed.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/vector/KML.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/vector/OSM.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/tile/Bing.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/tile/Google.js"></script>
	<script src="admin/bower_components/leaflet-plugins/layer/tile/Yandex.js"></script>
	<script src="admin/bower_components/leaflet-heat/dist/leaflet-heat.js"></script>
	<!-- Froala Plugins -->
	<!-- <script src="bower_components/jquery/jquery.min.js"></script> -->
	<!-- <script src="bower_components/jquery/jquery.min.js"></script> -->
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/tables.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/video.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/block_styles.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/colors.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/media_manager.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/lists.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/font_family.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/font_size.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/file_upload.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/urls.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/fullscreen.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/inline_styles.min.js"></script>
	<script src="admin/bower_components/FroalaWysiwygEditor/js/plugins/char_counter.min.js"></script>
	<!--[if lte IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Base64/0.3.0/base64.min.js"></script>
    <![endif]-->
	<script src="//cdn.jsdelivr.net/satellizer/0.11.3/satellizer.min.js"></script>
	<!-- endbower -->
	<script src="admin/assets/client/js/froala-sanitize.js"></script>
	<script src="admin/assets/client/js/angular-froala.js"></script>
	<script src="admin/assets/client/js/angular-ui-tree.min.js"></script>
	<!-- endbuild -->

	<!-- build:js({.tmp,app}) scripts/app.js -->
	
	<script src="vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="admin/scripts/app.js"></script>
	<script src="admin/scripts/directives/serialize.js"></script>
	<script src="admin/scripts/controllers/main.js"></script>
	<script src="admin/scripts/directives/navcollapse.js"></script>
	<script src="admin/scripts/directives/slimscroll.js"></script>
	<script src="admin/scripts/controllers/navbar-chart.js"></script>
	<script src="admin/scripts/directives/sparkline.js"></script>
	<script src="admin/scripts/controllers/dashboard.js"></script>
	<script src="admin/scripts/directives/collapsesidebar.js"></script>
	<script src="admin/scripts/directives/ripple.js"></script>
	<script src="admin/scripts/controllers/nav.js"></script>
	<script src="admin/scripts/directives/pageloader.js"></script>
	<script src="admin/scripts/controllers/daterangepicker.js"></script>
	<script src="admin/scripts/directives/daterangepicker.js"></script>
	<script src="admin/scripts/controllers/boxedlayout.js"></script>
	<script src="admin/scripts/controllers/fullwidthlayout.js"></script>
	<script src="admin/scripts/controllers/sidebarsmlayout.js"></script>
	<script src="admin/scripts/controllers/sidebarxslayout.js"></script>
	<script src="admin/scripts/controllers/hzmenu.js"></script>
	<script src="admin/scripts/controllers/rtl.js"></script>
	<script src="admin/scripts/controllers/ui-typography.js"></script>
	<script src="admin/scripts/directives/tilecontrolclose.js"></script>
	<script src="admin/scripts/directives/filestyle.js"></script>
	<script src="admin/scripts/directives/tilecontroltoggle.js"></script>
	<script src="admin/scripts/directives/tilecontrolrefresh.js"></script>
	<script src="admin/scripts/directives/tilecontrolfullscreen.js"></script>
	<script src="admin/scripts/directives/prettyprint.js"></script>
	<script src="admin/scripts/controllers/ui-lists.js"></script>
	<script src="admin/scripts/directives/lazymodel.js"></script>
	<script src="admin/scripts/controllers/ui-buttonsicons.js"></script>
	<script src="admin/scripts/directives/activatebutton.js"></script>
	<script src="admin/scripts/controllers/ui-navs.js"></script>
	<script src="admin/scripts/controllers/ui-modals.js"></script>
	<script src="admin/scripts/controllers/ui-tiles.js"></script>
	<script src="admin/scripts/controllers/ui-portlets.js"></script>
	<script src="admin/scripts/controllers/ui-grid.js"></script>
	<script src="admin/scripts/controllers/ui-alerts.js"></script>
	<script src="admin/scripts/directives/toastrinject.js"></script>
	<script src="admin/scripts/controllers/ui-general.js"></script>
	<script src="admin/scripts/controllers/ui-tree.js"></script>
	<script src="admin/scripts/directives/formsubmit.js"></script>
	<script src="admin/scripts/controllers/core.js"></script>
	<script src="admin/scripts/controllers/tables-ng-table.js"></script>
	<script src="admin/scripts/controllers/tables-smart-table.js"></script>
	<script src="admin/scripts/directives/wrap-owlcarousel.js"></script>
	<script src="admin/scripts/controllers/ui-widgets.js"></script>
	<script src="admin/scripts/controllers/pages-login.js"></script>
	<script src="admin/scripts/controllers/help.js"></script>
	<script src="admin/scripts/directives/offcanvas-sidebar.js"></script>
	<script src="admin/scripts/directives/submitvalidate.js"></script>
	<!-- endbuild -->
	


</body>

</html>