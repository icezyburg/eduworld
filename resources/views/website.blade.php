 <!DOCTYPE html>
<html lang="en">
  <head>
    <base href="/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EduWorld</title>
	<link rel="icon" href="/assets/images/favicon.png" type="image/png">
	<link rel="stylesheet" href="bower_components/datatables/media/css/jquery.dataTables.css" />
	<link rel="stylesheet" type="text/css" href="bower_components/hover/css/hover.css">
	<link rel="stylesheet" type="text/css" href="bower_components/magic/magic.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/lightbox.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css?v=2.0" rel="stylesheet">
    <!-- FONT -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- Plugin Scroll  -->
    <link rel="stylesheet" href="assets/plugin/scroll/css/jquery.mCustomScrollbar.min.css">
	<!-- Slider  -->
	<link rel="stylesheet" href="node_modules/slick-carousel/slick/slick.css">
	<link rel="stylesheet" href="node_modules/slick-carousel/slick/slick-theme.css">
	<!-- SmartMenus core CSS (required) -->
	<link href="/assets/css/menu/sm-core-css.css" rel="stylesheet" type="text/css" />

	<!-- "sm-blue" menu theme (optional, you can use your own CSS, too) -->
	<link href="/assets/css/menu/sm-blue.css" rel="stylesheet" type="text/css" />



  </head>

  <body ng-app="madApp">
  <div id="fb-root"></div>





  <div id="mYnotf"></div>

	  <div id="floatingSocial">
		<a ng-click="openLine()" title="LINE"><img width="40" src="/assets/images/float_line.png" /></a>
		<a href="%%social_youtube.value%%" title="%%social_youtube.name%%" target="_blank"><img width="40" src="/assets/images/float_youtube.png" /></a>
		<a href="%%social_facebook.value%%" title="%%social_facebook.name%%" target="_blank"><img width="40" src="/assets/images/float_facebook.png" /></a>
		<a ng-click="openContact()" title="Contact"><img width="40" src="/assets/images/float_contact.png" /></a>
		<a ui-sref="apply" title="Apply Online"><img width="40" src="/assets/images/float_apply.png" /></a>
	  </div>
	  <div id="loading">
		  <div class="loadingWrap">
			  	<div class="middle">
					<div><img src="/assets/images/logo.png" width="220"></div>
					<div style="margin-top: 15px"><img src="/assets/images/loading.gif"></div>
				</div>
		  </div>
	  </div>
	  <div class="overlayVideo">
	  	<div class="close"><i class="fa fa-times"></i></div>
		  <div class="iframeWrap">
		  	<div class="iframeVideo">

			</div>
		  </div>
	  </div>


	  <div class="overlayLine">
	  	<div class="close"><i class="fa fa-times"></i></div>
		  <div class="iframeWrap">
		  	<div class="iframe">
				<img src="/assets/images/line.png" />
			</div>
		  </div>
	  </div>
	  <div class="overlayContact">
	  	<div class="close"><i class="fa fa-times"></i></div>
		  <div class="iframeWrap">
		  	<div class="iframe">
			  	<div class="kotakNa">
			  	<h1 class="ttlNaih">CONTACT US</h1>
					<h1>Chamchuri Square Building</h1>
					<h3>0 2657 6401-2</h3>
					<h1>Central Pinklao</h1>
					<h3>0 2884 5101-2</h3>
				</div>
			</div>
		  </div>
	  </div>


    <div class="wrapper">

        <header ng-include="'website/tmpl/component/header.html'">

        </header>
        <div class="ovrlyMn fadeIn"></div>
        <div id="navHeader"></div>
		<nav id="main-nav" class="container fadeInLeft fadeOutLeft" role="navigation">

		  <ul id="main-menu" class="sm sm-blue">
		  <div class="logoHdr hide"><div><img src="/assets/images/logo.png"></div></div>
			<li class="hide"><a ui-sref="home">HOME</a></li>
			<li class="hide"><a ui-sref="about">ABOUT US</a></li>
			<li class="hide"><a ui-sref="apply">APPLY ONLINE</a></li>
			<li class="hide"><a ui-sref="blog">BLOG</a></li>
			<li class="hide"><a ui-sref="event">NEWS & EVENT</a></li>
			<li class="hide"><a ui-sref="gallery">GALLERY</a></li>
			<li class="hide"><a ui-sref="contact">CONTACT US</a></li>
			<li ng-repeat="menu in topMenu" class="mn_%% slugKeun(menu.cd_name) %%">
				<a href="#"> %% menu.cd_name %%</a>
			  	<ul>
					<li ng-repeat="pt in menu.children"><a ui-sref="servicesdetail({slug:slugKeun(menu.cd_name), id: pt.menu_id, type: pt.menu_type, slugpost: slugKeun(pt.menu_name)})"> %% pt.menu_name %%</a>
						<!-- <ul ng-if="menu.categories_id != 18">
							<li ng-repeat="mn in pt.children_post"><a ng-click="setIndex($index)" ui-sref="servicesdetailcontent({slug:slugKeun(menu.cd_name), id: pt.menu_id, type: pt.menu_type, slugpost: slugKeun(pt.menu_name), post: slugKeun(mn.menu_name), post_id: $index})"> %% mn.menu_name %%</a></li>
						</ul> -->
					</li>
					<li ng-if="menu.categories_id == 18"><a ui-sref="gallery">ภาพกิจกรรม</a></li>
				</ul>
			</li>
			<li class="mn_summer-camp">
				<a ui-sref="summercamp">Summer Camp</a>
			</li>
			<li class="mn_translation">
				<a ui-sref="translation">Translation</a>
			</li>

		  </ul>
		</nav>
		<div class="breadcrumbs">
			<ul class="breadcrumb container" ng-bind-html="breadcrumb">

			</ul>
		</div>
        <div id="main-content" ui-view>

        </div>

		<div class="footerBg"></div>
        <footer>

		  	<div class="container">
				<div class="col-md-9 col-xs-12">
					<a ui-sref="home" class="logoFooter"><img src="assets/images/logo.png" class="logo"/></a>
					<ul class="menuFooter" id="menuFooter">
						<li><a ui-sref="home">HOME</a></li>
						<li><a ui-sref="about">ABOUT US</a></li>
						<li><a ui-sref="blog">BLOG</a></li>
						<li><a ui-sref="gallery">GALLERY</a></li>
						<li><a ui-sref="contact">CONTACT US</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-xs-12 nopadding">
					<!-- <ul class="menuSocial">
						<li><a href="%%social_instagram.value%%" title="%%social_instagram.name%%" target="_blank"><img src="assets/images/icon_instagram.png"/></a></li>
						<li><a href="%%social_youtube.value%%" title="%%social_youtube.name%%" target="_blank"><img src="assets/images/icon_youtube.png"/></a></li>
						<li><a href="%%social_facebook.value%%" title="%%social_facebook.name%%" target="_blank"><img src="assets/images/icon_facebook.png"/></a></li>
						<li><a href="%%social_twitter.value%%" title="%%social_twitter.name%%" target="_blank"><img src="assets/images/icon_twitter.png"/></a></li>
					</ul> -->
					<form class="form-inline" ng-submit="subscribe()">
						<div class="form-group clearfix">
							<input type="email" required name="email_subscribe" ng-model="email_subscribe" class="form-control" id="email_subscribe" placeholder="Email Address">
						<button class="btn btn-primary">Subscribe</button>
						</div>
					</form>
				</div>


				<ul class="menuFooter" id="menuFooterMobile">
					<li><a ui-sref="about">ABOUT US</a></li>
					<li><a ui-sref="blog">BLOG</a></li>
					<li><a ui-sref="gallery">GALLERY</a></li>
					<li><a ui-sref="contact">CONTACT US</a></li>
				</ul>
			</div>
			<p class="copyright">Copyright &copy; 2016</p>
        </footer>
    </div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<script src="bower_components/angular/angular.min.js"></script>
	<script src="bower_components/angular-animate/angular-animate.min.js"></script>
	<script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
	<script src="bower_components/angular-cookies/angular-cookies.min.js"></script>
	<script src="bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
	<script src="bower_components/angular-strap/dist/angular-strap.js"></script>
	<script src="bower_components/angular-strap/dist/angular-strap.tpl.js"></script>
	<script src="bower_components/satellizer/satellizer.min.js"></script>
	<script src="bower_components/psResponsive/ps-responsive.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="bower_components/angularUtils-disqus/dirDisqus.js"></script>

	<script src="node_modules/jquery-bridget/jquery-bridget.js"></script>
	<script src="node_modules/ev-emitter/ev-emitter.js"></script>
	<script src="node_modules/desandro-matches-selector/matches-selector.js"></script>
	<script src="node_modules/fizzy-ui-utils/utils.js"></script>
	<script src="node_modules/get-size/get-size.js"></script>
	<script src="node_modules/outlayer/item.js"></script>
	<script src="node_modules/outlayer/outlayer.js"></script>
	<script src="node_modules/masonry/masonry.js"></script>
	<script src="node_modules/imagesloaded/imagesloaded.js"></script>
	<script src="node_modules/angular-masonry/angular-masonry.js"></script>
	<script src="/assets/js/jquery.masonry.ordered.js"></script>

	<script src="node_modules/slick-carousel/slick/slick.js"></script>
	<script src="node_modules/angular-slick/slick.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="/assets/js/ui-bootstrap-tpls-1.3.2.min.js"></script>
    <!-- Angular -->
    <script type="text/javascript" src="/website/app.js"></script>
	<script src="/website/controllers/madcorectrl.js"></script>
	<script src="/website/directives/maddirectives.js"></script>
	<script src="assets/plugin/scroll/js/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Slug  -->
    <script type="text/javascript" src="/assets/js/angular-slugify.js"></script>


	<!-- Lightbox  -->
    <script type="text/javascript" src="/assets/js/lightbox.js"></script>
	<!-- floating  -->
    <script type="text/javascript" src="/assets/js/floating-1.12.js"></script>

	<!-- SmartMenus jQuery plugin -->
	<script type="text/javascript" src="/assets/js/jquery.smartmenus.js"></script>
	<script type="text/javascript" src="/assets/js/jquery.parallaxify.min.js"></script>



	<script>
		function animate(){

		}

		$(window).load(function() {
			$('#loading').fadeOut();
			animate();
			setTimeout(function(){
				$('#main-menu').smartmenus({
					subMenusSubOffsetX: 1,
					subMenusSubOffsetY: -8,
					keepHighlighted: true
				});


			}, 1500);
		});



		$(document).ready(function(){
			$('body').on('click', '#nav-icon3, #main-menu li a', function(){
				$('#nav-icon3').toggleClass('open');
				$('#main-nav .sm').toggleClass('showTable');
				$('.ovrlyMn').toggleClass('show');

			});
		});
		floatingMenu.add('floatingSocial',
        {
            // Represents distance from left or right browser window
            // border depending upon property used. Only one should be
            // specified.
            // targetLeft: 0,
            targetRight: 10,

            // Represents distance from top or bottom browser window
            // border depending upon property used. Only one should be
            // specified.
            targetTop: 195,
            // targetBottom: 0,

            // Uncomment one of those if you need centering on
            // X- or Y- axis.
            // centerX: true,
            // centerY: true,

            // Remove this one if you don't want snap effect
            snap: true
        });
	</script>
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 863360938;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/863360938/?guid=ON&script=0"/>
	</div>
	</noscript>
  </body>
</html>
