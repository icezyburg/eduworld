 <!DOCTYPE html>
<html lang="en">
  <head>
    <base href="/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EduWorld</title>
	<link rel="icon" href="/assets/images/favicon.png" type="image/png">
	<style type="text/css">
		html,body {
			height:100%;
			text-align: center;
			background: #000;
			margin: 0;
		}

		a {
			display: inline-block;
			width: 100%;
			height: 100%;
		}

		a:before {
			content: ' ';
			display: inline-block;
			vertical-align: middle;
			height: 100%;
		}

		img {
			vertical-align: middle;
			display: inline-block;
		}

		.img-responsive {
			max-width: 100%;
			height: auto;
		}

		.center-block {
			margin-left: auto;
			margin-right: auto;
		}
	</style>
  </head>

  <body>
  <div id="fb-root"></div>

	<a href="/home">
		<img src="assets/images/king-2017.jpg" alt="" border="0" align="absmiddle" class="img-responsive center-block" />
	</a>

	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 863360938;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/863360938/?guid=ON&script=0"/>
	</div>
	</noscript>
  </body>
</html>
