<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteModel extends Model
{
   protected $table = 'madappe_setting';

   protected $guarded = [];

}
