<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
   protected $table = 'madappe_blog_categories';

   protected $guarded = [];

   protected $primaryKey = "categories_id";

   public function CategoryDesModel()
   {
       return $this->hasMany('App\CategoryDesModel', 'categories_id');
   }
}
