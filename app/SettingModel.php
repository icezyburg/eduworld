<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
   protected $table = 'madappe_setting';

   protected $guarded = [];

}
