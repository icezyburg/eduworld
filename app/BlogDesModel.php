<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogDesModel extends Model
{
   protected $table = 'madappe_blog_post_description';

   protected $guarded = [];

   protected $primaryKey = "pd_id";

   public function BlogModel()
   {
       return $this->belongsTo('App\BlogModel',"post_id");
   }

}
