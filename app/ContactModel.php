<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
   protected $table = 'madappe_contact';

   protected $guarded = [];

}
