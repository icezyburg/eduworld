<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::get('/', function () {
    return view('intro');
});
Route::get('/home', function () {
    return view('website');
});
Route::get('/video', function () {
    return view('website');
});
Route::get('/about', function () {
    return view('website');
});
Route::get('/services/{slug}/{id}/{slugpost}', function () {
    return view('website');
});
Route::get('/services/{slug}/{id}/{post_id}/{post}', function () {
    return view('website');
});

Route::get('/apply-online', function () {
    return view('website');
});
Route::get('/summer-camp', function () {
    return view('website');
});
Route::get('/translation', function () {
    return view('website');
});

Route::get('/blog', function () {
    return view('website');
});
Route::get('/blog/{id}/{slug}', function () {
    return view('website');
});
Route::get('/page/{id}', function () {
    return view('website');
});
Route::get('/video/{id}/{slug}', function () {
    return view('website');
});
Route::get('/contact', function () {
    return view('website');
});
Route::get('/testimonial', function () {
    return view('website');
});

Route::get('/testimonial/{id}', function () {
    return view('website');
});

Route::get('/event', function () {
    return view('website');
});
Route::get('/event/{id}/{slug}', function () {
    return view('website');
});
Route::get('/gallery', function () {
    return view('website');
});
Route::get('/gallery/{id}/{slug}', function () {
    return view('website');
});
Route::post('/apply/data', 'Controller@apply');
Route::post('/subscribe', 'Controller@subscribe');
Route::post('/send/contact', 'Controller@contact');
Route::post('/send/enquiry', 'Controller@enquiry');






///*** ADMIN ***///

Route::get('/madlogic', function(){
	Blade::setContentTags('<%', '%>');        // for variables and all things Blade
    Blade::setEscapedContentTags('<%%', '%%>');   // for escaped data

    return View::make('admin/admin');
});

//AUTH LOGIN WITH API
Route::group(['prefix' => 'api'], function()
{
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticateFB', 'AuthenticateController@authenticateFB');
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::post('authenticate/refresh', 'AuthenticateController@getAuthenticatedRefresh');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
    Route::get('authenticate/get_user/{id}/{type}', 'AuthenticateController@getDetailUser');
});
Route::post('/upload_image', [
    'middleware' => 'web',
    'uses' => 'Blog@upload_image'
]);
Route::post('tools/resize','Tools@resize');

//LANGUAGE
Route::get('/language/show', 'Language@show');


//CORE

Route::get('/setting/{type}', 'Blog@setting');

Route::post('/admin/info/update', [
    'middleware' => 'jwt.auth',
    'uses' => 'Blog@infoUpdate'
]);

Route::post('/upload/video/{id}', [
    'middleware' => 'jwt.auth',
    'uses' => 'Blog@uploadVideo'
]);

Route::post('/account/changepasswordadm', [
    'middleware' => 'jwt.auth',
    'uses' => 'Account@changepasswordadm'
]);

Route::get('/admin/show', 'Account@listAdmin');


Route::post('/account/admin', [
    'middleware' => 'jwt.auth',
    'uses' => 'Account@admin'
]);
Route::get('/admin/admin/delete/{id}', [
    'middleware' => 'jwt.auth',
    'uses' => 'Account@deleteMemberadm'
]);


//Admin Content
Route::get('/data/list/{type}', 'Controller@list_data');
Route::get('/data/detail/{id}', 'Controller@detail_data');
Route::get('/get/menu', 'Blog@show_menu');
Route::get('/get/website', 'Blog@website');
Route::get('/get/contact', 'Blog@contact');
Route::get('/get/social', 'Blog@social');
Route::get('/post/category/type/{id}', 'Blog@type_category');
Route::get('/post/type', 'Blog@allType');
Route::get('/post/about', 'Blog@about');
Route::get('/post/summercamp', 'Blog@summercamp');
Route::get('/post/aboutlist', 'Blog@aboutlist');
Route::get('/post/about/{id}', 'Blog@aboutdetail');
Route::get('/post/about/category/{id}', 'Blog@aboutcategorydetail');
Route::get('/post/services', 'Blog@services');
Route::get('/post/serviceslist', 'Blog@services');
Route::get('/post/services/{id}', 'Blog@servicesdetail');
Route::get('/post/services/category/{id}', 'Blog@servicescategorydetail');
Route::get('/post/testimonial', 'Blog@testimonial');
Route::get('/post/testimonial/{id}', 'Blog@testimonialdetail');
Route::get('/post/page', 'Blog@page');
Route::get('/post/page/{id}', 'Blog@pagedetail');
Route::get('/post/page/content/{type}', 'Blog@pagecontentdetail');
Route::get('/page/deleteimage/{id}/{name}', 'Blog@deleteimagepage');
Route::get('/post/blog/limit/{limit}/{offset}', 'Blog@postlimit');
Route::get('/post/blog/other/{id}', 'Blog@otherpost');
Route::get('/post/event/other/{id}', 'Blog@otherevent');
Route::get('/post/blog/category/{category}/limit/{limit}/{offset}', 'Blog@postcategorylimit');
Route::get('/post/blog', 'Blog@post');
Route::get('/post/blog/category/{id}', 'Blog@postcategory');
Route::get('/post/blog/{id}', 'Blog@postdetail');
Route::get('/post/category', 'Blog@category');
Route::get('/post/album', 'Blog@album_list');
Route::get('/post/parent_album', 'Blog@parent_album');


Route::get('/post/parent_category', 'Blog@parent_category');
Route::get('/post/sub_parent_category/{id}', 'Blog@sub_parent_category');
Route::get('/post/category/{id}', 'Blog@categorydetail');
Route::get('/post/content/{id}/{index}', 'Blog@postcontentdetail');
Route::get('/post/gallery', 'Blog@gallery');
Route::get('/post/gallery/{id}', 'Blog@gallerydetail');
Route::get('/post/gallery/category/{id}', 'Blog@gallerycategory');
Route::get('/post/gallery/category_parent/{id}', 'Blog@galleryparentcategory');
Route::get('/post/sub_category/{id}', 'Blog@sub_parent_category');


Route::get('/download/show', 'Blog@index_download');
Route::get('/download/show/{id}','Blog@detail_download');



Route::post('/admin/page/update_image_content', 'Blog@update_image_content');
Route::post('/admin/page/create', 'Blog@store_page');
Route::post('/admin/page/update', 'Blog@update_page');
Route::post('/admin/website/update', 'Blog@update_website');
Route::post('/admin/contact/update', 'Blog@update_contact');
Route::post('/admin/social/update', 'Blog@update_social');
Route::post('/admin/post/create', 'Blog@store');
Route::post('/admin/post/update', 'Blog@update');
Route::get('/admin/post/delete/{id}', 'Blog@delete');
Route::post('/admin/gallery/create', 'Blog@store_gallery');
Route::post('/admin/gallery/update', 'Blog@update_gallery');
Route::get('/admin/gallery/delete/{filename}/{id}', 'Blog@delete_gallery');
Route::post('/admin/gallery/bulk_create', 'Blog@bulk_gallery');
Route::get('/admin/gallery/bulk_delete', 'Blog@bulk_deletegallery');
Route::post('/admin/category/create', 'Blog@store_category');
Route::post('/admin/category/update', 'Blog@update_category');
Route::get('/admin/category/delete/{id}', 'Blog@delete_category');
Route::post('/admin/download/create', 'Blog@store_download');
Route::get('/admin/download/delete/{file}/{id}','Blog@delete_download');
Route::post('/admin/download/update','Blog@update_download');
Route::get('/admin/export/{type}', 'Controller@export');

