<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Input;

class Tools extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function resize(){
        if ( !empty( $_FILES ) ) {

            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $name = md5(uniqid(rand(), true));
            $filename = $_FILES['file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $uploadPath1 = getcwd().'/api/image/blog/bless/'.str_replace('.'.$ext,"_1.png",$_FILES['file']['name']);
            $uploadPath2 = getcwd().'/api/image/blog/bless/'.str_replace('.'.$ext,"_2.png",$_FILES['file']['name']);
            $uploadPath3 = getcwd().'/api/image/blog/bless/'.str_replace('.'.$ext,"_3.png",$_FILES['file']['name']);

            if($ext=="jpg"){
                $new = imagecreatefromjpeg($_FILES['file']['tmp_name']);
            }else if($ext=="png"){
                $new = imagecreatefrompng($_FILES['file']['tmp_name']);
            }else if($ext=="gif"){
                $new = imagecreatefromgif($_FILES['file']['tmp_name']);
            }

            imagepng($new,$uploadPath1);
            imagepng($new,$uploadPath2);
            imagepng($new,$uploadPath3);
            // Get new sizes
            //list($width, $height) = getimagesize($tempPath);

            // if($width>=600){
            //     $newwidth = 600;
            //     $newheight = $height * ($newwidth/$width);

            //     // Load
            //     $thumb = imagecreatetruecolor($newwidth, $newheight);
            //     $source = imagecreatefromjpeg($tempPath);

            //     // Resize
            //     imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

            //     // Output
            //     imagejpeg($thumb,$uploadPath);
            // }else{
            //     move_uploaded_file( $tempPath, $uploadPath );
            // }

            // move_uploaded_file($tempPath, $uploadPath1);
            // copy($uploadPath1, $uploadPath2);
            // copy($uploadPath1, $uploadPath3);

            $answer = array( 'answer' => 'File transfer completed' );
            $json = json_encode( $answer );

            return $json;

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
