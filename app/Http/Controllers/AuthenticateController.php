<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Socialite;
use Facebook;
use GuzzleHttp\Client;
use App\User;
use Hash;
use Input;
class AuthenticateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return $users;
    }

    public function authenticate(Request $request)
    {

        $credentials = $request->only('email', 'password','user_role');
        // var_dump($credentials);
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function authenticateFB(Request $request)
    {


        $accessTokenUrl = 'https://graph.facebook.com/v2.3/oauth/access_token?';
        $userApiUrl = 'https://graph.facebook.com/v2.4/me';

        $params = [
            'code' => $request->input('code'),
            'client_id' => $request->input('clientId'),
            'client_secret' => "27c7bc737bc39fe90549f80795641d5b",
            'redirect_uri' => $request->input('redirectUri')
        ];

        $client = new \GuzzleHttp\Client();

        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->get($accessTokenUrl, ['query' => $params]);
        $accessToken = array();
        $json_a = json_decode($accessTokenResponse->getBody(),true);
        // $ck = $accessTokenResponse->json();
        $headers = array('User-Agent' => 'Satellizer');

        $cpeg = json_decode(file_get_contents($userApiUrl."?fields=id%2Cname%2Cemail%2Cfirst_name%2Clast_name%2Cabout%2Clocation&access_token=".$json_a['access_token']),true);
        $mail = $cpeg["email"];
        $location = "";
        $fname = $cpeg["first_name"];
        $lname = $cpeg["last_name"];
        $username = str_replace(" ","",$cpeg["name"]);

        $us = count(User::where('email', $mail)->get());
        $customClaims = ['by' => 'fb'];
        if($us == 0)
        {
            $acc = new User;
            $acc->username = $username;
            $acc->user_fname = $fname;
            $acc->user_lname = $lname;
            $acc->email = $mail;
            $acc->password = Hash::make("aurakasih2015");
            $acc->active_code = "";
            $acc->user_status = "active";
            $acc->join_date = date('Y-m-d H:i:s');
            $acc->user_address = $location;
            $acc->user_role = 1;
            $acc->save();

            try {
                // verify the credentials and create a token for the user
                $user = User::where('email', $mail)->first();
                if (! $token = JWTAuth::fromUser($user)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
        }
        else
        {
            try {
                // verify the credentials and create a token for the user
                $user = User::where('email', $mail)->first();
                if (! $token = JWTAuth::fromUser($user)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
        }
        return response()->json(array('token' => $token));


    }
    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (JWTException $e) {

            // return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
    public function getAuthenticatedRefresh()
    {
        try {


            $newToken = JWTAuth::parseToken()->refresh();


        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('newToken'));
    }
    function imageTolib($path)
    {
        $type = pathinfo($path, PATHINFO_EXTENSION);

            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        return $base64;
    }
    public function getDetailUser($id,$type)
    {
        $lang = User::where('id',$id)
                           ->get();
        if($type == 0)
        {
            foreach($lang as $key)
            {
                if($key->user_photo == "")
                {
                    $key->user_photo = "Profile_03.png";
                }
                else
                {
                    $key->user_photo = $key->user_photo;
                }

                $date_time = strtotime($key->join_date);
                $new_date = date('d M, Y', $date_time);
                $key->join_date =  $new_date;

                if($key->user_phone == "")
                {
                    $key->user_phone = "-";
                }
                else
                {
                    $key->user_phone = $key->user_phone;
                }

                if($key->user_about == "")
                {
                    $key->user_about = "-";
                }
                else
                {
                    $key->user_about = $key->user_about;
                }

                if($key->user_address == "")
                {
                    $key->user_address = "-";
                }
                else
                {
                    $key->user_address = $key->user_address;
                }

            }

        }else if($type == 1)
        {
            foreach($lang as $key)
            {
                if($key->user_photo == "")
                {
                    $user_photo = "Profile_03.png";
                }
                else
                {
                    $user_photo = $key->user_photo;
                }

                $key->user_photo = $this->imageTolib('api/image/profile/'. $user_photo);
            }
        }
        return response()->json($lang);
    }
}
