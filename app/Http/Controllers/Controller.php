<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\DataModel;
use Excel;
use Auth;

class Controller extends BaseController
{

    public function export($type)
    {
            $d = DataModel::where('type',$type)->get();
            $r =array();
            $dt =array();
            $no = 0;
            foreach($d as $key)
            {
                $datan = json_decode($key->data);
                foreach($datan as $ky)
                {
                $dt[$no][$ky->name] = $ky->value;
                }
                $no++;
            }
            $today = date("Ymd-his");
            $namefile = $type.'_'.$today;
            return Excel::create($namefile, function($excel) use($dt) {
                $excel->sheet('Sheet 1', function($sheet) use($dt) {
                    $sheet->fromArray($dt);
                });
            })->download('xls');
            echo "<script>window.close();</script>";
       

    }

    public function detail_data($id)
    {
        $d = DataModel::where('id',$id)->get();
        $r =array();
        foreach($d as $key)
        {
            $r['id'] = $key->id;
            $r['title'] = $key->title;
            $r['updated_at'] = $key->updated_at;
            $r['data'] = json_decode($key->data);
        }
        return response()->json($r);
    }
    public function list_data($type)
    {
        $d = DataModel::where('type',$type)->get();
        $r =array();
        $no = 0;
        foreach($d as $key)
        {
            $r[$no]['id'] = $key->id;
            $r[$no]['title'] = $key->title;
            $r[$no]['updated_at'] = $key->updated_at;
            $no++;
        }
        return response()->json($r);
    }
    public function apply()
    {
    	$message = array();
		$dataNa = input::get("data");
		$dataTitle = input::get("title");
		$data = new DataModel;

        $data->title = $dataTitle;
        $data->type = 'apply';
        $data->data = json_encode($dataNa);
        try {
            $data->save();


            try{
                $mailN = $this->sendMail( $dataTitle.' - Eduworld Apply Online', $dataNa);
                $message = array("status"=>"Successfully sent your data","t"=>1, 'mail'=>$mailN);
            }catch(Exception $e){
                $message = array("status"=>"Data saved but did not send to email","t"=>0);
            }


    	}catch(Exception $e){
            $message = array("status"=>"Failed to save your data","t"=>0);
        }
        return response()->json($message);
    }
    public function subscribe()
    {
    	$message = array();
		$dataTitle = input::get("title");
		$data = new DataModel;

        $data->title = $dataTitle;
        $data->type = 'subscribe';
        try {
            $data->save();
    		$message = array("status"=>"Successfully sent your data","t"=>1);
    	}catch(Exception $e){
            $message = array("status"=>"Failed to save your data","t"=>0);
        }
        return response()->json($message);
    }
    public function contact()
    {
    	$message = array();
		$dataNa = input::get("data");
		$dataTitle = $dataNa[0]['value'];
		$data = new DataModel;

        $data->data = json_encode($dataNa);
        $data->title = $dataTitle;
        $data->type = 'contact';
        try {
            $data->save();


            try{


                $mailN = $this->sendMail('Contact | Eduworld', $dataNa);
                $message = array("status"=>"Successfully sent your data","t"=>1, 'mail'=>$mailN);
            }catch(Exception $e){
                $message = array("status"=>"Data saved but did not send to email","t"=>0);
            }
    	}catch(Exception $e){
            $message = array("status"=>"Failed to save your data","t"=>0);
        }
        return response()->json($message);
    }
    public function enquiry()
    {
    	$message = array();
		$dataNa = input::get("data");
		$dataTitle = $dataNa[0]['value'].' ( '.$dataNa[1]['value'].' )';
		$data = new DataModel;

        $data->data = json_encode($dataNa);
        $data->title = $dataTitle;
        $data->type = 'enquiry';
        try {
            $data->save();

            try{


                $mailN = $this->sendMail('Enquiry | Eduworld', $dataNa);
                $message = array("status"=>"Successfully sent your data","t"=>1, 'mail'=>$mailN);
            }catch(Exception $e){
                $message = array("status"=>"Data saved but did not send to email","t"=>0);
            }
    	}catch(Exception $e){
            $message = array("status"=>"Failed to save your data","t"=>0);
        }
        return response()->json($message);
    }

    public function sendMail($subject, $data){

        Mail::send('sendmail', ['data' => $data], function ($message) use ($subject)
        {

            $message->from('developer@madappe.com', 'developer madappe');

            $message->subject($subject);

            $message->to('info@eduworld.co.th');

            $message->cc('eduworldbangkok@gmail.com');


        });

        return response()->json(['message' => 'Request completed']);


    }
}
