<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\BlogModel;
use App\BlogDesModel;
use App\CategoryModel;
use App\SettingModel;
use App\CategoryDesModel;
use App\AuthorModel;
use App\ContactModel;
use App\WebsiteModel;
use App\SocialModel;
use App\CommentModel;
use App\TypeModel;
use App\CommentLoveModel;
use App\ImageModel;
use App\Banner;
use Illuminate\Support\Facades\Input;
use Auth;
use ZipArchive;

// use Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class Blog extends Controller
{

	public function store()
    {
        $message = array();

        $blog = new BlogModel;

        $vsbl = input::get("visibility");

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = (isset($vsbl)?$vsbl:'visible');
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        $data1 = input::get("imag1");
        $data2 = input::get("imag2");
        $data3 = input::get("imag3");

        $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
		$img1Na = md5(uniqid(rand(), true)).'1.png';
		file_put_contents('api/image/blog/'.$img1Na, $data1);

		if($data2 != ''){
			$data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
			$img2Na = md5(uniqid(rand(), true)).'2.png';
			file_put_contents('api/image/blog/'.$img2Na, $data2);
        	$blog->post_thumbnail2 = $img2Na;
		}
		if($data3 != ''){
			$data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
			$img3Na = md5(uniqid(rand(), true)).'3.png';
			file_put_contents('api/image/blog/'.$img3Na, $data3);
        	$blog->post_thumbnail3 = $img3Na;
		}

        $clr = input::get("color");
        if(isset($clr) && $clr != ''){
            $blog->additional = $clr;
        }

        $blog->post_thumbnail = $img1Na;

        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->post_id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }
	public function store_page()
    {
        $message = array();

        $blog = new BlogModel;

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = input::get("pagetype");

        $blog->post_ordered = "1";





        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);


                    $data1 = input::get("imag1");
                    $img1Na = array();
                    $no = 1;

                    $imgN = new ImageModel;

                    if(isset($data1))
                    {
                        foreach($data1 as $da){
                            $datas = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $da['name']));
                            $img1N = md5(uniqid(rand(), true)).'.png';
                            file_put_contents('api/image/blog/'.$img1N, $datas);

                            DB::table('madappe_post_image')->insert(
                                ['image' => $img1N, 'post_id' => $blog->post_id]
                            );
                            $no++;
                        }
                    }

                    $message = array("status"=>"Insert Data Success","id"=>$blog->post_id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }

    public function bulk_gallery()
    {
        $message = array();




        $data1 = input::get("imag1");
        $img1Na = array();
        $no = 1;

        $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);


        if(isset($data1))
        {
            foreach($data1 as $da){

                $datas = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $da['name']));
                $img1N = md5(uniqid(rand(), true)).'.png';
                file_put_contents('api/image/gallery/'.$img1N, $datas);

                $blog = new BlogModel;

                $blog->post_thumbnail = $img1N;

                $blog->post_author = input::get("author");
                $blog->post_date = date("Y-m-d H:i:s");
                $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
                $blog->post_type_id = input::get("type");
                $blog->post_categories_id = input::get("category");
                $blog->post_status = input::get("visibility");
                $blog->post_publish_date = date("Y-m-d H:i:s");
                $blog->post_comment_status = "active";
                $blog->post_comment_count = "1";
                $blog->post_password = "";
                $blog->post_modified = date("Y-m-d H:i:s");
                $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
                $blog->post_type = 'gallery';

                $blog->post_ordered = "1";

                try {
                    $blog->save();
                        try {
                            $blogDes = new BlogDesModel;


                            $blogDes->pd_content = input::get("content").' '.$no;
                            $blogDes->pd_title = input::get("title").' '.$no;
                            $blogDes->pd_excerpt = input::get("excerpt").' '.$no;
                            $blogDes->pd_meta_description = input::get("pagetitle").' '.$no;
                            $blogDes->pd_pagetitle = input::get("metadecription").' '.$no;
                            $blogDes->pd_meta_keyword ='';
                            $blogDes->language = "1";

                            $blog->BlogDesModels()->save($blogDes);



                            $message = array("status"=>"Insert Data Success","id"=>$blog->post_id,"t"=>1);

                        } catch (Exception $e) {
                            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                        }

                }
                catch(Exception $e){
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

                $no++;

            }
        }


        return response()->json($message);

    }
	public function store_gallery()
    {
        $message = array();

        $blog = new BlogModel;

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        $data1 = input::get("imag1");
        $data2 = input::get("imag2");
        $data3 = input::get("imag3");

        $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
		$img1Na = md5(uniqid(rand(), true)).'1.png';
		file_put_contents('api/image/gallery/'.$img1Na, $data1);

		if($data2 != ''){
			$data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
			$img2Na = md5(uniqid(rand(), true)).'2.png';
			file_put_contents('api/image/gallery/'.$img2Na, $data2);
        	$blog->post_thumbnail2 = $img2Na;
		}
		if($data3 != ''){
			$data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
			$img3Na = md5(uniqid(rand(), true)).'3.png';
			file_put_contents('api/image/gallery/'.$img3Na, $data3);
        	$blog->post_thumbnail3 = $img3Na;
		}

        $blog->post_thumbnail = $img1Na;

        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("metadescription");
                    $blogDes->pd_pagetitle = input::get("pagetitle");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }

	public function store_category()
    {
        $message = array();

        $categoryM = new CategoryModel;

        $categoryM->type_id = input::get("type");
        $categoryM->parent_id = input::get("parent_id");
        $categoryM->status = input::get("visibility");
        $categoryM->categories_created = date("Y-m-d H:i:s");
        $categoryM->categories_modified = date("Y-m-d H:i:s");
        if(input::get("thumbnail")!=""){
            $data2 = input::get("thumbnail");
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            $name = md5(uniqid(rand(), true)).'.png';
            $categoryM->post_thumbnail = $name;
            file_put_contents('api/image/blog/'.$name, $data2);
        }
        try {
            $categoryM->save();
			try {
                    $categoryMDes = new CategoryDesModel;

                    $categoryMDes->cd_name = input::get("name");
                    $categoryMDes->cd_meta_description = input::get("metadecription");
                    $categoryMDes->cd_meta_keyword = input::get("metakeyword");
                    $categoryMDes->language_id = "1";

                    $categoryM->CategoryDesModel()->save($categoryMDes);

                    $message = array("status"=>"Insert Data Success","id"=>$categoryM->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }
        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }
	public function update_contact()
    {
        $message = array();
		$dataGet = input::get("data");

        try {
            foreach($dataGet as $dt => $value){
				$slug = $value['name'];
				$value = $value['value'];
				$thisUpdate = array(
					 'value' => $value
				);
				$update = ContactModel::where('slug',$slug)->update($thisUpdate);

			}
			$message = array("status"=>"Success","id"=>$update,"t"=>1);
        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }
	public function update_social()
    {
        $message = array();
		$dataGet = input::get("data");

        try {
            foreach($dataGet as $dt => $value){
				$slug = $value['name'];
				$value = $value['value'];
				$valueUpdate = array(
					 'value' => $value,
				);
				$nameUpdate = array(
					 'name' => $value,
				);

				$pieces = explode("-", $slug);

				$nm = $pieces[0];
				$nm2 = $pieces[1];

				if($nm == 'name'){
					$update = SocialModel::where('slug',$nm2)->update($nameUpdate);
				}else{
					$update = SocialModel::where('slug',$nm2)->update($valueUpdate);
				}


			}
			$message = array("status"=>"Success","id"=>$update,"t"=>1);
        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }

	 /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");

        $blog->post_ordered = "1";


        if(input::get("imag1") != ""){

            $data1 = input::get("imag1");
            $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
            $name = md5(uniqid(rand(), true)).'1.png';
            $blog->post_thumbnail = $name;
            file_put_contents('api/image/blog/'.$name, $data1);
        }

        if(input::get("imag2")!=""){
            $data2 = input::get("imag2");
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            $name = md5(uniqid(rand(), true)).'2.png';
            $blog->post_thumbnail2 = $name;
            file_put_contents('api/image/blog/'.$name, $data2);
        }

        if(input::get("imag3")!=""){
            $data3 = input::get("imag3");
            $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
            $name = md5(uniqid(rand(), true)).'3.png';
            file_put_contents('api/image/blog/'.$name, $data3);
            $blog->post_thumbnail3 = $name;
        }

        $clr = input::get("color");
        if(isset($clr) && $clr != ''){
            $blog->additional = $clr;
        }

        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_image_content()
    {
        $message = array();

        $img = ImageModel::where('id', input::get("id"))->first();

        $img->title = input::get("title");
        $img->description = input::get("description");
        $img->link = input::get("link");
        try {
            $img->save();
            $message = array("status"=>"Update Data Success","id"=>$img->id,"t"=>1);
        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_page()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");

        $blog->post_ordered = "1";






        try {
            $blog->save();
                try {






                    if(input::get("imag1")!=""){
                        $data1 = input::get("imag1");
                        $img1Na = array();
                        $no = 1;
                        if(isset($data1))
                        {
                            foreach($data1 as $da){
                                $datas = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $da['name']));
                                $img1N = md5(uniqid(rand(), true)).'.png';
                                file_put_contents('api/image/blog/'.$img1N, $datas);

                                DB::table('madappe_post_image')->insert(
                                    ['image' => $img1N, 'post_id' => $blog->post_id]
                                );
                                $no++;
                            }
                        }
                    }



                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;



                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_gallery()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");

        $blog->post_ordered = "1";



        if(input::get("imag1")!=""){
            $data1 = input::get("imag1");
            $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
            $name = md5(uniqid(rand(), true)).'1.png';
            $blog->post_thumbnail = $name;
            file_put_contents('api/image/gallery/'.$name, $data1);
        }
        if(input::get("imag2")!=""){
            $data2 = input::get("imag2");
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            $name = md5(uniqid(rand(), true)).'2.png';
            $blog->post_thumbnail2 = $name;
            file_put_contents('api/image/gallery/'.$name, $data2);
        }

        if(input::get("imag3")!=""){
            $data3 = input::get("imag3");
            $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
            $name = md5(uniqid(rand(), true)).'3.png';
            file_put_contents('api/image/gallery/'.$name, $data3);
            $blog->post_thumbnail3 = $name;
        }

        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("metadescription");
                    $blogDes->pd_pagetitle = input::get("pagetitle");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }/**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_website()
    {
        $message = array();

        $web = WebsiteModel::where('type', 'website_name')->update(array('value' => input::get("website_name")));
        $web = WebsiteModel::where('type', 'description')->update(array('value' => input::get("description")));
        $web = WebsiteModel::where('type', 'main_email')->update(array('value' => input::get("email")));
        $web = WebsiteModel::where('type', 'title')->update(array('value' => input::get("title")));
        $web = WebsiteModel::where('type', 'website_name')->update(array('value' => input::get("website_name")));
		$tags = input::get("keyword");
		$count_tags = sizeof($tags);
		$tag_result = array();
		for ($i=0; $i < $count_tags; $i++) {
			$tag_result[$i] = $tags[$i]["text"];
		}
		$tags_re = implode(",",$tag_result);
        $web = WebsiteModel::where('type', 'keyword')->update(array('value' => $tags_re));

        if(input::get("logolight")!=""){
            $data2 = input::get("logolight");
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            $name = md5(uniqid(rand(), true)).'_light.png';
			$web = WebsiteModel::where('type', 'logo_light')->update(array('value' => $name));
            file_put_contents('api/image/website/'.$name, $data2);
        }

        if(input::get("logodark")!=""){
            $data3 = input::get("logodark");
            $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
            $names = md5(uniqid(rand(), true)).'_dark.png';
			$web = WebsiteModel::where('type', 'logo_dark')->update(array('value' => $names));
            file_put_contents('api/image/website/'.$names, $data3);
        }

		$message = array("status"=>"Update Data Success","id"=>1,"t"=>1);
        return response()->json($message);
    }
	 /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_category()
    {
        $message = array();

        $categoryM = CategoryModel::where('categories_id', input::get("category_id"))->first();

        $categoryM->type_id = input::get("type");
        $categoryM->parent_id = input::get("parent_id");
        $categoryM->status = input::get("visibility");
        $categoryM->categories_created = date("Y-m-d H:i:s");
        $categoryM->categories_modified = date("Y-m-d H:i:s");
        if(input::get("thumbnail")!=""){
            $data2 = input::get("thumbnail");
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            $name = md5(uniqid(rand(), true)).'.png';
            $categoryM->post_thumbnail = $name;
            file_put_contents('api/image/blog/'.$name, $data2);
        }
        try {
            $categoryM->save();
			try {
                    $categoryMDes = CategoryDesModel::where('categories_id', input::get("category_id"))->first();

                    $categoryMDes->cd_name = input::get("name");
                    $categoryMDes->cd_meta_description = input::get("metadecription");
                    $categoryMDes->cd_meta_keyword = input::get("metakeyword");
                    $categoryMDes->language_id = "1";

                    $categoryM->CategoryDesModel()->save($categoryMDes);

                    $message = array("status"=>"Insert Data Success","id"=>$categoryM->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }
        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }

    /**
     * All Post
     *
     * @param  int  $id
     * @return Response
     */
    public function post()
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','1')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;
		}
        return response()->json($lang);
    }

    public function postcategory($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','1')
                            ->where('madappe_blog_post.post_categories_id',$id)
                            ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                            ->orderBy('madappe_blog_post.post_date','desc')
                            ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }
        return response()->json($lang);
    }

    /**
     * All Post Limit
     *
     * @param  int  $id
     * @return Response
     */
    public function postcategorylimit($category,$limit,$offset)
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','1')
                           ->where('madappe_blog_post.post_categories_id',$category)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->skip($limit)->take($offset)
                           ->orderBy('madappe_blog_post.post_date','desc')
                           ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }
        return response()->json($lang);
    }

    /**
     * All Post Limit
     *
     * @param  int  $id
     * @return Response
     */
    public function postlimit($limit,$offset)
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','1')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->skip($limit)->take($offset)
                           ->orderBy('madappe_blog_post.post_id','desc')
                           ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
	 /**
     * Display Download
     *
     * @return Response
     */
    public function index_download($order = 'asc')
    {
        $lang = BlogModel::where('post_type_id', '5')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date',$order)
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
	/**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */

	public function getExtension($mime_type){

		$extensions = array('application/excel' => 'xls',
							'application/vnd-ms-excel' => 'xls',
							'application/x-excel' => 'xls',
							'application/x-msexcel' => 'xls',
							'application/pdf' => 'pdf',
							'application/msword' => 'doc',
							'application/mspowerpoint' => 'ppt',
							'application/powerpoint' => 'ppt',
							'application/vnd-ms-powerpoint' => 'ppt',
							'application/x-mspowerpoint' => 'ppt',
							'application/x-compressed' => 'zip',
							'application/x-zip-compressed' => 'zip',
							'application/zip' => 'zip',
							'application/x-zip' => 'zip'
						   );

		// Add as many other Mime Types / File Extensions as you like

		return '.'.$extensions[$mime_type];

	}
	public function getB64Type($str) {
		return substr($str, 5, strpos($str, ';')-5);
	}

    public function store_download()
    {
        $message = array();

        $blog = new BlogModel;

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "download";

        $blog->post_ordered = "1";

        $data2 = input::get("imag2");
        $data3 = input::get("imag3");

		$mimes = $this->getB64Type($data2);
		$tp  = $this->getExtension($mimes);

        $data2 = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $data2));

        $pdf = md5(uniqid(rand(), true)).$tp;
        file_put_contents('api/files/'.$pdf, $data2);

        $sizePDF = filesize('api/files/'.$pdf);
        $blog->post_thumbnail2 = $pdf;


        // Output
        //imagejpeg($thumb,$uploadPath);

        $name = md5(uniqid(rand(), true)).'_cover.png';
        if($data3 != ''){
            $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
            file_put_contents('api/image/blog/'.$name, $data3);
            $blog->post_thumbnail3 = $name;
        }





        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_pagetitle = input::get("pagetitle");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = $sizePDF;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }

    /**
     * Post Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function otherevent($id)
    {
        $next = BlogModel::where('madappe_blog_post.post_id','>',$id)
                           ->where('madappe_blog_post.post_type_id',1)
                           ->where('madappe_blog_post.post_categories_id',46)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->take(1)
                           ->orderBy('madappe_blog_post.post_date', 'asc')
                           ->get();
        foreach($next as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }



        $prev = BlogModel::where('madappe_blog_post.post_id','<',$id)
                           ->where('madappe_blog_post.post_type_id',1)
                           ->where('madappe_blog_post.post_categories_id',46)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->take(1)
                           ->orderBy('madappe_blog_post.post_date', 'desc')
                           ->get();
        foreach($prev as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }
        $resp = array('next'=>$next,'prev'=>$prev);
        return response()->json($resp);
    }
    /**
     * Post Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function otherpost($id)
    {
        $next = BlogModel::where('madappe_blog_post.post_id','>',$id)
                           ->where('madappe_blog_post.post_type_id',1)
                           ->where('madappe_blog_post.post_categories_id',2)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->take(1)
                           ->orderBy('madappe_blog_post.post_id', 'asc')
                           ->get();
        foreach($next as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }


        $prev = BlogModel::where('madappe_blog_post.post_id','<',$id)
                           ->where('madappe_blog_post.post_type_id',1)
                           ->where('madappe_blog_post.post_categories_id',2)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->take(1)
                           ->orderBy('madappe_blog_post.post_id', 'desc')
                           ->get();
        foreach($prev as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }
        $resp = array('next'=>$next,'prev'=>$prev);
        return response()->json($resp);
    }

    /**
     * Post Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function postcontentdetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)->get();
        foreach ($lang as $value) {
            $cn = $value->post_thumbnail4;
        }
        explode(',', $cn);
        return response()->json($cn);
    }

    /**
     * Post Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function postdetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;
		}
        return response()->json($lang);
    }

    /**
     * All Post Category
     *
     * @param  int  $id
     * @return Response
     */
    public function category()
    {
        $lang = CategoryModel::get();
		foreach($lang as $key)
		{
            $thm = $key->post_thumbnail;
            $key->image = '';
            if(isset($thm) && $thm != NULL){
                // $path1 = $this->imageTolib('api/image/blog/'.$thm);
                $path1 = 'api/image/blog/'.$thm;

                $key->image = $path1;
            }

			$cate = CategoryDesModel::where('categories_id',$key->categories_id)
                       ->get();
			$yo = "";
			foreach($cate as $ko)
			{
				$yo = $ko->cd_name;
				$ho = $ko->cd_meta_description;
				$do = $ko->cd_meta_keyword;
			}
			$key->cd_name = $yo;
			$key->cd_meta_description = $ho;
			$key->cd_meta_keyword = $do;

			$parent = $key->parent_id;

			$key->cd_parent = 'Root';

			if($parent != 0){
				$cateParent = CategoryDesModel::where('categories_id',$key->parent_id)
						   ->get();
				$yos = "";
				foreach($cateParent as $kos)
				{
					$yos = $kos->cd_name;
				}
				$key->cd_parent = $yos;
			}
		}
        return response()->json($lang);
    }

    public function parent_category()
    {
        $lang = CategoryModel::where('parent_id',0)->where('categories_id','!=',0)->get();
        foreach($lang as $key)
        {

            $thm = $key->post_thumbnail;
            $key->image = '';
            if(isset($thm) && $thm != NULL){
                // $path1 = $this->imageTolib('api/image/blog/'.$thm);
                $path1 = 'api/image/blog/'.$thm;
                $key->image = $path1;
            }

            $cate = CategoryDesModel::where('categories_id',$key->categories_id)
                       ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
                $ho = $ko->cd_meta_description;
                $do = $ko->cd_meta_keyword;
            }
            $key->cd_name = $yo;
            $key->cd_meta_description = $ho;
            $key->cd_meta_keyword = $do;
            $parent = $key->parent_id;
            $key->children = $this->children_category($key->categories_id);
        }
        return response()->json($lang);
    }
    
    

    public function sub_parent_category($id)
    {
        $lang = CategoryModel::where('parent_id',$id)->where('categories_id','!=',0)->get();
        foreach($lang as $key)
        {

            $thm = $key->post_thumbnail;
            $key->image = '';
            if(isset($thm) && $thm != NULL){
                // $path1 = $this->imageTolib('api/image/blog/'.$thm);
                $path1 = 'api/image/blog/'.$thm;
                $key->image = $path1;
            }

            $cate = CategoryDesModel::where('categories_id',$key->categories_id)
                       ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
                $ho = $ko->cd_meta_description;
                $do = $ko->cd_meta_keyword;
            }
            $key->cd_name = $yo;
            $key->cd_meta_description = $ho;
            $key->cd_meta_keyword = $do;
            $parent = $key->parent_id;
            $key->children = $this->children_category($key->categories_id);
        }
        return response()->json($lang);
    }

    /**
     * Top Menu
     *
     * @param  int  $id
     * @return Response
     */
    public function show_menu()
    {
        $lang = CategoryModel::where('type_id',7)->where('parent_id',0)->whereNotIn('categories_id', [19, 20])->get();
		foreach($lang as $key)
		{

			$cate = CategoryDesModel::where('categories_id',$key->categories_id)
					   ->get();
			$yo = "";
			foreach($cate as $ko)
			{
				$yo = $ko->cd_name;
				$ho = $ko->cd_meta_description;
				$do = $ko->cd_meta_keyword;
			}
			$key->cd_name = $yo;
			$key->cd_meta_description = $ho;
			$key->cd_meta_keyword = $do;
			$langs ='';
			$length = 0;
			$langs = CategoryModel::where('madappe_blog_categories.parent_id',$key->categories_id)
                         ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
						->take(8)
                         ->get();

			$length =count($langs);
			$key->length = $length;
			if($length > 0){
				$key->children = $langs;
				foreach($langs as $keys)
				{
					$keys->menu_name = $keys->cd_name;
					$keys->menu_id = $keys->categories_id;
					$keys->menu_type = 'category';



                    $langss = BlogModel::where('madappe_blog_post.post_categories_id',$keys->categories_id)
                            ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                            ->orderBy('madappe_blog_post.post_id','ASC')
                            ->take(8)
                            ->get();
                    $xjfg = array();
                    $idm = 0;
                    foreach($langss as $keyss)
                    {
                        $xjfg[$idm]['menu_name'] = $keyss->pd_title;
                        $xjfg[$idm]['menu_id'] = $keyss->post_id;
                        $xjfg[$idm]['menu_type'] = 'post';
                        $idm++;
                    }
                    $keys->children_post = $xjfg;


				}
			}



		}
        return response()->json($lang);
    }

    /**
     * Post Category Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function categorydetail($id)
    {
        $lang = CategoryModel::where('madappe_blog_categories.categories_id',$id)
                           ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                           ->get();
		foreach($lang as $key)
		{
            $thm = $key->post_thumbnail;
            $key->image = '';
            if(isset($thm) && $thm != NULL){
                $path1 = $this->imageTolib('api/image/blog/'.$thm);
                $key->image = $path1;
            }

			$cate = CategoryDesModel::where('categories_id',$key->categories_id)
                       ->get();
			$yo = "";
			foreach($cate as $ko)
			{
				$yo = $ko->cd_name;
				$ho = $ko->cd_meta_description;
				$do = $ko->cd_meta_keyword;
			}
			$key->cd_name = $yo;
			$key->cd_meta_description = $ho;
			$key->cd_meta_keyword = $do;

			$parent = $key->parent_id;

			$key->cd_parent = 'Root';

			if($parent != 0){
				$cateParent = CategoryDesModel::where('categories_id',$key->parent_id)
						   ->get();
				$yos = "";
				foreach($cateParent as $kos)
				{
					$yos = $kos->cd_name;
				}
				$key->cd_parent = $yos;
			}
		}
        return response()->json($lang);
    }

    /**
     * About Us
     *
     * @param  int  $id
     * @return Response
     */
    public function about()
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','2')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }

    /**
     * About Us
     *
     * @param  int  $id
     * @return Response
     */
    public function services()
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','7')
                        ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                        ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;


        }
        return response()->json($lang);
    }
    /**
     * Services
     *
     * @param  int  $id
     * @return Response
     */
    public function summercamp()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id',19)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_id','DESC')
                           ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;


        }
        return response()->json($lang);
    }
    /**
     * Services
     *
     * @param  int  $id
     * @return Response
     */
    public function servicescategorydetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_id','ASC')
                           ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;


        }
        return response()->json($lang);
    }
    /**
     * About Us
     *
     * @param  int  $id
     * @return Response
     */
    public function aboutcategorydetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }
    /**
     * About Us
     *
     * @param  int  $id
     * @return Response
     */
    public function aboutlist()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','1')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }
    /**
     * About Us Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function servicesdetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    /**
     * About Us Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function aboutdetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;
		}
        return response()->json($lang);
    }
    /**
     * Testimonial
     *
     * @param  int  $id
     * @return Response
     */
    public function testimonial()
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','6')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }
    /**
     * About Us Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function testimonialdetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;
		}
        return response()->json($lang);
    }
    /**
     * Gallery
     *
     * @param  int  $id
     * @return Response
     */
    public function gallery()
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','4')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/gallery/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }
    /**
     * Gallery
     *
     * @param  int  $id
     * @return Response
     */
    public function gallerycategory($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/gallery/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }
    /**
     * Gallery
     *
     * @param  int  $id
     * @return Response
     */
    public function galleryparentcategory($id)
    {
        $kt = array();
        $ct = CategoryModel::where('type_id',4)->where('parent_id',$id)->get();
        foreach($ct as $key)
        {
            $kt[] = $key->categories_id;
        }
        $ktNa = implode(',' , $kt);

        $lang = BlogModel::whereRaw('madappe_blog_post.post_categories_id in('.$ktNa.')')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        foreach($lang as $key)
        {
                //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/gallery/'.$key->post_thumbnail;
                $key->image = $path1;

                $img_tags = explode(",",$key->pd_meta_keyword);
                $count = count($img_tags);
                $tags = "[";
                for ($i=0; $i < $count; $i++) {
                    // echo $i;
                    $tags .= '{text:"'.$img_tags[$i].'"},';
                }

                $tags .= "]";

                $key->tagsa = $tags;

                $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
                $yo = "";
                foreach($cate as $ko)
                {
                    $yo = $ko->cd_name;
                }
                $key->cd_name = $yo;


        }
        return response()->json($lang);
    }

    /**
     * Gallery Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function gallerydetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
				//$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                $path1 = '/api/image/gallery/'.$key->post_thumbnail;
				$key->image = $path1;

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;
		}
        return response()->json($lang);
    }
    /**
     * Page Content
     *
     * @param  int  $id
     * @return Response
     */
    public function page()
    {
        $lang = BlogModel::where('madappe_blog_post.post_type_id','3')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


		}
        return response()->json($lang);
    }
    /**
     * Page Detail
     *
     * @param  int  $id
     * @return Response
     */
    public function pagedetail($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{

				$img_tags = explode(",",$key->pd_meta_keyword);
				$count = count($img_tags);
				$tags = "[";
				for ($i=0; $i < $count; $i++) {
					// echo $i;
					$tags .= '{text:"'.$img_tags[$i].'"},';
				}

				$tags .= "]";

				$key->tagsa = $tags;

				$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
				$yo = "";
				foreach($cate as $ko)
				{
					$yo = $ko->cd_name;
				}
				$key->cd_name = $yo;


                $imgM = ImageModel::where('post_id',$key->post_id)
                           ->get();
                $key->image_list = $imgM;
		}
        return response()->json($lang);
    }

	/**
     * Page Detail With Type
     *
     * @param  int  $id
     * @return Response
     */
    public function pagecontentdetail($type)
    {
        $lang = BlogModel::where('madappe_blog_post.post_type',$type)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{

			$img_tags = explode(",",$key->pd_meta_keyword);
			$count = count($img_tags);
			$tags = "[";
			for ($i=0; $i < $count; $i++) {
				// echo $i;
				$tags .= '{text:"'.$img_tags[$i].'"},';
			}

			$tags .= "]";

			$key->tagsa = $tags;

			$cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
					    ->get();
			$yo = "";
			foreach($cate as $ko)
			{
				$yo = $ko->cd_name;
			}
			$key->cd_name = $yo;

            $imgM = ImageModel::where('post_id',$key->post_id)
                           ->get();
            $key->image_list = $imgM;
		}
        return response()->json($lang);
    }
	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
	public function type_category($id)
    {
        $lang = CategoryModel::where('type_id', $id)
                           ->where('parent_id', 0)
                           ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                           ->get();
        foreach($lang as $key)
        {
            $thm = $key->post_thumbnail;
            $key->image = '';
                if(isset($thm) && $thm != NULL){
                    $path1 = $this->imageTolib('api/image/blog/'.$thm);
                    $key->image = $path1;
                }
            $key->children = $this->children_category($key->categories_id);
        }

        return response()->json($lang);
    }

    
	public function album_list()
    {
        $lang = CategoryModel::where('type_id', 4)->where('madappe_blog_categories.categories_id','!=' , 14)->where('madappe_blog_categories.categories_id','!=' , 21)
                           ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                           ->get();
        foreach($lang as $key)
        {
            $thm = $key->post_thumbnail;
            $key->image = '';
                if(isset($thm) && $thm != NULL){
                    $path1 = 'api/image/blog/'.$thm;
                    $key->image = $path1;
                }
            $prnt = $this->parent_category_id($key->parent_id);
            $key->parent = $prnt;
        }

        return response()->json($lang);
    }
    
    public function parent_album()
    {
        $lang = CategoryModel::where('type_id',4)->where('parent_id',0)->where('categories_id','!=',0)->get();
        foreach($lang as $key)
        {

            $thm = $key->post_thumbnail;
            $key->image = '';
            if(isset($thm) && $thm != NULL){
                // $path1 = $this->imageTolib('api/image/blog/'.$thm);
                $path1 = 'api/image/blog/'.$thm;
                $key->image = $path1;
            }

            $cate = CategoryDesModel::where('categories_id',$key->categories_id)
                       ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
                $ho = $ko->cd_meta_description;
                $do = $ko->cd_meta_keyword;
            }
            $key->cd_name = $yo;
            $key->cd_meta_description = $ho;
            $key->cd_meta_keyword = $do;
            $parent = $key->parent_id;
            $key->children = $this->children_category($key->categories_id);
        }
        return response()->json($lang);
    }

    public function parent_category_id($id)
    {
        if($id == 0){
            return 0;
        }else{
            $lang = CategoryModel::where('madappe_blog_categories.categories_id', $id)
                            ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                            ->get();
            $ttl = count($lang);
            if($ttl > 0){
                foreach($lang as $key)
                {
                    $thm = $key->post_thumbnail;
                    $key->image = '';
                        if(isset($thm) && $thm != NULL){
                            $path1 = 'api/image/blog/'.$thm;
                            $key->image = $path1;
                        }
                }
            }

            return $lang[0];
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function children_category($id)
    {

        $lang = CategoryModel::where('parent_id', $id)
                           ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                           ->get();
        $ttl = count($lang);
        if($ttl > 0){
            foreach($lang as $key)
            {
                $thm = $key->post_thumbnail;
                $key->image = '';
                    if(isset($thm) && $thm != NULL){
                        $path1 = $this->imageTolib('api/image/blog/'.$thm);
                        $key->image = $path1;
                    }
                $key->children = $this->children_category($key->categories_id);
            }
        }

        return $lang;
    }

	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
	public function setting($type)
    {
        $lang = SettingModel::where('type', $type)->get();
		$value = $lang[0]['value'];
        return response()->json($value);
    }

    /**
     * Display All Type
     *
     * @return Response
     */
    public function allType()
    {
        $lang = TypeModel::get();
        return response()->json($lang);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $message="";
        $remove = BlogModel::where('post_id',$id)->delete();
            if($remove){
                $remove2 = BlogDesModel::where('post_id',$id)->delete();
                if($remove2)
                {
                    $message = array("status"=>"Post successfully deleted");
                }
            }
                    return response()->json($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteimagepage($id,$name)
    {
		ImageModel::where('id',$id)->delete();
        if($name != ''){
            File::delete('api/image/blog/'.$name);
        }

		$message = array("status"=> "image successfully deleted");
        return response()->json($message);
    }

    public function deleteimage($path)
    {
		
        File::delete($path);
       
    }


    /**
     * Remove Categories
     *
     * @param  int  $id
     * @return Response
     */
    public function delete_category($id)
    {

        $lang = BlogModel::where('madappe_blog_post.post_categories_id',$id)
                            ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                            ->orderBy('madappe_blog_post.post_date','desc')
                            ->get();
        foreach($lang as $key)
        {
            if($key->post_type_id == 4){
                $path1 = '/api/image/gallery/'.$key->post_thumbnail;
            }else{
                $path1 = '/api/image/blog/'.$key->post_thumbnail;
            }
                
            $this->deleteimage($path1);
        }
        $message="";
        $remove = CategoryModel::where('categories_id',$id)->delete();
            if($remove){
                $remove2 = CategoryDesModel::where('categories_id',$id)->delete();
                if($remove2)
                {
                    $message = array("status"=>"Post successfully deleted");
                }
            }
        return response()->json($message);
    }
	public function detail_download($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                    $thumbNa = $key->post_thumbnail3;
                    $path1 = (isset($thumbNa) && $thumbNa != ''? $this->imageTolib('api/image/blog/'.$thumbNa):'');
                    // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                    // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                    $key->image = $path1;
                    // $key->image2 = $path2;
                    // $key->image3 = $path3;

                    $img_tags = explode(",",$key->pd_meta_keyword);
                    $count = count($img_tags);
                    $tags = "[";
                    for ($i=0; $i < $count; $i++) {
                        // echo $i;
                        $tags .= '{text:"'.$img_tags[$i].'"},';
                    }

                    $tags .= "]";

                    $key->tagsa = $tags;
                }
        return response()->json($lang);
    }
	/**
     * Update Download
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_download()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");

        $blog->post_ordered = "1";

        $data2 = input::get("imag2");
        $data3 = input::get("imag3");
        if($data2!=""){
			$mimes = $this->getB64Type($data2);
			$tp  = $this->getExtension($mimes);
            $data2 = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $data2));

			$pdf = md5(uniqid(rand(), true)).$tp;
			$data2 = base64_decode(preg_replace('#^data:application/\w+;base64,#i', '', $data2));
			file_put_contents('api/files/'.$pdf, $data2);
			$sizePDF = filesize('api/files/'.$pdf);
            $blog->post_thumbnail2 = $pdf;
        }
        if($data3!=""){
            $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));
            $name = md5(uniqid(rand(), true)).'_cover.png';
            file_put_contents('api/image/blog/'.$name, $data3);
            $blog->post_thumbnail3 = $name;
        }


        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;
                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_pagetitle = input::get("pagetitle");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->language = "1";
                    if($data2!=""){
                        $blogDes->pd_meta_description = $sizePDF;
                    }
                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete_gallery($filename ="",$id)
    {
        $message="";
        if(File::delete('api/image/gallery/'.$filename)){
            $message=array("image"=>"Photo successfully deleted");
        }else{
            $message=array("image"=>"error");
        }

        $remove = BlogModel::where('post_id',$id)->delete();
        if($remove){
            $remove2 = BlogDesModel::where('post_id',$id)->delete();
            if($remove2)
            {
                $message = array("status"=>"Post successfully deleted");
            }
        }

        
        return response()->json($message);
    }
     public function delete_gallery_id($id)
    {
        $message="";
        
        $lang = BlogModel::where('madappe_blog_post.post_id',$id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
		foreach($lang as $key)
		{
                $path1 = 'api/image/gallery/'.$key->post_thumbnail;


                if(File::delete($path1)){
                    $message=array("image"=>"Photo successfully deleted");
                }else{
                    $message=array("image"=>"error");
                }
        }

        $remove = BlogModel::where('post_id',$id)->delete();
        if($remove){
            $remove2 = BlogDesModel::where('post_id',$id)->delete();
            if($remove2)
            {
                $message = array("status"=>"Post successfully deleted");
            }
        }

        
        return response()->json($message);
    }
    public function bulk_deletegallery()
    {
      
        $data1 = explode(',', input::get("id"));
        $no = 1;

        foreach($data1 as $dt){
           $this->delete_gallery_id($dt);
        }

        $message = array("status"=>"Deleted","id"=>$data1,"t"=>1);
        return response()->json($message);

    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete_download($filename,$id)
    {
        $message="";
        if(File::delete('api/files/'.$filename)){
            $remove = BlogModel::where('post_id',$id)->delete();
            if($remove){
                $remove2 = BlogDesModel::where('post_id',$id)->delete();
                if($remove2)
                {
                    $message = array("status"=>"Post successfully deleted");
                }
            }
            $message=array("status"=>"Photo successfully deleted");
        }else{
            $message=array("status"=>"error");
        }
        return response()->json($message);
    }

	public function contact()
    {
        $lang = ContactModel::get();
        return response()->json($lang);
    }
	public function website()
    {
        $lang = WebsiteModel::get();
		$web = array();
		foreach($lang as $key)
		{
			$typ = $key->type;
			$web[$typ] = $key->value;

			if($typ == 'logo_light'){
				$logolight = $this->imageTolib('api/image/website/'.$key->value);
				$web['logolight'] = $logolight;
			}
			if($typ == 'logo_light'){
				$logodark = $this->imageTolib('api/image/website/'.$key->value);
				$web['logodark'] = $logodark;
			}
		}

        return response()->json($web);
    }
	public function social()
    {
        $lang = SocialModel::get();
        return response()->json($lang);
    }







	//untouchable


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_news($id, $order = 'asc')
    {
        $lang = BlogModel::where('post_categories_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date',$order)
                           ->get();

        foreach($lang as $key)
        {
            $path1 = '';
            if($key->post_thumbnail2 != '') {
                $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail2);
            }

            // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
            // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

            $key->image = $path1;

            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_team($order = 'asc')
    {
        $lang = BlogModel::where('post_type', 'team')
                           ->where('post_status', 'visible')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date',$order)
                           ->get();

        foreach($lang as $key)
        {
            $path1 = '';
            if($key->post_thumbnail2 != '') {
                $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail2);
            }

            // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
            // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

            $key->image = $path1;

            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_showcase($order = 'asc')
    {
        $lang = BlogModel::where('post_type', 'showcase')
                           ->where('post_status', 'visible')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date',$order)
                           ->get();

        foreach($lang as $key)
        {
            $path1 = '';
            if($key->post_thumbnail2 != '') {
                $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail2);
            }

            // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
            // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

            $key->image = $path1;

            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_brands()
    {
        $lang = BlogModel::where('post_categories_id', 10)
                           ->where('post_status', 'visible')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date','asc')
                           ->get();

        foreach($lang as $key)
        {
            $path1 = '';
            if($key->post_thumbnail2 != '') {
                $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail2);
            }

            // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
            // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

            $key->image = $path1;

            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_quick_download($order = 'asc')
    {
        $lang = BlogModel::where('post_type', 'download')
                           ->where('pd_content', '1')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date',$order)
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_quick_download_group($group = 'post_categories_id')
    {
        $lang = BlogModel::where('post_type', 'download')
                           ->where('pd_content', '1')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date','desc')
                           ->groupBy($group)
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index_articles()
    {
        $lang = BlogModel::where('post_type_id', '99')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date','asc')
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function articles_category($id)
    {
        $lang = BlogModel::where('post_categories_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date','asc')
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }
    public function download_category($order = 'asc',$id)
    {
        $lang = BlogModel::where('post_categories_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy('madappe_blog_post.post_publish_date',$order)
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('categories_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }

    public function index_info()
    {
        $lang = BlogModel::where('post_type_id',1)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        foreach($lang as $key)
        {
            $cate = CategoryDesModel::where('cd_id',$key->post_categories_id)
                           ->get();
            $yo = "";
            foreach($cate as $ko)
            {
                $yo = $ko->cd_name;
            }
            $key->cd_name = $yo;
        }
        return response()->json($lang);
    }

    public function index_cat($id,$slug)
    {
        $lang = BlogModel::where('post_categories_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy("madappe_blog_post.post_id","DESC")
                           ->get();
                           $data["news"] = $lang;
        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->dateTime = $new_date;
            $key->author = $key->post_author;
        }
        return view("event")->with("data",$data);
    }

    public function index_video()
    {
        $vid1 = BlogModel::where('post_id', 6)
                           ->get();

        $vid2 = BlogModel::where('post_id', 7)
                           ->get();

        $vid3 = BlogModel::where('post_id', 8)
                           ->get();

        foreach($vid1 as $key)
        {
            $video1 = $key->post_thumbnail;
        }
        foreach($vid2 as $key)
        {
            $video2 = $key->post_thumbnail;

        }
        foreach($vid3 as $key)
        {
            $video3 = $key->post_thumbnail;
        }

        $vi = array("video1"=>$video1,"video2"=>$video2,"video3"=>$video3);
        return response()->json($vi);
    }

    public function lastBlog($id)
    {
        $lang = BlogModel::where('post_categories_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy("madappe_blog_post.post_id","DESC")
                           ->limit(1)
                           ->get();

        $data["news"] = $lang;

        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->dateTime = $new_date;
            $key->author = $key->post_author;
        }

        return response()->json($lang);
    }

    public function lastest_news($id)
    {
        $lang = BlogModel::where('post_type_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy("madappe_blog_post.post_id","DESC")
                           ->get();
        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->date = $new_date;

            $arrayReplace = array(" ",",","'");
            $key->title_href = strip_tags(str_replace($arrayReplace, "-", str_replace("-", " ", $key->pd_title)));
        }
        return response()->json($lang);
    }
    public function event()
    {
        $data["title"] = "Event And News";
        $lang = BlogModel::where('post_type_id', "1")
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy("madappe_blog_post.post_date","DESC")
                           ->get();
        $data["news"] = $lang;

        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->dateTime = $new_date;
            $key->author = $key->post_author;
        }
        return view("event")->with("data",$data);
    }
    public function life()
    {
        $data["title"] = "Life";
        $lang = BlogModel::where('post_type_id', "2")
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy("madappe_blog_post.post_id","DESC")
                           ->get();
        $data["news"] = $lang;
        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->dateTime = $new_date;
            $key->author = $key->post_author;
        }
        return view("event")->with("data",$data);
    }
    public function world()
    {
        $data["title"] = "World";
        $lang = BlogModel::where('post_type_id', "3")
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->orderBy("madappe_blog_post.post_id","DESC")
                           ->get();
        $data["news"] = $lang;
        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->dateTime = $new_date;
            $key->author = $key->post_author;
        }
        return view("event")->with("data",$data);
    }
    public function news_detail($id)
    {
        $temp = explode("-", $id);
        $blog_id = $temp[sizeof($temp)-1];

        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        $data["title"] = "Event Detail";
        $data["news"] = $lang;
        foreach($lang as $key)
        {
            $date_time = strtotime($key->post_publish_date);
            $new_date = date('d M Y', $date_time);
            $key->date = $new_date;
        }
        return response()->json($lang);
    }


    public function show_album($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        return response()->json($lang);
    }

    public function show_contact()
    {
        $lang = BlogModel::where('post_categories_id', "7")
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        return response()->json($lang);
    }

    //GET CATEGORY JSON
    public function category_news($id)
    {
        $lang = CategoryModel::where('type_id', $id)
                           ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                           ->get();
        return response()->json($lang);
    }
    public function articles_category_list($id)
    {
        $lang = CategoryModel::where('type_id', $id)
                           ->join("madappe_blog_category_description","madappe_blog_category_description.categories_id",'=','madappe_blog_categories.categories_id')
                           ->get();
        return response()->json($lang);
    }

    public function dna_list($id)
    {
        $lang = BlogModel::where('post_categories_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        return response()->json($lang);
    }


    public function gallery_list()
    {
        $lang = \DB::select( DB::raw("SELECT post1.post_id,post1.post_thumbnail,post1.post_modified,pd1.pd_title,pd2.pd_title as album FROM madappe_blog_post post1 INNER JOIN madappe_blog_post_description pd1 ON post1.post_id=pd1.post_id ,madappe_blog_post post2 INNER JOIN madappe_blog_post_description pd2 ON post2.post_id=pd2.post_id where post1.post_type_id=post2.post_id and post1.post_categories_id=9"));
        return response()->json($lang);
    }

    public function photo_gallery($id)
    {
        $lang = \DB::select( DB::raw("SELECT post1.post_thumbnail as url FROM madappe_blog_post post1 INNER JOIN madappe_blog_post_description pd1 ON post1.post_id=pd1.post_id ,madappe_blog_post post2 INNER JOIN madappe_blog_post_description pd2 ON post2.post_id=pd2.post_id where post1.post_type_id=post2.post_id and post1.post_categories_id=9 and post1.post_type_id=$id"));
        return response()->json($lang);
    }

    public function author()
    {
        $lang = AuthorModel::get();
        return response()->json($lang);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function team_store()
    {
        $message = array();

        $blog = new BlogModel;

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = input::get("post_type");

        $blog->post_ordered = "1";

        $data1 = input::get("imag1");
        $data2 = input::get("imag2");
        // $data3 = input::get("imag3");

        // list($type, $data) = explode(';', $data);
        // list(, $data)      = explode(',', $data);
        $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
        $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
        // $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));

        $name = md5(uniqid(rand(), true));
        $logo = md5(uniqid(rand(), true)).'_thumbnail.png';

        file_put_contents('api/image/blog/'.$name.'_1.png', $data1);
        file_put_contents('api/image/blog/'.$logo, $data2);

        // Output
        //imagejpeg($thumb,$uploadPath);
        // file_put_contents('api/image/blog/'.$name.'_3.png', $data3);



        $blog->post_thumbnail = $name;
        $blog->post_thumbnail2 = $logo;

        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showcase_store()
    {
        $message = array();

        $blog = new BlogModel;

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = input::get("post_type");

        $blog->post_ordered = "1";

        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;
                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->language = "1";
                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function gallery_uploader($id)
    {
        echo $id;
        if ( !empty( $_FILES ) ) {

            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $name = md5(uniqid(rand(), true));
            $uploadPath = getcwd().'/api/image/gallery/'.$name.$id.'.jpg';

            $message = array();

            $blog = new BlogModel;

            $blog->post_author = "Admin";
            $blog->post_date = date("Y-m-d H:i:s");
            $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
            $blog->post_type_id = $id;
            $blog->post_categories_id = "9";
            $blog->post_status = "visible";
            $blog->post_publish_date = date("Y-m-d H:i:s");
            $blog->post_comment_status = "active";
            $blog->post_comment_count = "1";
            $blog->post_password = "";
            $blog->post_modified = date("Y-m-d H:i:s");
            $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
            $blog->post_type = "photo gallery";
            $blog->post_ordered = "1";
            $blog->post_thumbnail = $name.$id.'.jpg';

            try {
                $blog->save();
                    try {
                        $blogDes = new BlogDesModel;

                        $blogDes->pd_content = "Content Gallery";
                        $blogDes->pd_title = "Title Gallery";
                        $blogDes->pd_excerpt = "";
                        $blogDes->pd_meta_description = "";
                        $blogDes->pd_pagetitle = "";
                        $blogDes->pd_meta_keyword = "";
                        $blogDes->language = "1";

                        $blog->BlogDesModels()->save($blogDes);

                    } catch (Exception $e) {
                        $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                    }

            }
            catch(Exception $e){
                $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
            }

            // Get new sizes
            list($width, $height) = getimagesize($tempPath);

            if($width>1280){
                $newwidth = 1280;
                $newheight = $height * ($newwidth/$width);

                // Load
                $thumb = imagecreatetruecolor($newwidth, $newheight);
                $source = imagecreatefromjpeg($tempPath);

                // Resize
                imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                // Output
                imagejpeg($thumb,$uploadPath);
            }else{
                move_uploaded_file( $tempPath, $uploadPath );
            }

            $answer = array( 'answer' => 'File transfer completed' );
            $json = json_encode( $answer );

            return $json;

        } else {

            return 'No files';

        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store_album()
    {
        $message = array();

        $blog = new BlogModel;

        $date = str_replace("/","-",input::get("events_date"))." 00:00:00";

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = $date;
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        $data1 = input::get("imag1");
        $data2 = input::get("imag2");
        // $data3 = input::get("imag3");

        // list($type, $data) = explode(';', $data);
        // list(, $data)      = explode(',', $data);
        $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
        $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
        // $data3 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data3));

        $name = md5(uniqid(rand(), true)).'_thumbnail.png';
        $logo = md5(uniqid(rand(), true)).'_logo.png';

        file_put_contents('api/image/blog/'.$name, $data1);
        file_put_contents('api/image/blog/'.$logo, $data2);
        // file_put_contents('api/image/blog/'.$name.'_3.png', $data3);

        $blog->post_thumbnail = $name;
        $blog->post_thumbnail2 = $logo;

        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store_join()
    {
        $message = array();

        $blog = new BlogModel;

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        try {
            $blog->save();
                try {
                    $blogDes = new BlogDesModel;

                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit_news($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $path1 = '';
                        if($key->post_thumbnail != '') {
                            $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_1.png');
                        }
                        // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                        // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                        $key->image = $path1;
                        // $key->image2 = $path2;
                        // $key->image3 = $path3;

                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    public function edit_brands($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $path1 = '';
                        if($key->post_thumbnail2 != '') {
                            $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail2);
                        }

                        // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                        // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                        $key->image = $path1;
                        // $key->image2 = $path2;
                        // $key->image3 = $path3;

                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }




    public function edit_about($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $path1 = '';
                        if($key->post_thumbnail != '') {
                            $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_1.png');
                        }

                        // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                        // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                        $key->image = $path1;
                        // $key->image2 = $path2;
                        // $key->image3 = $path3;

                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit_album($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail);
                        // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                        // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                        $key->image = $path1;
                        // $key->image2 = $path2;
                        // $key->image3 = $path3;

                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit_join($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();

        return response()->json($lang);
    }
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function news()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','11')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_1.png');
                        // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                        // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                        //$key->image = $path1;
                        // $key->image2 = $path2;
                        // $key->image3 = $path3;

                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function brands()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','8')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        //$path1 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_1.png');
                        // $path2 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_2.png');
                        // $path3 = $this->imageTolib('api/image/blog/'.$key->post_thumbnail.'_3.png');

                        //$key->image = $path1;
                        // $key->image2 = $path2;
                        // $key->image3 = $path3;

                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function corporate()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','12')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function csr()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','13')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function joinus()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','6')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function contactus()
    {
        $lang = BlogModel::where('madappe_blog_post.post_categories_id','7')
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
                foreach($lang as $key)
                {
                        $img_tags = explode(",",$key->pd_meta_keyword);
                        $count = count($img_tags);
                        $tags = "[";
                        for ($i=0; $i < $count; $i++) {
                            // echo $i;
                            $tags .= '{text:"'.$img_tags[$i].'"},';
                        }

                        $tags .= "]";

                        $key->tagsa = $tags;
                }
        return response()->json($lang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit_info($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->join("madappe_blog_post_description","madappe_blog_post_description.post_id",'=','madappe_blog_post.post_id')
                           ->get();
        return response()->json($lang);
    }



    public function edit_video($id)
    {
        $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->where("post_type_id",2)
                           ->get();
        return response()->json($lang);
    }

    public function bannerShow($id)
    {
        $lang = Banner::where('banner_id', $id)
                           ->get();
                foreach($lang as $key)
                {
                        if($id ==1 || $id ==2 || $id ==7  || $id ==8  || $id ==9 )
                        {
                            $path1 = $this->imageTolib('api/image/banner/'.$key->banner_image.'.png');
                            $key->image = $path1;

                        }
                        else if($id == 3 || $id == 4 || $id == 5 || $id == 6)
                        {

                            $path1 = $this->imageTolib('api/image/banner/'.$key->banner_image.'.png');
                            $path2 = $this->imageTolib('api/image/banner/'.$key->banner_image.'_2.png');
                            $key->image2 = $path2;
                            $key->image = $path1;
                        }
                        else
                        {

                            $path3 = $this->imageTolib('api/image/banner/'.$key->banner_image.'_3.png');


                            $key->image3 = $path3;

                        }

                        // $img_tags = explode(",",$key->pd_meta_keyword);
                        // $count = count($img_tags);
                        // $tags = "[";
                        // for ($i=0; $i < $count; $i++) {
                        //     // echo $i;
                        //     $tags .= '{text:"'.$img_tags[$i].'"},';
                        // }

                        // $tags .= "]";

                        // $key->tagsa = $tags;
                }
        return response()->json($lang);
    }
    public function bannerfront($id)
    {
        $lang = Banner::where('banner_id', $id)
                           ->get();
        return response()->json($lang);
    }
    function imageTolib($path)
    {
        $type = pathinfo($path, PATHINFO_EXTENSION);

            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        return $base64;
    }
    public function updateBanner()
    {
        $message = array();

        $blog = Banner::where('banner_id', input::get("post_id"))->first();

            $data1 = input::get("imag1");
            $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
            $name = md5(uniqid(rand(), true));
            $blog->banner_image = $name;
            file_put_contents('api/image/banner/'.$name.'.png', $data1);
        if(input::get("post_id") == 1 || input::get("post_id") == 2  || input::get("post_id") == 7 || input::get("post_id") == 8 || input::get("post_id") == 9)
        {
            $blog->ext1 = input::get("ext1");
            $blog->save();
        }
        else if(input::get("post_id") == 3 || input::get("post_id") == 4 || input::get("post_id") == 5 || input::get("post_id") == 6)
        {
            $blog->ext1 = input::get("ext1");
            $data2 = input::get("imag2");
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            file_put_contents('api/image/banner/'.$name.'_2.png', $data2);
            $blog->save();
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function infoUpdate()
    {
        $message = array();
        if(Auth::check())
        {
            $user = Auth::User();
            if($user->user_role == 0)
            {
                $blog = BlogModel::where('post_id', input::get("post_id"))->first();

                // $field = array(
                //                 "post_author" => input::get("author"),
                //                 "post_date" => date("Y-m-d H:i:s"),
                //                 "post_date_gmt" => gmdate("Y-m-d H:i:s"),
                //                 "post_type_id" => "1",
                //                 "post_categories_id" => "1",
                //                 "post_status" => input::get("visibility"),
                //                 "post_publish_date" => date("Y-m-d H:i:s"),
                //                 "post_comment_status" => "active",
                //                 "post_comment_count" => "1",
                //                 "post_password" => "",
                //                 "post_modified" => date("Y-m-d H:i:s"),
                //                 "post_modified_gmt" => gmdate("Y-m-d H:i:s"),
                //                 "post_type" => "article",
                //                 "post_thumbnail" => "",
                //                 "post_ordered" => "1",

                //     );


                $blog->post_modified = date("Y-m-d H:i:s");
                $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");

                try {
                    $blog->save();
                        try {
                            $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                            // $blogDes = new BlogDesModel;

                            $blogDes->pd_content = input::get("content");
                            $blogDes->language = "1";

                            $blog->BlogDesModels()->save($blogDes);

                            $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                        } catch (Exception $e) {
                            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                        }

                }
                catch(Exception $e){
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }
            }
            else
            {
                $message = array("status"=>"Login Expired","id"=>"0","t"=>0);
            }
        }
        else{
             $message = array("status"=>"Login Expired","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_album()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $date = str_replace("/","-",input::get("events_date"))." 00:00:00";
        //echo $date;
        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type_id = input::get("type");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = $date;
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        $data1 = input::get("imag1");
        $data2 = input::get("imag2");
        if($data1!=""){
            $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
            $name = md5(uniqid(rand(), true))."_thumbnail.png";
            $blog->post_thumbnail = $name;
            file_put_contents('api/image/blog/'.$name, $data1);
        }

        if($data2!=""){
            $data2 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data2));
            $name = md5(uniqid(rand(), true))."_logo.png";
            $blog->post_thumbnail2 = $name;
            file_put_contents('api/image/blog/'.$name, $data2);
        }


        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;

                    $tags = input::get("tags");

                    $count_tags = sizeof($tags);

                    $tag_result = array();

                    for ($i=0; $i < $count_tags; $i++) {
                        // $tag_count = $tags[$i];
                        // $tag_result[] = array_merge($tags[$i],$tag_result);

                        $tag_result[$i] = $tags[$i]["text"];
                    }

                    $tags_re = implode(",",$tag_result);


                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->pd_excerpt = input::get("excerpt");
                    $blogDes->pd_meta_description = input::get("pagetitle");
                    $blogDes->pd_pagetitle = input::get("metadecription");
                    $blogDes->pd_meta_keyword = $tags_re;
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>$date."Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_join()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;

                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_corporate()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;

                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update_csr()
    {
        $message = array();

        $blog = BlogModel::where('post_id', input::get("post_id"))->first();

        $blog->post_author = input::get("author");
        $blog->post_date = date("Y-m-d H:i:s");
        $blog->post_date_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_categories_id = input::get("category");
        $blog->post_status = input::get("visibility");
        $blog->post_publish_date = date("Y-m-d H:i:s");
        $blog->post_comment_status = "active";
        $blog->post_comment_count = "1";
        $blog->post_password = "";
        $blog->post_modified = date("Y-m-d H:i:s");
        $blog->post_modified_gmt = gmdate("Y-m-d H:i:s");
        $blog->post_type = "article";

        $blog->post_ordered = "1";

        try {
            $blog->save();
                try {
                    $blogDes = BlogDesModel::where('post_id', input::get("post_id"))->first();
                    // $blogDes = new BlogDesModel;

                    $blogDes->pd_content = input::get("content");
                    $blogDes->pd_title = input::get("title");
                    $blogDes->language = "1";

                    $blog->BlogDesModels()->save($blogDes);

                    $message = array("status"=>"Insert Data Success","id"=>$blog->id,"t"=>1);

                } catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
                }

        }
        catch(Exception $e){
            $message = array("status"=>"Internal Server Error","id"=>"0","t"=>0);
        }
        return response()->json($message);
    }



    public function uploadVideo($id)
    {
        // print_r($_FILES);
        // $file = Request::file("file");
        $message="";
        if(Auth::check())
        {
            $user = Auth::User();
            if($user->user_role == 0)
            {
                $file = Input::file("file");
                $extension = $file->getClientOriginalExtension();
                $name = sha1(microtime()) . "." . $extension;
                move_uploaded_file($file->getPathName(), getcwd() . "/assets/video/" . $name);
                if($id == 6 || $id == 7 || $id == 8)
                {
                    $video ="";
                    $lang = BlogModel::where('madappe_blog_post.post_id', $id)
                           ->get();
                    foreach($lang as $key)
                    {
                        $video = getcwd() . "/assets/video/" . $key->post_thumbnail;
                    }

                    if(File::exists($video)) {
                        if(File::delete($video))
                        {
                            $blog = BlogModel::where('post_id',$id)->first();
                            $blog->post_thumbnail = $name;
                            if($blog->save())
                            {
                                $message = array("status"=>"Saved","t"=>1);
                            }
                            else
                            {
                                $message = array("status"=>"Internal Server Error","t"=>0);
                            }
                        }
                        else
                        {
                                $message = array("status"=>"Internal Server Error","t"=>0);
                        }
                    }

                }

            }
        }
        return response()->json($message);
    }
    public function upload_image()
    {

            // Allowed extentions.
            $allowedExts = array("gif", "jpeg", "jpg", "png");

            // Get filename.
            $temp = explode(".", $_FILES["image_param"]["name"]);

            // Get extension.
            $extension = end($temp);

            // An image check is being done in the editor but it is best to
            // check that again on the server side.
            // Do not use $_FILES["file"]["type"] as it can be easily forged.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $_FILES["image_param"]["tmp_name"]);

            if ((($mime == "image/gif")
            || ($mime == "image/jpeg")
            || ($mime == "image/pjpeg")
            || ($mime == "image/x-png")
            || ($mime == "image/png"))
            && in_array($extension, $allowedExts)) {
                // Generate new random name.
                $name = sha1(microtime()) . "." . $extension;

                // Save file in the uploads folder.
                move_uploaded_file($_FILES["image_param"]["tmp_name"], getcwd() . "/api/image/blog/" . $name);

                // Generate response.
                // $response = new StdClass;
                $response["link"] = "api/image/blog/" . $name;
                echo stripslashes(json_encode($response));
            }
    }
    public function upload_file()
    {
        // Allowed extentions.
        $allowedExts = array("txt", "pdf", "doc");

        // Get filename.
        $temp = explode(".", $_FILES["upload_param"]["name"]);
        // print_r($_FILES);
        // Get extension.
        $extension = end($temp);

        // Validate uploaded files.
        // Do not use $_FILES["file"]["type"] as it can be easily forged.
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $_FILES["upload_param"]["tmp_name"]);

        if ((($mime == "text/plain")
        || ($mime == "application/msword")
        || ($mime == "application/x-pdf")
        || ($mime == "application/pdf"))
        && in_array($extension, $allowedExts)) {
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            // Save file in the uploads folder.
            move_uploaded_file($_FILES["upload_param"]["tmp_name"], getcwd() . "/api/file/blog/" . $name);

            // Generate response.

            $response["link"] = "download/file/blog/" . $name;
            echo stripslashes(json_encode($response));
        }
    }

    public function getComment($id)
    {
        $lang = CommentModel::where('forum_comment.thread_id',$id)
                           ->where('forum_comment.comment_type',3)
                           ->select(\DB::raw("*,forum_comment.created_at as dates"))
                           ->where('forum_comment.comment_parent',0)
                           ->join("madappe_gen_users","madappe_gen_users.id",'=','forum_comment.user_id')
                           ->get();
        foreach($lang as $key)
        {
            $comment = CommentModel::where('forum_comment.thread_id',$id)
                           ->where('forum_comment.comment_type',3)
                           ->select(\DB::raw("*,forum_comment.created_at as dates"))
                           ->where('comment_parent',$key->comment_id)
                           ->join("madappe_gen_users","madappe_gen_users.id",'=','forum_comment.user_id')
                           ->get();
            $love = CommentLoveModel::where('comment_id',$key->comment_id)
                            ->where('status', 1)
                           ->get();

            $key->comment_count = count($comment);
            $key->love_count = count($love);
            if($key->user_photo == "")
            {
                $key->user_photo = "Profile_03.png";
            }
            else
            {
                $key->user_photo = $key->user_photo;
            }
            $date_time1 = strtotime($key->dates);
            $new_date1 = date('d M ,Y', $date_time1);
            $key->create_date1 =  $new_date1;

            $key->love_status = "Like";
            if(Auth::check())
                    {
                        $user = Auth::User();
                        $userId = $user->id;
                        $check_love = CommentLoveModel::where('comment_id', $key->comment_id)
                                            ->where('status', 1)
                                            ->where('user_id', $userId)
                                            ->get();
                        $check_row = count($check_love);

                        if($check_row != 0)
                        {
                            $key->love_status = "Unlike";
                        }else
                        {
                            $key->love_status = "Like";
                        }
                    }

            $key->comment_child = $comment;
            foreach($key->comment_child as $key)
            {
                $love2 = CommentLoveModel::where('comment_id',$key->comment_id)
                        ->where('status', 1)
                           ->get();
                    $key->love_count2 = count($love2);
                    $key->love_status2 = "Like";
                    //CHECK USER LOVED STATUS

                    if(Auth::check())
                    {
                        $user = Auth::User();
                        $userId = $user->id;
                        $check_love1 = CommentLoveModel::where('comment_id', $key->comment_id)
                                            ->where('status', 1)
                                            ->where('user_id', $userId)
                                            ->get();
                        $check_row1 = count($check_love1);

                        if($check_row1 != 0)
                        {
                            $key->love_status2 = "Unlike";
                        }else
                        {
                            $key->love_status2 = "Like";
                        }
                    }

                if($key->user_photo == "")
                {
                    $key->user_photo = "Profile_03.png";
                }
                else
                {
                    $key->user_photo = $key->user_photo;
                }
                $date_time1 = strtotime($key->dates);
                $new_date1 = date('d M ,Y', $date_time1);
                $key->create_date1 =  $new_date1;
            }
        }
        return response()->json($lang);
    }
    /* creates a compressed zip file */
    public function archive_download() {
        $overwrite = false;
        $acak = md5(uniqid(rand(), true));
        $destination = public_path('/api/file/'.$acak.'.zip');
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) { return false; }
        //vars
        $valid_files = array();
        $files = $_POST['files'];
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists(public_path('/api/image/blog/'.$file))) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return 'Failed';
            }
            //add the files
            foreach($valid_files as $file) {
                $zip->addFile(public_path('/api/image/blog/'.$file),$file);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return redirect('/api/file/'.$acak.'.zip');
        }
        else
        {
            return 'Failed';
        }
    }
}
