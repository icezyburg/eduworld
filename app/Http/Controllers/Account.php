<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AccountModel;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use Crypt;
use Auth;
class Account extends Controller
{
    public function register()
    {
        $message = array();
        $acc = new AccountModel;

        $captcha = input::get('g-recaptcha-response');

        $postdata = http_build_query(
          array(
            'secret' => '6LelMQgTAAAAAMOCcfEXURbhEujZjryh_MQjOk-D', //secret KEy provided by google
            'response' => $captcha,                    // g-captcha-response string sent from client
            'remoteip' => $_SERVER['REMOTE_ADDR']
          )
        );
        $opts = array('http' =>
          array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
          )
        );
        //Create a stream this is required to make post request with fetch_file_contents
        $context  = stream_context_create($opts);

    /* Send request to Googles siteVerify API */
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify",false,$context);
    $response = json_decode($response, true);

        if($response["success"]===false) {
            $message = array("Status"=>"Captcha verification failed",'t'=>0);
        }
        else
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $vowel = array("/","+","=");
            $time = date("HiaDMY", time());
            $active_code = str_replace($vowel,"",Crypt::encrypt(substr(str_shuffle($characters), 0, 20).input::get("email")));
            $acc->username = input::get("email");
            $acc->user_fname = input::get("fname");
            $acc->user_lname = input::get("lname");
            $acc->email = input::get("email");
            $acc->password = Hash::make(input::get("password"));
            $acc->active_code = $active_code;
            $acc->user_status = "deactive";
            $acc->join_date = date('Y-m-d H:i:s');
            $acc->user_role = 1;
            try {
                $acc->save();
                Mail::send('mail.register', array(
                    'name' => input::get("fname")+" "+input::get("lname"),
                    'email' => input::get("email"),
                    'username' => input::get("email"),
                    'date' => date('Y-m-d H:i:s'),
                    'active' => $active_code,
                ), function ($message_im) {
                            $message_im->from('clevatea.games@gmail.com', 'Admin');
                            $message_im->sender('clevatea.games@gmail.com', 'Admin');
                            $message_im->to(input::get("email"))->subject('Account Activation');
                        });
                $message = array("status"=>"Your email activation has been sended to your email",'t'=>1);
            }
            catch(Exception $e){
                $message = array("status"=>"Internal Server Error, Please Try Again",'t'=>1);
            }
        }
    return response()->json($message);
    }
    public function admin()
    {
        $message = array();
        $acc = new AccountModel;

          if(Auth::check())
            {
            $user = Auth::User();
            if($user->user_token == 1)
            {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $vowel = array("/","+","=");
                $time = date("HiaDMY", time());
                $active_code = str_replace($vowel,"",Crypt::encrypt(substr(str_shuffle($characters), 0, 20).input::get("email")));
                $acc->username = input::get("email");
                $acc->user_fname = input::get("fname");
                $acc->user_lname = input::get("lname");
                $acc->email = input::get("email");
                $acc->password = Hash::make(input::get("password"));
                $acc->active_code = $active_code;
                $acc->user_status = "active";
                $acc->user_token = "2";
                $acc->join_date = date('Y-m-d H:i:s');
                $acc->user_role = 0;
                try {
                    $acc->save();
                    $message = array("status"=>"Your email activation has been sended to your email",'t'=>1);
                }
                catch(Exception $e){
                    $message = array("status"=>"Internal Server Error, Please Try Again",'t'=>0);
                }
            }
            else
            {
                    $message = array("status"=>"Internal Server Error, Please Try Again",'t'=>0);

            }
        }
        else
        {
                    $message = array("status"=>"Internal Server Error, Please Try Again",'t'=>0);

        }

    return response()->json($message);
    }
    public function changepasswordadm()
	{
		$message = array("status"=>"Internal Server Error","t"=>"0");
        $acc = AccountModel::where('id', input::get("user_id"))->first();
        $acc_user = AccountModel::where('id', input::get("user_id"))->get();

          if(Auth::check())
            {
            $user = Auth::User();
            if($user->user_token == 1 || $user->user_token == 2)
            {
                if(count($acc_user) != 0)
                {
                    foreach ($acc_user as $key) {
                        $current_password = $key->password;
                    }
                    $getCur = input::get('password');
                    if (Hash::check($getCur, $current_password))
                    {
                        $password = Hash::make(input::get('newpassword'));

                        $acc->password = $password;

                        try {
                        $acc->save();
                            $message = array("status"=>"Change Password Success","t"=>"1");
                        }
                        catch (Exception $e) {
                                $message = array("status"=>"Internal Server Error","t"=>"0");
                        }
                    }
                    else{
                        $message = array("status"=>"Wrong Current Password","t"=>"0");
                    }
                }else
                {
                    $message = array("status"=>"Internal Server Error, Please Try Again",'t'=>0);
                }
            }
            else
            {
                    $message = array("status"=>"Internal Server Error, Please Try Again",'t'=>0);

            }
        }
        else
        {
                	$message = array("status"=>"Internal Server Error, Please Try Again",'t'=>0);

        }

	return response()->json($message);
	}
    public function forgotpassword()
    {
        $message = array("status"=>"Internal Server Error","t"=>"0");


        $captcha = input::get('g-recaptcha-response');

        $postdata = http_build_query(
          array(
            'secret' => '6LelMQgTAAAAAMOCcfEXURbhEujZjryh_MQjOk-D', //secret KEy provided by google
            'response' => $captcha,                    // g-captcha-response string sent from client
            'remoteip' => $_SERVER['REMOTE_ADDR']
          )
        );
        $opts = array('http' =>
          array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
          )
        );
        //Create a stream this is required to make post request with fetch_file_contents
        $context  = stream_context_create($opts);

    /* Send request to Googles siteVerify API */
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify",false,$context);
    $response = json_decode($response, true);
    // dd($response["success"]);
        if($response["success"]==false) {
            $message = array("status"=>"Captcha verification failed","t"=>0);
        }
        else
        {
            $acc_user = AccountModel::where('email', input::get("email"))->get();

            if(count($acc_user) != 0)
            {
                try{
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $vowel = array("/","+","=");
                $time = date("HiaDMY", time());
                $active_code = str_replace($vowel,"",Crypt::encrypt(substr(str_shuffle($characters), 0, 20).input::get("email")));

                Mail::send('mail.forgot', array(
                    'email' => input::get("email"),
                    'date' => date('Y-m-d H:i:s'),
                    'active' => $active_code,
                ), function ($message_im) {
                            $message_im->from('clevatea.games@gmail.com', 'Admin');
                            $message_im->sender('clevatea.games@gmail.com', 'Admin');
                            $message_im->to(input::get("email"))->subject('Forgot Password');
                        });
                $acc = AccountModel::where('email', input::get("email"))->first();
                $acc->active_code = $active_code;
                    try{
                        $acc->save();
                        $message = array("status"=>"Check your email for continue","t"=>"1");
                    } catch (Exception $e) {

                    $message = array("status"=>"Internal Server Error","t"=>"0");
                    }
                }
                catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","t"=>"0");
                }
            }
            else
            {
                 $message = array("status"=>"Email not registered","t"=>0);
            }
        }
        return response()->json($message);
    }
    public function changepassword()
    {
        $message = array("status"=>"Internal Server Error","t"=>"0");
        $acc = AccountModel::where('id', input::get("user_id"))->first();
        $acc_user = AccountModel::where('id', input::get("user_id"))->get();

        $captcha = input::get('g-recaptcha-response');

        $postdata = http_build_query(
          array(
            'secret' => '6LelMQgTAAAAAMOCcfEXURbhEujZjryh_MQjOk-D', //secret KEy provided by google
            'response' => $captcha,                    // g-captcha-response string sent from client
            'remoteip' => $_SERVER['REMOTE_ADDR']
          )
        );
        $opts = array('http' =>
          array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
          )
        );
        //Create a stream this is required to make post request with fetch_file_contents
        $context  = stream_context_create($opts);

    /* Send request to Googles siteVerify API */
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify",false,$context);
    $response = json_decode($response, true);
    // dd($response["success"]);
        if($response["success"]==false) {
            $message = array("status"=>"Captcha verification failed","t"=>0);
        }
        else
        {
            // echo "jancokkk";
            if(count($acc_user) != 0)
            {
                foreach ($acc_user as $key) {
                    $current_password = $key->password;
                }
                $getCur = input::get('password');
                if (Hash::check($getCur, $current_password))
                {
                    $password = Hash::make(input::get('newpassword'));

                    $acc->password = $password;

                    try {
                    $acc->save();
                        $message = array("status"=>"Change Password Success","t"=>"1");
                    }
                    catch (Exception $e) {
                            $message = array("status"=>"Internal Server Error","t"=>"0");
                    }
                }
                else{
                    $message = array("status"=>"Wrong Current Password","t"=>"0");
                }
            }
            else{
                $message = array("status"=>"Internal Server Error","t"=>"0");
            }
            return response()->json($message);
        }
    }

    public function edited()
    {
        $message = array("status"=>"Internal Server Error","t"=>"0");
        $acc = AccountModel::where('id', input::get("user_id"))->first();;

        $captcha = input::get('g-recaptcha-response');

        $postdata = http_build_query(
          array(
            'secret' => '6LelMQgTAAAAAMOCcfEXURbhEujZjryh_MQjOk-D', //secret KEy provided by google
            'response' => $captcha,                    // g-captcha-response string sent from client
            'remoteip' => $_SERVER['REMOTE_ADDR']
          )
        );
        $opts = array('http' =>
          array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
          )
        );
        //Create a stream this is required to make post request with fetch_file_contents
        $context  = stream_context_create($opts);

    /* Send request to Googles siteVerify API */
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify",false,$context);
    $response = json_decode($response, true);

        if($response["success"]===false) {
            $message = array("Status"=>"Captcha verification failed");
        }
        else
        {
            $acc->user_fname = input::get("fname");
            $acc->user_lname = input::get("lname");
            $acc->user_address = input::get("address");
            $acc->user_about = input::get("about");
            $acc->user_phone = input::get("phone");
            $acc->user_soclink1 = input::get("soclink1");
            $acc->user_soclink2 = input::get("soclink2");
            $acc->user_soclink3 = input::get("soclink3");

            $data1 = input::get("photo");
            $data1 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data1));
            $name = md5(uniqid(rand(), true)).".png";
            $acc->user_photo = $name;

            file_put_contents('api/image/profile/'.$name, $data1);
            try {
            $acc->save();
                $message = array("status"=>"Internal Server Error","avatar" => $name ,"t"=>"1");
            }
            catch (Exception $e) {
                    $message = array("status"=>"Internal Server Error","t"=>"0");
            }
            return response()->json($message);
        }
    }
    public function activation()
    {
        $message = array();
        $acc = AccountModel::where('active_code', input::get('active'))
                            ->where('user_status',"deactive")
                            ->first();
        if(!is_null($acc))
        {
            $acc->user_status = "active";
             try {
                $acc->save();
                $message = array("status"=>"Activation Success","t"=>1);
             }
             catch(Exception $e)
             {
                    $message = array("status"=>"Internal Server Error","t"=>0);
             }
        }
        else
        {
            $message = array("status"=>"user not found","t"=>0);
        }
        return response()->json($message);
    }
    public function show_index()
    {
        $lang = AccountModel::where('user_role',1)
                           ->get();
        return response()->json($lang);
    }

    public function listAdmin()
    {
        $lang = AccountModel::where('user_role',0)
//                           ->where('user_token',2)
                           ->get();
        return response()->json($lang);
    }

    public function rowMember()
    {
        $lang = AccountModel::where('user_role',1)
                           ->where('user_status','active')
                           ->get();
        return response()->json(array("row" => count($lang)));
    }
    public function deleteMember($id)
    {
        $message="";
        $remove = AccountModel::where('id',$id)->delete();
            if($remove){
                $message = array("status"=>"Account successfully deleted","t"=>1);
            }
                    return response()->json($message);
    }
    public function deleteMemberadm($id)
    {
        $message="";
    if(Auth::check())
        {
        $user = Auth::User();
        $message = array("status"=>"Internal Server Error","t"=>0);
        if($user->user_token == 1)
        {
            $message = array("status"=>"Internal Server Error","t"=>0);
            $remove = AccountModel::where('id',$id)->delete();
            if($remove){
                $message = array("status"=>"Account successfully deleted","t"=>1);
            }
        }
    }
                    return response()->json($message);
    }
    public function listByAccount($userID)
    {
        $di = $userID;
        $marketplace = \DB::table('forum_thread')
                        ->select(\DB::raw("thread_id as id,
                            madappe_gen_users.username as author,
                            forum_thread.thread_title as title,
                            forum_thread.thread_view_count as ext,
                            forum_thread.thread_create_date as date ,
                            'Marketplace' AS 'type'"))
                        ->join("madappe_gen_users","madappe_gen_users.id",'=','forum_thread.user_id')
                        ->where('forum_thread.thread_type', '=', "1")
                        ->where('madappe_gen_users.user_status', '=', "active")
                        ->where('madappe_gen_users.id', '=', $di);


        $webboard = \DB::table('forum_thread')
                        ->select(\DB::raw("thread_id as id,
                            madappe_gen_users.username as author,
                            forum_thread.thread_title as title,
                            forum_thread.thread_view_count as ext,
                            forum_thread.thread_create_date as date,
                            'Webboard' AS 'type'"))
                        ->join("madappe_gen_users","madappe_gen_users.id",'=','forum_thread.user_id')
                        ->where('forum_thread.thread_type', '=', "2")
                        ->where('madappe_gen_users.user_status', '=', "active")
                        ->where('madappe_gen_users.id', '=', $di);



        $results = $marketplace->union($webboard)->orderBy("date","DESC")->get();

        foreach ($results as $key) {

            $date_time1 = strtotime($key->date);
            $new_date = date('d M Y', $date_time1);
            if($key->type == "Marketplace" || $key->type == "Webboard")
            {
                $key->head = $key->title;


                $title = str_replace(" ","-",trim($key->title));

                $key->link = "webboard.detail({threadID:'".$title."' || html,thID:".$key->id."})";
                $key->sub_head = $new_date." By ".$key->author;
                $key->attr_head = '<a href=""><i class="fa fa-tag"></i>'.$key->type.'</a>
                                   <a href=""><i class="fa fa-eye"></i>'.$key->ext.'</a>';
            }
        }

        return response()->json($results);
    }
}
