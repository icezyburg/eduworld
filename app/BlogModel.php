<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
   protected $table = 'madappe_blog_post';

   protected $guarded = [];

   protected $primaryKey = "post_id";

   public function BlogDesModels()
   {
       return $this->hasMany('App\BlogDesModel', 'post_id');
   }
   public function ImageModels()
   {
       return $this->hasMany('App\ImageModel', 'post_id');
   }
   
}


