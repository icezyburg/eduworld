<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageModel extends Model
{
    protected $table = 'madappe_gen_language';
}
