<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeModel extends Model
{

   protected $table = 'madappe_type';
   protected $primaryKey = "id";
   protected $guarded = [];
}
