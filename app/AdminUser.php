<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'madappe_gen_admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['admin_fname', 'admin_lname', 'admin_username','admin_password','admin_email','admin_photo','admin_status','admin_token','admin_role'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = ['password', 'remember_token'];
}
