<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDesModel extends Model
{
   protected $table = 'madappe_blog_category_description';

   protected $guarded = [];

   protected $primaryKey = "cd_id";

   public function CategoryModel()
   {
       return $this->belongsTo('App\CategoryModel',"categories_id");
   }
}
