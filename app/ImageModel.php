<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
   protected $table = 'madappe_post_image';

   protected $guarded = [];
   
   protected $primaryKey = "id";

   public function BlogModel()
   {
       return $this->belongsTo('App\BlogModel',"post_id");
   }
}
