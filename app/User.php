<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = 'madappe_gen_users';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_fname', 'user_lname', 'user_username','user_password','user_email','user_photo','user_status','user_token','user_role'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}
