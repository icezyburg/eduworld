<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountModel extends Model
{

   protected $table = 'madappe_gen_users';
   protected $primaryKey = "id";
   protected $guarded = [];
}
