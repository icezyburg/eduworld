<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialModel extends Model
{
   protected $table = 'madappe_sosial_media';

   protected $guarded = [];

}
